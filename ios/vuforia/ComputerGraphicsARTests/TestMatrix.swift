//
//  TestMatrix.swift
//  ComputerGraphicsARTests
//
//  Created by Philipp Jenke on 21.11.17.
//  Copyright © 2017 PTC. All rights reserved.
//

import XCTest
@testable import Vuforia

class TestMatrix: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testInitDimension() {
        let A = Matrix(2,3)
        XCTAssertEqual(A.getNumberOfRows(), 2)
        XCTAssertEqual(A.getNumberOfColumns(), 3)
    }
    
    func testInit22() {
        let A = Matrix(1,2,3,4)
        XCTAssert(abs(A.get(0,0) - 1.0) < MathHelpers.EPSILON)
        XCTAssert(abs(A.get(0,1) - 2.0) < MathHelpers.EPSILON)
        XCTAssert(abs(A.get(1,0) - 3.0) < MathHelpers.EPSILON)
        XCTAssert(abs(A.get(1,1) - 4.0) < MathHelpers.EPSILON)
    }
    
    func testInit33() {
        let A = Matrix(1,2,3,4,5,6,7,8,9)
        XCTAssert(abs(A.get(0,0) - 1) < MathHelpers.EPSILON)
        XCTAssert(abs(A.get(0,1) - 2) < MathHelpers.EPSILON)
        XCTAssert(abs(A.get(0,2) - 3) < MathHelpers.EPSILON)
        XCTAssert(abs(A.get(1,0) - 4) < MathHelpers.EPSILON)
        XCTAssert(abs(A.get(1,1) - 5) < MathHelpers.EPSILON)
        XCTAssert(abs(A.get(1,2) - 6) < MathHelpers.EPSILON)
        XCTAssert(abs(A.get(2,0) - 7) < MathHelpers.EPSILON)
        XCTAssert(abs(A.get(2,1) - 8) < MathHelpers.EPSILON)
        XCTAssert(abs(A.get(2,2) - 9) < MathHelpers.EPSILON)
    }
    
    func testInit44() {
        let A = Matrix(1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16)
        XCTAssert(abs(A.get(0,0) - 1) < MathHelpers.EPSILON)
        XCTAssert(abs(A.get(0,1) - 2) < MathHelpers.EPSILON)
        XCTAssert(abs(A.get(0,2) - 3) < MathHelpers.EPSILON)
        XCTAssert(abs(A.get(0,3) - 4) < MathHelpers.EPSILON)
        XCTAssert(abs(A.get(1,0) - 5) < MathHelpers.EPSILON)
        XCTAssert(abs(A.get(1,1) - 6) < MathHelpers.EPSILON)
        XCTAssert(abs(A.get(1,2) - 7) < MathHelpers.EPSILON)
        XCTAssert(abs(A.get(1,3) - 8) < MathHelpers.EPSILON)
        XCTAssert(abs(A.get(2,0) - 9) < MathHelpers.EPSILON)
        XCTAssert(abs(A.get(2,1) - 10) < MathHelpers.EPSILON)
        XCTAssert(abs(A.get(2,2) - 11) < MathHelpers.EPSILON)
        XCTAssert(abs(A.get(2,3) - 12) < MathHelpers.EPSILON)
        XCTAssert(abs(A.get(3,0) - 13) < MathHelpers.EPSILON)
        XCTAssert(abs(A.get(3,1) - 14) < MathHelpers.EPSILON)
        XCTAssert(abs(A.get(3,2) - 15) < MathHelpers.EPSILON)
        XCTAssert(abs(A.get(3,3) - 16) < MathHelpers.EPSILON)
    }
    
    func testInitCoord() {
        let x = Vector(1,2,3)
        let y = Vector(4,5,6)
        let z = Vector(7,8,9)
        let A = Matrix(x, y, z)
        XCTAssert(abs(A.get(0,0) - 1) < MathHelpers.EPSILON)
        XCTAssert(abs(A.get(0,1) - 4) < MathHelpers.EPSILON)
        XCTAssert(abs(A.get(0,2) - 7) < MathHelpers.EPSILON)
        XCTAssert(abs(A.get(1,0) - 2) < MathHelpers.EPSILON)
        XCTAssert(abs(A.get(1,1) - 5) < MathHelpers.EPSILON)
        XCTAssert(abs(A.get(1,2) - 8) < MathHelpers.EPSILON)
        XCTAssert(abs(A.get(2,0) - 3) < MathHelpers.EPSILON)
        XCTAssert(abs(A.get(2,1) - 6) < MathHelpers.EPSILON)
        XCTAssert(abs(A.get(2,2) - 9) < MathHelpers.EPSILON)
    }
    
    func testCopy() {
        let A = Matrix(1,2,3,4)
        let B = Matrix(2,2);
        B.copy(A)
        XCTAssertEqual(B, A)
    }
    
    func testSet() {
        let A = Matrix(1,2,3,4)
        A.set(0,1,5)
        let B = Matrix(1,5,3,4)
        XCTAssertEqual(B, A)
    }
    
    func testMultiplyVector() {
        let A = Matrix(1,2,3,4)
        let x = Vector(1,2)
        let y = Vector(5,11)
        XCTAssertEqual(A.multiply(x), y)
    }
    
    func testMultiplyMatrix() {
        let A = Matrix(1,2,3,4)
        let B = Matrix(4,3,2,1)
        let C = Matrix(8,5,20,13)
        XCTAssertEqual(A.multiply(B), C)
    }
    
    func testMultiplyScalar() {
        let A = Matrix(1,2,3,4)
        let C = Matrix(2,4,6,8)
        XCTAssertEqual(A.multiply(2), C)
    }
    
    func testAdd() {
        let A = Matrix(1,2,3,4)
        let B = Matrix(4,3,2,1)
        let C = Matrix(5,5,5,5)
        XCTAssertEqual(A.add(B), C)
    }
    
    func testSubtract() {
        let A = Matrix(1,2,3,4)
        let B = Matrix(4,3,2,1)
        let C = Matrix(-3,-1,1,3)
        XCTAssertEqual(A.subtract(B), C)
    }
    
    func testGetTransposed() {
        let A = Matrix(2,3)
        A.set(0,0,1)
        A.set(0,1,2)
        A.set(0,2,3)
        A.set(1,0,4)
        A.set(1,1,5)
        A.set(1,2,6)
        let B = Matrix(3,2)
        B.set(0,0,1)
        B.set(0,1,4)
        B.set(1,0,2)
        B.set(1,1,5)
        B.set(2,0,3)
        B.set(2,1,6)
        XCTAssertEqual(A.getTransposed(), B)
    }
    
    func testData() {
        let A = Matrix(2,3)
        A.set(0,0,1)
        A.set(0,1,2)
        A.set(0,2,3)
        A.set(1,0,4)
        A.set(1,1,5)
        A.set(1,2,6)
        XCTAssertEqual(A.data(), [1.0,2.0,3.0,4.0,5.0,6.0])
    }
    
    func testFloatData() {
        let A = Matrix(2,3)
        A.set(0,0,1)
        A.set(0,1,2)
        A.set(0,2,3)
        A.set(1,0,4)
        A.set(1,1,5)
        A.set(1,2,6)
        XCTAssertEqual(A.floatData(), [1, 2, 3, 4, 5, 6])
    }
    
    func testInverse() {
        let A = Matrix(1,2,3,6,2,1,3,5,1)
        let B = Matrix(1,2,3,6,2,1,3,5,1,5,5,1,4,6,1,5)
        XCTAssertEqual(A.getInverse().multiply(A), Matrix.createIdentityMatrix3())
        XCTAssertEqual(A.multiply(A.getInverse()), Matrix.createIdentityMatrix3())
        XCTAssertEqual(B.getInverse().multiply(B), Matrix.createIdentityMatrix4())
        XCTAssertEqual(B.multiply(B.getInverse()), Matrix.createIdentityMatrix4())
    }
    
    func testmakeHomogenious() {
        let A = Matrix(1,2,3,4,5,6,7,8,9)
        let B = Matrix(1,2,3,0,4,5,6,0,7,8,9,0,0,0,0,1)
        XCTAssertEqual(Matrix.makeHomogenious(A), B)
    }
}
