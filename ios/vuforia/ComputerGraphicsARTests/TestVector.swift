//
//  TestVector.swift
//  ComputerGraphicsARTests
//
//  Created by Philipp Jenke on 20.11.17.
//  Copyright © 2017 PTC. All rights reserved.
//

import XCTest
@testable import Vuforia

class TestVector: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testInitDimension() {
        let a = Vector(7)
        XCTAssertEqual(a.getDimension(), 7)
    }
    
    func testInitCopy() {
        let a = Vector(1,2)
        let b = Vector(a)
        XCTAssertEqual(a, b)
    }
    
    func testInitVec2() {
        let a = Vector(1,2)
        XCTAssertEqual(a.getDimension(), 2)
        XCTAssertEqual(a.get(0), 1)
        XCTAssertEqual(a.get(1), 2)
    }
    
    func testInitVec3() {
        let a = Vector(1,2,3)
        XCTAssertEqual(a.getDimension(), 3)
        XCTAssertEqual(a.get(0), 1)
        XCTAssertEqual(a.get(1), 2)
        XCTAssertEqual(a.get(2), 3)
    }
    
    func testInitVec4() {
        let a = Vector(1,2,3,4)
        XCTAssertEqual(a.getDimension(), 4)
        XCTAssertEqual(a.get(0), 1)
        XCTAssertEqual(a.get(1), 2)
        XCTAssertEqual(a.get(2), 3)
        XCTAssertEqual(a.get(3), 4)
    }
    
    func testCopy() {
        let a = Vector(1,2,3)
        let b = Vector(3)
        b.copy(a)
        XCTAssertEqual(a, b)
    }
    
    func testSet() {
        let a = Vector(1,2,3)
        a.set(1, 4)
        let b = Vector(1,4,3)
        b.copy(a)
        XCTAssertEqual(a, b)
    }
    
    func testAdd() {
        let a = Vector(1,2,3)
        let b = Vector(-2, -1, 0)
        let expected = Vector(-1, 1, 3)
        XCTAssertEqual(expected, a.add(b))
    }
    
    func testSubtract() {
        let a = Vector(1,2,3);
        let b = Vector(0, -1, -2);
        let expected = Vector(1, 3, 5);
        XCTAssertEqual(expected, a.subtract(b))
    }
    
    func testGetNorm() {
        let a = Vector(1,1);
        XCTAssert((a.getNorm() - sqrt(2)) < MathHelpers.EPSILON)
    }
    
    func testSqrGetNorm() {
        let a = Vector(1,1);
        XCTAssert((a.getNorm() - 2) < MathHelpers.EPSILON)
    }
    
    func testDot() {
        let a = Vector(1,2,3)
        let b = Vector(-1, -2, -3)
        XCTAssertEqual(a.dot(b), -14)
    }
    
    func testMultiply() {
        let a = Vector(1,2,3)
        let b = Vector(2,4,6)
        XCTAssertEqual(a.multiply(2), b)
    }
    
    func testInnerProduct() {
        let a = Vector(1,2)
        let b = Vector(3,4)
        let A = Matrix(3.0, 4.0, 6.0, 8.0)
        XCTAssertEqual(a.innerProduct(b), A)
    }
    
    func testGetNormalized() {
        let a = Vector(0,2)
        let b = Vector(0,1)
        XCTAssertEqual(a.getNormalized(), b)
    }
    
    func testNormalize() {
        let a = Vector(0,2)
        a.normalize()
        let b = Vector(0,1)
        XCTAssertEqual(a, b)
    }
    
    func testCross() {
        let a = Vector(1,0,0)
        let b = Vector(0,1,0)
        let c = Vector(0,0,1)
        XCTAssertEqual(a.cross(b), c)
    }
    
    func testAddSelf() {
        let a = Vector(1,0,0)
        let b = Vector(0,1,1)
        a.addSelf(b)
        let c = Vector(1,1,1)
        XCTAssertEqual(a, c)
    }
    
    func testSubtractSelf() {
        let a = Vector(1,0,0)
        let b = Vector(0,1,1)
        a.subtractSelf(b)
        let c = Vector(1,-1,-1)
        XCTAssertEqual(a, c)
    }
    
    func testMultiplySelf() {
        let a = Vector(1,0,1)
        a.multiplySelf(2)
        let c = Vector(2,0,2)
        XCTAssertEqual(a, c)
    }
    
    func testFloatData() {
        let a = Vector(1,2,3)
        XCTAssertEqual(a.floatData(), [Float(1.0), Float(2.0), Float(3.0)])
    }
    
    func testData() {
        let a = Vector(1,2,3)
        XCTAssertEqual(a.floatData(), [1.0, 2.0, 3.0])
    }
    
    func testXyz() {
        let a = Vector(1,2,3,4)
        XCTAssert(abs(a.x() - 1) < MathHelpers.EPSILON)
        XCTAssert(abs(a.y() - 2) < MathHelpers.EPSILON)
        XCTAssert(abs(a.z() - 3) < MathHelpers.EPSILON)
        let b = Vector(1,2,3)
        XCTAssertEqual(a.xyz(), b)
    }
    
    func testMakeHomogenious() {
        let a = Vector(1,2,3)
        let b = Vector(1,2,3,1)
        XCTAssertEqual(Vector.makeHomogenious(a), b)
    }
}
