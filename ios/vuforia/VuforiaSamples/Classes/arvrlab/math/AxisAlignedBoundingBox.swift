//
//  AxisAlignedBoundingBox.swift
//  VuforiaSamples
//
//  Created by Philipp Jenke on 20.11.17.
//  Copyright © 2017 PTC. All rights reserved.
//

import Foundation

/**
 * Represents an axis-aligned bounding box with lower left and upper right corner.
 *
 * @author Philipp Jenke
 */

public class AxisAlignedBoundingBox {
    /**
     * Lower left corner.
     */
    private var ll : Vector?;
    
    /**
     * Upper right corner
     */
    private var ur : Vector?;
    
    init() {
    }
    
    init(_ bbox: AxisAlignedBoundingBox) {
        self.ll = Vector(bbox.ll!);
        self.ur = Vector(bbox.ur!);
    }
    
    init(_ ll: Vector, _ ur: Vector) {
        self.ll = Vector(ll);
        self.ur = Vector(ur);
    }
    
    func getLL() ->Vector? {
        return ll;
    }
    
    func getUR() -> Vector?{
        return ur;
    }
    
    func add(_ point: Vector) {
        if ll == nil {
            ll = Vector(point);
        }
        if ur == nil {
            ur = Vector(point);
        }
        for i in 0 ... 2{
            if (point.get(i) < ll!.get(i)) {
                ll!.set(i, point.get(i));
            }
            if (point.get(i) > ur!.get(i)) {
                ur!.set(i, point.get(i));
            }
        }
    }
    
    func getExtend() -> Vector? {
        if let l = ll {
            if let u = ur {
               return u.subtract(l);
            }
        }
        return nil
    }
    
    func getCenter() -> Vector? {
        if let l = ll {
            if let u = ur {
                return l.add(u).multiply(0.5);
            }
        }
        return nil
    }
    
    func transform(_ transformation: Matrix) {
        if ll == nil || ur == nil {
            return
        }
        
        var transformedPoints = [Vector]();
        transformedPoints.append(transformation.multiply( Vector(ll!.x(), ll!.y(), ll!.z(), 1)).xyz());
        transformedPoints.append(transformation.multiply( Vector(ll!.x(), ur!.y(), ll!.z(), 1)).xyz());
        transformedPoints.append(transformation.multiply( Vector(ur!.x(), ur!.y(), ll!.z(), 1)).xyz());
        transformedPoints.append(transformation.multiply( Vector(ur!.x(), ll!.y(), ll!.z(), 1)).xyz());
    
        transformedPoints.append(transformation.multiply( Vector(ll!.x(), ll!.y(), ur!.z(), 1)).xyz());
        transformedPoints.append(transformation.multiply( Vector(ll!.x(), ur!.y(), ur!.z(), 1)).xyz());
        transformedPoints.append(transformation.multiply( Vector(ur!.x(), ur!.y(), ur!.z(), 1)).xyz());
        transformedPoints.append(transformation.multiply( Vector(ur!.x(), ll!.y(), ur!.z(), 1)).xyz());
    
        ll =  Vector(MathHelpers.MAX_DOUBLE, MathHelpers.MAX_DOUBLE, MathHelpers.MAX_DOUBLE);
        ur =  Vector(MathHelpers.MIN_DOUBLE, MathHelpers.MIN_DOUBLE, MathHelpers.MIN_DOUBLE);
        for v in transformedPoints {
            for i in 0 ... 2 {
                ll!.set(i, min(ll!.get(i), v.get(i)));
                ur!.set(i, max(ur!.get(i), v.get(i)));
            }
        }
    }
    
    func clone() ->AxisAlignedBoundingBox? {
        if ll == nil || ur == nil {
            return nil
        }
        return AxisAlignedBoundingBox(ll!, ur!);
    }
    
    /**
     * Unite with second bbox.
     */
    func add(_ boundingBox: AxisAlignedBoundingBox) {
        if let l = boundingBox.getLL() {
            add(l);
        }
        if let u = boundingBox.getUR() {
            add(u);
        }
    }
}
