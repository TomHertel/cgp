//
//  MathHelpers.swift
//  VuforiaSamples
//
//  Created by Philipp Jenke on 19.11.17.
//  Copyright © 2017 PTC. All rights reserved.
//

import Foundation

class MathHelpers {
    /**
     * Numerical precision.
     */
    static let EPSILON = 0.0000001;
    static let DIMENSION_2 = 2;
    static let DIMENSION_3 = 3;
    static let DIMENSION_4 = 4;
    static let INDEX_0 = 0;
    static let  INDEX_1 = 1;
    static let  INDEX_2 = 2;
    static let  INDEX_3 = 3;
    static let  ONE_HUNDRET_EIGHTY = 180.0;
    // TODO: Hack!
    static let  MAX_DOUBLE = 10000000.0;
    // TODO: Hack!
    static let  MIN_DOUBLE = -10000000.0;
    
    static let VECTOR_3_X = Vector(1.0, 0.0, 0.0);
    static let VECTOR_3_Y = Vector(0.0, 1.0, 0.0);
    static let VECTOR_3_Z = Vector(0.0, 0.0, 1.0);
}
