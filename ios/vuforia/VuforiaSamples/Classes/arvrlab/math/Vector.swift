//
//  Vector.swift
//  VuforiaSamples
//
//  Created by Philipp Jenke on 19.11.17.
//  Copyright © 2017 PTC. All rights reserved.
//

import Foundation

public class Vector : Equatable {
   
    /**
     * Array contaising the values of the vector. Length of the array matches the
     * vector dimension.
     */
    private var values = [Double]();
    
    public init(_ dim: Int) {
        setDimension(dim);
        for index in 0 ... getDimension()-1 {
            set(index, 0);
        }
    }
    
    /**
     * Copy contructor
     *
     * @param other Vector to be copied from.
     */
    public init(_ other: Vector) {
        setDimension(other.getDimension());
        for index in 0 ... getDimension()-1 {
            set(index, other.get(index));
        }
    }
    
    /**
     * Convenience constructor for 2-dimensional vectors.
     *
     * @param x x-coordinate.
     * @param y y-coordinate.
     */
    public init(_ x: Double,_  y: Double) {
        setDimension(2);
        set(0, x);
        set(1, y);
    }
    
    /**
     * Convenience constructor for 3-dimensional vectors.
     *
     * @param x x-coordinate.
     * @param y y-coordinate.
     * @param z z-coordinate.
     */
    public init( _ x: Double, _ y: Double, _ z: Double) {
        setDimension(3);
        set(0, x);
        set(1, y);
        set(2, z);
    }
    
    /**
     * Convenience constructor for 3-dimensional vectors.
     *
     * @param x x-coordinate.
     * @param y y-coordinate.
     * @param z z-coordinate.
     * @param w w-coordinate (forth coordinate).
     */
    public init(_ x: Double, _ y: Double, _ z: Double, _ w: Double) {
        setDimension(4);
        set(0, x);
        set(1, y);
        set(2, z);
        set(3, w);
    }
    
    private func setDimension(_ dim: Int){
        values = [Double]()
        for _ in 1 ... dim {
            values.append(0)
        }
    }
    
    /**
     * Copy coordinates of other vector.
     *
     * @param other Vector to copy from.
     */
    public func copy(_ other: Vector) {
        if (other.getDimension() != getDimension()) {
            preconditionFailure("Dimensions must match")
        }
        for i in 0 ... getDimension()-1 {
            set(i, other.get(i));
        }
    }
    
    /**
     * Getter for the dimension.
     *
     * @return Dimension of the vector.
     */
    public  func getDimension() -> Int {
        return values.count;
    }
    
    /**
     * Getter for a specified coordinate.
     *
     * @param index Index of the coordinate.
     * @return Coordinate value.
     */
    public func get(_ index: Int) -> Double{
        return values[index];
    }
    
    /**
     * Setter for value.
     *
     * @param index Coordinate index
     * @param value new value.
     */
    public func set(_ index: Int, _ value: Double) {
        values[index] = value;
    }
    
    /**
     * Subtract other vector, return result as new vector.
     *
     * @param other Vector to be subtracted.
     * @return Vector containing the result.
     */
    public func subtract(_ other: Vector) -> Vector {
        if (other.getDimension() != getDimension()) {
            preconditionFailure("Dimensions must match")
        }
        let result = Vector(getDimension());
        for index in 0 ...  getDimension()-1 {
            result.set(index, get(index) - other.get(index) );
        }
        return result;
    }
    
    /**
     * Add other vector, return result as new vector.
     *
     * @param other Vector to be added.
     * @return Vector containing the result.
     */
    public func add(_ other: Vector) -> Vector {
        if (other.getDimension() != getDimension()) {
            preconditionFailure("Dimensions must match")
        }
        let result =  Vector(getDimension());
        for index in 0 ... getDimension()-1{
            result.set(index, get(index) + other.get(index));
        }
        return result;
    }
    
    /**
     * Getter for norm of the vector.
     *
     * @return Norm (length) of the vector.
     */
    public func getNorm() -> Double{
        return sqrt(getSqrNorm());
    }
    
    /**
     * Getter for squared norm of the vector.
     *
     * @return Squared norm (squared length).
     */
    public func getSqrNorm() -> Double{
        var norm = 0.0;
        for index in 0 ... getDimension()-1 {
            norm += get(index) * get(index);
        }
        return norm;
    }
    
    /**
     * Multiply other vector, compute scalar product.
     *
     * @param other Vector to be mutiplied.
     * @return Scalar product of the two vector.
     */
    public func dot(_ other: Vector) -> Double{
        if ( other.getDimension() != getDimension()) {
            preconditionFailure("Dimensions must match")
        }
        var result = 0.0;
        for index in 0 ... getDimension()-1{
            result += get(index) * other.get(index);
        }
        return result;
    }
    
    /**
     * Scale vector, return result as new vector.
     *
     * @param factor Scaling factor.
     * @return Scaled vector.
     */
    public func multiply(_ factor: Double) -> Vector {
        let result = Vector(getDimension());
        for index in 0 ... getDimension()-1 {
            result.set(index, get(index) * factor);
        }
        return result;
    }
    
    /**
     * Compute the inner product of the vector with another vector.
     *
     * @param other Other vector
     * @return Resulting matrix.
     */
    public func innerProduct(_ other: Vector) -> Matrix {
        let M = Matrix(other.getDimension(), other.getDimension());
        for  rowIndex in 0 ... other.getDimension()-1 {
            for columnIndex in 0 ... other.getDimension() - 1 {
                M.set(rowIndex, columnIndex, get(rowIndex) * other.get(columnIndex));
            }
        }
        return M;
    }
    
    /**
     * Create a normalized version of the vector, return as result.
     *
     * @return Normalized vector.
     */
    public func getNormalized() -> Vector{
        let d = getNorm();
        if (abs(d) < MathHelpers.EPSILON) {
            //Log.i(Constants.LOGTAG, "Cannot normalize zero-vector!");
            return self;
        }
        return multiply(1.0 / d);
    }
    
    /**
     * Normalize the vector.
     */
    public func normalize() {
        let norm = getNorm();
        for i in 0 ... getDimension()-1{
            values[i] /= norm;
        }
    }
    
    /**
     * Compute the cross product of two vectors (only on 3-space).
     *
     * @param other Vector to be computed with
     * @return Cross product result vector.
     */
   public  func cross( _ other: Vector) -> Vector {
        if (getDimension() != 3 || other.getDimension() != 3) {
            preconditionFailure("Dimensions must match")
        }
        let result = Vector(3);
        result.set(MathHelpers.INDEX_0,
                   get(MathHelpers.INDEX_1) * other.get(MathHelpers.INDEX_2)
                    - get(MathHelpers.INDEX_2) * other.get(MathHelpers.INDEX_1));
        result.set(MathHelpers.INDEX_1,
                   get(MathHelpers.INDEX_2) * other.get(MathHelpers.INDEX_0)
                    - get(MathHelpers.INDEX_0) * other.get(MathHelpers.INDEX_2));
        result.set(MathHelpers.INDEX_2,
                   get(MathHelpers.INDEX_0) * other.get(MathHelpers.INDEX_1)
                    - get(MathHelpers.INDEX_1) * other.get(MathHelpers.INDEX_0));
        return result;
    }
    
    /**
     * Add another vector to this-object, change this coordinates.
     *
     * @param other Vector to be added.
     */
    public func addSelf(_ other: Vector) {
        if (other.getDimension() != getDimension()) {
            preconditionFailure("Dimensions must match")
        }
        for i in 0 ...  getDimension()-1{
            set(i, get(i) + other.get(i));
        }
    }
    
    /**
     * Subtract another vector from this-object, change this coordinates.
     *
     * @param other Vector to be added.
     */
    public func subtractSelf(_ other: Vector) {
        if (other.getDimension() != getDimension()) {
            preconditionFailure("Dimensions must match")
        }
        for i in 0 ... getDimension()-1 {
            set(i, get(i) - other.get(i));
        }
    }
    
    /**
     * Scale vector, change this.
     *
     * @param d Scaling factor.
     */
    public func multiplySelf(_ d: Double) {
        for  i in 0 ... getDimension()-1{
            set(i, get(i) * d);
        }
    }
    
    /**
     * Create array of the coordinates in float format.
     *
     * @return Coordinate array.
     */
    public func floatData() -> [Float] {
        var floatData = [Float]();
        for  i  in 0 ... values.count-1{
            floatData.append(Float(values[i]));
        }
        return floatData;
    }
    
    /**
     * Create array of the coordinates in double format.
     *
     * @return Coordinate array.
     */
    public func data() -> [Double] {
        var data = [Double]();
        for i in 0 ... values.count - 1 {
            data.append(values[i]);
        }
        return data;
    }
    
    /**
     * Specialized version of the toString() method with given precision for the
     * coordinates.
     *
     * @param precision Number of digits after the . or ,
     * @return String representation of the vector.
     */
    public func toString(precision: Int) -> String{
        var result = "( ";
        for i in 0 ... getDimension()-1{
            result += String(format: "%.\(precision)f ", values[i]);
        }
        result += ")\n";
        return result;
    }
    
    public func toString() -> String{
        var content = "( ";
        for index in  0 ...  getDimension()-1{
            content += String(format: "%4.3f ", get(index));
        }
        content += ")\n";
        return content;
    }
    
    
    /**
     * Set the coordinates of a 3D vector.
     */
    public func set(x: Double, y: Double, z: Double) {
        set(0, x);
        set(1, y);
        set(2, z);
    }
    
    public func x() -> Double {
        return get(0);
    }
    
    public func y() -> Double{
        return get(1);
    }
    
    public func z() -> Double {
        return get(2);
    }
    
    public func  w() -> Double {
        return get(3);
    }
    
    /**
     * Create a new vector from the first 3 coordinates.
     */
    public func xyz() -> Vector {
        return Vector(x(), y(), z());
    }
    
    /**
     * Create a homogenious 4-vector from an xyz-vector.
     */
    public static func makeHomogenious(_ v: Vector) -> Vector{
        return Vector(v.x(), v.y(), v.z(), 1);
    }
    
    /**
     * Equality
     */
    public static func ==(lhs: Vector, rhs: Vector) -> Bool {
        if lhs.getDimension() != rhs.getDimension() {
            return false
        }
        for i in 0 ... lhs.getDimension() - 1 {
            if abs(lhs.get(i) - rhs.get(i)) > MathHelpers.EPSILON {
                return false
            }
        }
        return true
    }
}
