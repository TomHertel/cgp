//
//  Matrix.swift
//  VuforiaSamples
//
//  Created by Philipp Jenke on 19.11.17.
//  Copyright © 2017 PTC. All rights reserved.
//

import Foundation
import Accelerate

public class Matrix : Equatable {
    
    /**
     * Coordinates as 3D aray with row-based order (values[0] contains the values
     * of the first row).
     */
    private var values = [[Double]]();
    
    /**
     * Create matrix with given dimensions.
     *
     * @param numberOfRows    Number of rows.
     * @param numberOfColumns Number of columns.
     */
    public init(_ numberOfRows: Int, _ numberOfColumns: Int) {
        create(numberOfRows, numberOfColumns)
    }
    
    /**
     * Constructor for 2x2 matrix.
     */
    public init(_ v00: Double, _ v01: Double, _ v10: Double, _ v11: Double) {
        create(2, 2);
        set(0, 0, v00);
        set(0, 1, v01);
        set(1, 0, v10);
        set(1, 1, v11);
    }
    
    /**
     * Constructor for 3x3 matrix.
     */
    public init(_ v00: Double, _ v01: Double, _ v02: Double, _ v10: Double, _ v11: Double, _ v12: Double, _ v20: Double, _ v21: Double,
         _ v22: Double) {
        create(3, 3);
        set(0, 0, v00);
        set(0, 1, v01);
        set(0, 2, v02);
        set(1, 0, v10);
        set(1, 1, v11);
        set(1, 2, v12);
        set(2, 0, v20);
        set(2, 1, v21);
        set(2, 2, v22);
    }
    
    /**
     * Constructor for 4x4 matrix.
     */
    public init(_ v00: Double, _ v01: Double, _ v02: Double, _ v03: Double, _ v10: Double, _ v11: Double, _ v12: Double, _ v13: Double,
         _ v20: Double, _ v21: Double, _ v22: Double, _ v23: Double, _ v30: Double, _ v31: Double, _ v32: Double, _ v33: Double) {
        create(4, 4);
        set(0, 0, v00);
        set(0, 1, v01);
        set(0, 2, v02);
        set(0, 3, v03);
        set(1, 0, v10);
        set(1, 1, v11);
        set(1, 2, v12);
        set(1, 3, v13);
        set(2, 0, v20);
        set(2, 1, v21);
        set(2, 2, v22);
        set(2, 3, v23);
        set(3, 0, v30);
        set(3, 1, v31);
        set(3, 2, v32);
        set(3, 3, v33);
    }
    
    /**
     * Copy constructor.
     *
     * @param other Matrix to be cloned.
     */
    public init(_ other: Matrix) {
        create(other.getNumberOfRows(), other.getNumberOfColumns());
        copy(other);
    }
    
    /**
     * Init from 16-array.
     *
     * @param data Array with 16 doubles.
     */
    public init(_ data: [Double]) {
        create(4, 4);
        set(0, 0, data[0]);
        set(0, 1, data[1]);
        set(0, 2, data[2]);
        set(0, 3, data[3]);
        set(1, 0, data[4]);
        set(1, 1, data[5]);
        set(1, 2, data[6]);
        set(1, 3, data[7]);
        set(2, 0, data[8]);
        set(2, 1, data[9]);
        set(2, 2, data[10]);
        set(2, 3, data[11]);
        set(3, 0, data[12]);
        set(3, 1, data[13]);
        set(3, 2, data[14]);
        set(3, 3, data[15]);
    }
    
    /**
     * Init from 16-array.
     *
     * @param data Array with 16 floats.
     */
    public init(_ data: [Float]) {
        create(4, 4);
        set(0, 0, Double(data[0]));
        set(0, 1, Double(data[1]));
        set(0, 2, Double(data[2]));
        set(0, 3, Double(data[3]));
        set(1, 0, Double(data[4]));
        set(1, 1, Double(data[5]));
        set(1, 2, Double(data[6]));
        set(1, 3, Double(data[7]));
        set(2, 0, Double(data[8]));
        set(2, 1, Double(data[9]));
        set(2, 2, Double(data[10]));
        set(2, 3, Double(data[11]));
        set(3, 0, Double(data[12]));
        set(3, 1, Double(data[13]));
        set(3, 2, Double(data[14]));
        set(3, 3, Double(data[15]));
    }
    
    public init(_ dir: Vector, _ up: Vector, _ right: Vector) {
        create(3, 3);
        for i in 0 ... 2 {
            set(i, 0, dir.get(i));
            set(i, 1, up.get(i));
            set(i, 2, right.get(i));
        }
    }
    
    public func create(_ numRows: Int, _ numCols: Int) {
        if (numRows < 1 || numCols < 1) {
            preconditionFailure("Invalid dimension")
        }
        values = Array(repeating: Array(repeating: 0, count: numCols), count: numRows)
    }
    
    /**
     * Copy matrix content (dimensions must match!).
     */
    public func copy(_ other: Matrix) {
        if (getNumberOfRows() != other.getNumberOfRows() || getNumberOfColumns() != other.getNumberOfColumns()) {
            preconditionFailure("Invalid dimension")
        }
        for rowIndex in 0 ... getNumberOfRows()-1 {
            for columnIndex in 0 ... getNumberOfColumns()-1 {
                set(rowIndex, columnIndex, other.get(rowIndex, columnIndex));
            }
        }
    }
    
    public func getNumberOfRows() -> Int{
        return values.count;
    }
    
    public func getNumberOfColumns() ->Int {
        return values[0].count;
    }
    
    /**
     * Get the coordinate at the specified position.
     *
     * @param rowIndex    Row index (0-based).
     * @param columnIndex Column index (0-based).
     * @return Coordinate at the specified position.
     */
    public func get(_ rowIndex: Int, _ columnIndex: Int) -> Double{
        return values[rowIndex][columnIndex];
    }
    
    /**
     * Set the coordinate at the specified position.
     *
     * @param rowIndex    Row index (0-based).
     * @param columnIndex Column index (0-based).
     * @return Coordinate at the specified position.
     */
    public func set(_ rowIndex: Int, _ columnIndex: Int, _ value: Double) {
        values[rowIndex][columnIndex] = value;
    }
    
    /**
     * Multiply this with vector, return result as new vector.
     *
     * @param other Vector to be multiplied
     * @return New vector containing the result.
     */
    public func multiply(_ other: Vector) -> Vector {
        if (getNumberOfColumns() != other.getDimension()) {
            preconditionFailure("Invalid dimension")
        }
        let result = Vector(getNumberOfRows());
        for rowIndex in 0 ... getNumberOfRows()-1 {
            var value = 0.0;
            for columnIndex in 0 ... getNumberOfColumns()-1 {
                value += get(rowIndex, columnIndex) * other.get(columnIndex);
            }
            result.set(rowIndex, value);
        }
        return result;
    }
    
    /**
     * Multiply this with other matrix, return result as new matrix.
     *
     * @param other Matrix to be multiplied.
     * @return New matrix containing the result.
     */
    public func multiply(_ other: Matrix) -> Matrix {
        if (getNumberOfColumns() != other.getNumberOfRows()) {
            preconditionFailure("Invalid dimension")
        }
        let result = Matrix(getNumberOfRows(), other.getNumberOfColumns());
        for rowIndex in 0 ... getNumberOfRows()-1 {
            for columnIndex in 0 ... other.getNumberOfColumns()-1 {
                var value = 0.0;
                for i in 0 ... getNumberOfColumns()-1 {
                    value += get(rowIndex, i) * other.get(i, columnIndex);
                }
                result.set(rowIndex, columnIndex, value);
            }
        }
        return result;
    }
    
    /**
     * Scale matrix, return result as new matrix.
     *
     * @param d Scaling factor.
     * @return New scaled vertex.
     */
    public func multiply(_ d: Double) -> Matrix {
        let result = Matrix(getNumberOfRows(), getNumberOfColumns());
        for row in  0 ... getNumberOfRows()-1 {
            for column in 0 ... getNumberOfColumns()-1 {
                result.set(row, column, get(row, column) * d);
            }
        }
        return result;
    }
    
    /**
     * Add other matrix, return result as new matrix.
     *
     * @param other Matrix to be added.
     * @return New matrix containing the result.
     */
    public func add(_ other: Matrix) -> Matrix {
        let result = Matrix(getNumberOfRows(), getNumberOfColumns());
        for i in 0 ... result.getNumberOfRows()-1 {
            for j in 0 ... result.getNumberOfColumns()-1 {
                result.set(i, j, get(i, j) + other.get(i, j));
            }
        }
        return result
    }
    
    /**
     * Subtract other matrix, return result as new matrix.
     *
     * @param other Matrix to be added.
     * @return New matrix containing the result.
     */
    public func subtract(_ other: Matrix) -> Matrix{
        let result = Matrix(getNumberOfRows(), getNumberOfColumns());
        for i in 0 ... result.getNumberOfRows() - 1 {
            for j in 0 ... result.getNumberOfColumns()-1 {
                result.set(i, j, get(i, j) - other.get(i, j));
            }
        }
        return result;
    }
    
    /**
     * Compute and return the transposed of the matrix.
     *
     * @return New matrix containing the transposed.
     */
    public func getTransposed() -> Matrix{
        let result = Matrix(getNumberOfColumns(), getNumberOfRows());
        for rowIndex in 0 ... getNumberOfRows()-1 {
            for columnIndex in 0 ... getNumberOfColumns()-1{
                result.set(columnIndex, rowIndex, get(rowIndex, columnIndex));
            }
        }
        return result;
    }
    
    /**
     * Return 1-dimensional row-based array of the matrix coordinates.
     *
     * @return Array with coordinates.
     */
    public func data() -> [Double] {
        var data = [Double]();
        for row in 0 ... getNumberOfRows()-1 {
            for col in 0 ... getNumberOfColumns()-1 {
                data.append(get(row, col));
            }
        }
        return data;
    }
    
    public func floatData() -> [Float]{
        var data = [Float]();
        for row in 0 ... getNumberOfRows()-1 {
            for col in 0 ... getNumberOfColumns()-1 {
                data.append(Float(get(row, col)));
            }
        }
        return data;
    }
    
    
    public func getInverse() -> Matrix{
        if (getNumberOfRows() != getNumberOfColumns()) {
            preconditionFailure("Invalid dimension")
        }
        if (getNumberOfRows() == 3) {
            let det = getDeterminant();
            let a = get(0, 0);
            let b = get(0, 1);
            let c = get(0, 2);
            let d = get(1, 0);
            let e = get(1, 1);
            let f = get(1, 2);
            let g = get(2, 0);
            let h = get(2, 1);
            let i = get(2, 2);
            let inverse = Matrix(e * i - f * h, c * h - b * i, b * f - c * e, f * g - d * i, a * i - c * g,
                                 c * d - a * f, d * h - e * g, b * g - a * h, a * e - b * d).multiply(1.0 / det);
            return inverse;
        } else {
            var inMatrix = [Double]()
            for row in 0 ... getNumberOfRows()-1 {
                for col in 0 ... getNumberOfColumns()-1 {
                    inMatrix.append(get(row, col));
                }
            }
            // Get the dimensions of the matrix. An NxN matrix has N^2
            // elements, so sqrt( N^2 ) will return N, the dimension
            var N:__CLPK_integer        = __CLPK_integer( getNumberOfRows() )
            // Initialize some arrays for the dgetrf_(), and dgetri_() functions
            var pivots:[__CLPK_integer] = [__CLPK_integer](repeating: 0, count: Int(N) )
            var workspace:[Double]      = [Double](repeating: 0.0, count: Int(N))
            var error: __CLPK_integer   = 0
            // Perform LU factorization
            dgetrf_(&N, &N, &inMatrix, &N, &pivots, &error)
            // Calculate inverse from LU factorization
            dgetri_(&N, &inMatrix, &N, &pivots, &workspace, &N, &error)
            
            let result = Matrix(getNumberOfRows(), getNumberOfRows())
            for row in 0 ... getNumberOfRows()-1 {
                for col in 0 ... getNumberOfColumns()-1 {
                    let value = inMatrix[row * getNumberOfColumns() + col]
                    result.set(row, col, value);
                }
            }
            return result
        }
    }
    
    /**
     * Compute and return the matrix determinant. Matrix must be squared.
     *
     * @return Determinant of the matrix.
     */
    public func getDeterminant() -> Double {
        if (getNumberOfRows() != getNumberOfColumns()) {
            preconditionFailure("Invalid dimension")
        }
        if (getNumberOfColumns() == 3) {
            return get(0, 0) * get(1, 1) * get(2, 2) + get(0, 1) * get(1, 2) * get(2, 0) + get(0, 2) * get(1, 0) * get(2, 1)
                - get(0, 2) * get(1, 1) * get(2, 0) - get(0, 1) * get(1, 0) * get(2, 2) - get(0, 0) * get(1, 2) * get(2, 1);
        } else {
            preconditionFailure("Determinat only implemented for dimension 3")
        }
    }
    
    /**
     * Create a homogenious 4x4 matrix from a 3x3 matrix.
     */
    public static func makeHomogenious(_ A: Matrix) -> Matrix{
        if (A.getNumberOfRows() != 3 || A.getNumberOfColumns() != 3) {
            preconditionFailure("Invalid dimension")
        }
        
        return Matrix(A.get(0, 0), A.get(0, 1), A.get(0, 2), 0, A.get(1, 0), A.get(1, 1), A.get(1, 2), 0, A.get(2, 0),
                      A.get(2, 1), A.get(2, 2), 0, 0, 0, 0, 1);
    }
    
    /**
     * Create a 3x3 rotation matrix from an axis and an angle.
     */
    public static func createRotationMatrix3(_ axis: Vector, _ angle: Double) -> Matrix{
        let s = sin(angle);
        let c = cos(angle);
        let t = 1.0 - c;
        return Matrix(t * axis.get(0) * axis.get(0) + c, t * axis.get(0) * axis.get(1) - s * axis.get(2),
                      t * axis.get(0) * axis.get(2) + s * axis.get(1), t * axis.get(0) * axis.get(1) + s * axis.get(2),
                      t * axis.get(1) * axis.get(1) + c, t * axis.get(1) * axis.get(2) - s * axis.get(0),
                      t * axis.get(0) * axis.get(2) - s * axis.get(1), t * axis.get(2) * axis.get(1) + s * axis.get(0),
                      t * axis.get(2) * axis.get(2) + c);
    }
    
    /**
     * Create a 4x4 homogenious rotation matrix.
     */
    public static func createRotationMatrix4(_ axis: Vector, _ angle: Double) -> Matrix{
        return makeHomogenious(createRotationMatrix3(axis, angle));
    }
    
    /**
     * Create 4x4 homogenious scaling matrix.
     */
    public static func createScaleMatrix4(_ scale: Vector) -> Matrix{
        return Matrix(scale.x(), 0, 0, 0, 0, scale.y(), 0, 0, 0, 0, scale.z(), 0, 0, 0, 0, 1);
    }
    
    /**
     * Create 4x4 homogenious translation matrix.
     */
    public static func createTranslationMatrix4(_ translation: Vector) -> Matrix{
        return Matrix(1, 0, 0, translation.x(), 0, 1, 0, translation.y(), 0, 0, 1, translation.z(), 0, 0, 0, 1);
    }
    
    /**
     * Create 3x3 identity.
     */
    static func createIdentityMatrix3() -> Matrix{
        return Matrix(1, 0, 0, 0, 1, 0, 0, 0, 1);
    }
    
    /**
     * Create 4x4 identity.
     */
    public static func createIdentityMatrix4() -> Matrix{
        return Matrix(1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1);
    }
    
    /**
     * Set all values for a 4x4 matrix.
     */
    public func set(v0: Float, v1: Float, v2: Float, v3: Float,
             v4: Float, v5: Float, v6: Float, v7: Float,
             v8: Float, v9: Float, v10: Float, v11: Float,
             v12: Float, v13: Float, v14: Float, v15: Float) {
        set(0, 0, Double(v0));
        set(0, 1, Double(v1));
        set(0, 2, Double(v2));
        set(0, 3, Double(v3));
        set(1, 0, Double(v4));
        set(1, 1, Double(v5));
        set(1, 2, Double(v6));
        set(1, 3, Double(v7));
        set(2, 0, Double(v8));
        set(2, 1, Double(v9));
        set(2, 2, Double(v10));
        set(2, 3, Double(v11));
        set(3, 0, Double(v12));
        set(3, 1, Double(v13));
        set(3, 2, Double(v14));
        set(3, 3, Double(v15));
    }
    
    /**
     * Equality
     */
    public static func ==(lhs: Matrix, rhs: Matrix) -> Bool {
        if (lhs.getNumberOfRows() != rhs.getNumberOfRows()) || (lhs.getNumberOfColumns() != rhs.getNumberOfColumns()) {
            return false
        }
        for row in 0 ... lhs.getNumberOfRows() - 1 {
            for col in 0 ... lhs.getNumberOfColumns() - 1 {
                if abs(lhs.get(row, col) - rhs.get(row, col)) > MathHelpers.EPSILON {
                    return false
                }
            }
        }
        return true
    }
    
    public var description: String {
        var desc = ""
        for row in 0 ... getNumberOfRows()-1 {
            for col in 0 ... getNumberOfColumns()-1 {
                desc = desc + "\(get(row, col)) ";
            }
            desc = desc + "\n"
        }
        return desc
        
    }
}
