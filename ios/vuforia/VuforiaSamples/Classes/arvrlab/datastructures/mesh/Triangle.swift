//
//  Triangle.swift
//  ComputergraphicsAR
//
//  Created by Philipp Jenke on 22.11.17.
//  Copyright © 2017 PTC. All rights reserved.
//

import Foundation

/**
 * Representation of a triangle consisting of three indices. The indices
 * reference vertices in the vertex list in a triangle mesh.
 *
 * @author Philipp Jenke
 */
public class Triangle : AbstractTriangle {
    
    /**
     * Indices of the vertices.
     */
    private var vertexIndices = [-1, -1, -1];
    
    public init(_ a: Int, _ b: Int, _ c: Int) {
        super.init()
        vertexIndices[0] = a;
        vertexIndices[1] = b;
        vertexIndices[2] = c;
    }
    
    public init(_ a: Int, _ b: Int, _ c: Int, _ tA: Int, _ tB: Int, _ tC: Int) {
        super.init(tA, tB, tC, Vector(0,1,0))
        vertexIndices[0] = a;
        vertexIndices[1] = b;
        vertexIndices[2] = c;
    }
    
    public init(_ a: Int, _ b: Int, _ c: Int, _ tA: Int, _ tB: Int, _ tC: Int, _ normal: Vector) {
        super.init(tA, tB, tC, normal);
        vertexIndices[0] = a;
        vertexIndices[1] = b;
        vertexIndices[2] = c;
    }
    
    public init(_ triangle: Triangle) {
        super.init(triangle);
        vertexIndices[0] = triangle.vertexIndices[0];
        vertexIndices[1] = triangle.vertexIndices[1];
        vertexIndices[2] = triangle.vertexIndices[2];
    }
    
    
    public func toString() -> String {
        return "Triangle";
    }
    
    public func getVertexIndex(_ index: Int) -> Int {
        return vertexIndices[index];
    }
    
    /**
     * Add an offset to all vertex indices.
     */
    public func addVertexIndexOffset(offset: Int) {
        for i in 0 ... 2 {
            vertexIndices[i] += offset;
        }
    }
}
