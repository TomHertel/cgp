//
//  TriangleMesh.swift
//  ComputergraphicsAR
//
//  Created by Philipp Jenke on 22.11.17.
//  Copyright © 2017 PTC. All rights reserved.
//

import Foundation

/**
 * Implementation of a indexed vertex list triangle mesh.
 */
public class TriangleMesh : ITriangleMesh {
    
    /**
     * Vertices
     */
    private var vertices = [Vertex]();
    
    /**
     * Triangles.
     */
    private var triangles = [Triangle]();
    
    /**
     * Texture coordinates.
     */
    private var textureCoordinates = [Vector]();
    
    /**
     * Texture object, leave null if no texture is used.
     */
    private var textureName: String?
    
    public init() {
    }
    
    public init (_ textureName: String) {
        self.textureName = textureName;
    }
    
    /**
     * Copy constructor
     */
    public init(_ mesh: TriangleMesh) {
        // Vertices
        for i in 0 ... mesh.getNumberOfVertices() - 1 {
            _ = addVertex(Vertex(mesh.getVertex(i)))
        }
        // Texture coordinates
        for i in 0 ... mesh.getNumberOfTextureCoordinates() - 1 {
            addTextureCoordinate(Vector(mesh.getTextureCoordinate(i)));
        }
        // Triangles
        for i in 0 ... mesh.getNumberOfTriangles() - 1 {
            if let triangle = mesh.getTriangle(i) as? Triangle {
                addTriangle(Triangle(triangle));
            }
        }
        
    }
    
    public func clear() {
        vertices = [Vertex]();
        triangles = [Triangle]();
        textureCoordinates  = [Vector]();
    }
    
    public func addTriangle(_ vertexIndex1: Int, _ vertexIndex2: Int, _ vertexIndex3: Int) {
        triangles.append(Triangle(vertexIndex1, vertexIndex2, vertexIndex3));
    }
    
    public func addTriangle(_ triangle: AbstractTriangle) {
        if let t = triangle as? Triangle {
            triangles.append(t)
        } else {
            preconditionFailure("Must be Triangle")
        }
    }
    
    public func addVertex(_ position: Vector) -> Int {
        vertices.append(Vertex(position));
        return vertices.count - 1;
    }
    
    public func addVertex(_ vertex: Vertex) -> Int {
        vertices.append(vertex);
        return vertices.count - 1;
    }
    
    public func getVertex(_ index: Int) -> Vertex {
        return vertices[index];
    }

    public func getNumberOfTriangles() -> Int{
        return triangles.count;
    }

    public func getNumberOfVertices() -> Int{
        return vertices.count;
    }
    
    public func getVertex(_ triangle: AbstractTriangle, _ index: Int) -> Vertex? {
        if let t = triangle as? Triangle {
            return vertices[t.getVertexIndex(index)]
            
        }
        return nil;
    }
    
    public func getTriangle(_ triangleIndex: Int) -> AbstractTriangle {
        return triangles[triangleIndex];
    }
    
    public func getTextureCoordinate(_ texCoordIndex: Int) -> Vector{
        return textureCoordinates[texCoordIndex];
    }

    public func computeTriangleNormals() {
        for triangleIndex in 0 ... getNumberOfTriangles() - 1 {
            let t = triangles[triangleIndex];
            let a = vertices[t.getVertexIndex(0)].getPosition();
            let b = vertices[t.getVertexIndex(1)].getPosition();
            let c = vertices[t.getVertexIndex(2)].getPosition();
            var normal = b.subtract(a).cross(c.subtract(a));
            let norm = normal.getNorm();
            if (norm > 1e-5) {
                normal = normal.multiply(1.0 / norm);
            }
            t.setNormal(normal);
        }
    }
    
    public func addTextureCoordinate(_ t: Vector) {
        textureCoordinates.append(t)
    }
    
    public func getTexture() -> Texture? {
        if hasTexture(){
            return TextureManager.getInstance().getTexture(textureName!);
        }
        return nil
    }
    
    public func addTriangle(_ vertexIndex1: Int, _ vertexIndex2: Int, _ vertexIndex3: Int, _ texCoordIndex1: Int, _ texCoordIndex2: Int,
    _ texCoordIndex3: Int) {
        triangles
            .append(Triangle(vertexIndex1, vertexIndex2, vertexIndex3, texCoordIndex1, texCoordIndex2, texCoordIndex3));
    }
    
    public func getNumberOfTextureCoordinates() -> Int{
        return textureCoordinates.count;
    }
    
    public func hasTexture() -> Bool {
        return textureName != nil;
    }
    
    public func setColor(_ color: Vector) {
        if (color.getDimension() != 4) {
            NSLog("Color must be in RGBA format.");
            return;
        }
        for triangle in triangles {
            triangle.setColor(color);
        }
        for vertex in vertices {
            vertex.setColor(color);
        }
    }

    public func setTransparency(_ alpha: Double) {
        for triangle in triangles {
            triangle.getColor().set(3, alpha);
        }
        for vertex in vertices {
            vertex.getColor().set(3, alpha);
        }
    }
    
    public func getBoundingBox() -> AxisAlignedBoundingBox{
        let bb = AxisAlignedBoundingBox();
        for v in vertices {
            bb.add(v.getPosition());
        }
        return bb;
    }
    
    public func setTextureName(_ textureFilename: String?) {
        self.textureName = textureFilename;
    }
}
