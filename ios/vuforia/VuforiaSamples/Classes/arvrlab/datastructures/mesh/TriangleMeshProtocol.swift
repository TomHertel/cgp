//
//  TriangleMeshProtocol.swift
//  VuforiaSamples
//
//  Created by Philipp Jenke on 20.11.17.
//  Copyright © 2017 PTC. All rights reserved.
//

import Foundation

/**
 * Shared interface for all triangle mesh implementations.
 *
 * @author Philipp Jenke
 */
public protocol ITriangleMesh {
    /**
     * Add a new vertex (given by position) to the vertex list. The new vertex is
     * appended to the end of the list.
     */
    func addVertex(_ position: Vector) -> Int;
    
    /**
     * Index in vertex list.
     */
    func getVertex(_ index: Int) -> Vertex;
    
    /**
     * Index in triangle, must be in 0, 1, 2.
     */
    func getVertex(_ triangle: AbstractTriangle, _ index: Int) -> Vertex?;
    
    func getNumberOfVertices() -> Int;
    
    /**
     * Add a new triangle to the mesh with the vertex indices a, b, c. The index
     * of the first vertex is 0.
     */
    func addTriangle(_ vertexIndex1: Int, _ vertexIndex2: Int, _ vertexIndex3: Int);
    
    /**
     * Add a new triangle to the mesh with the vertex indices a, b, c. The index
     * of the first vertex is 0.
     */
    func addTriangle(_ t: AbstractTriangle);
    
    /**
     * Add triangle by vertex indices and corresponding texture coordinate
     * indices.
     */
    func addTriangle(_ vertexIndex1: Int, _ vertexIndex2: Int, _ vertexIndex3: Int,
                     _ texCoordIndex1: Int, _ texCoordIndex2: Int, _ texCoordIndex3: Int);
    
    func getNumberOfTriangles() -> Int;
    
    func getTriangle(_ triangleIndex: Int) -> AbstractTriangle;
    
    /**
     * Clear mesh - remove all triangles and vertices.
     */
    func clear();
    
    /**
     * Compute the triangles normals.
     */
    func computeTriangleNormals();
    
    
    func getTextureCoordinate(_ index: Int) -> Vector;
    
    /**
     * Add texture coordinate to mesh.
     */
    func addTextureCoordinate(_ t: Vector);
    
    func getTexture() -> Texture?
    
    /**
     * Return the number of texture coordinates in the mesh.
     */
    func getNumberOfTextureCoordinates() -> Int;
    
    /**
     * Returns true if the mesh has a texture assigned.
     */
    func hasTexture() -> Bool;
    
    
    /**
     * Return the bounding box of the mesh.
     */
    func getBoundingBox() -> AxisAlignedBoundingBox;
    
    func setTextureName(_ textureFilename: String?);
    
    /**
     * Set color to all triangles and all vertices of the mesh.
     */
    func setColor(_ color: Vector);
    
    /**
     * Sets the alpha (blendding/trasparency) value for all triangles.
     */
    func setTransparency(_ alpha: Double);
}
