//
//  TriangleMeshTools.swift
//  ComputergraphicsAR
//
//  Created by Philipp Jenke on 22.11.17.
//  Copyright © 2017 PTC. All rights reserved.
//

import Foundation

public class TriangleMeshTools {
    
    /**
     * The minimum y-value is 0.
     */
    public static func placeOnXZPlane(_ mesh: ITriangleMesh) {
        placeOnXZPlane( [mesh] );
    }
    
    /**
     * The minimum y-value is 0.
     */
    public static func placeOnXZPlane(_ meshes: [ITriangleMesh]) {
        var yMin = MathHelpers.MAX_DOUBLE;
    
        for mesh in meshes {
            for i in 0 ... mesh.getNumberOfVertices()-1 {
                let pos = mesh.getVertex(i).getPosition();
                if (pos.y() < yMin) {
                    yMin = pos.y();
                }
            }
        }
    
        for mesh in meshes {
            for i in 0 ... mesh.getNumberOfVertices() - 1 {
                let vertex = mesh.getVertex(i);
                vertex.getPosition().set(1, vertex.getPosition().y() - yMin);
            }
            mesh.computeTriangleNormals();
        }
        NSLog("Successfully placed object on x-z-plane.");
    }
    
}
