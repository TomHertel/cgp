//
//  Vertex.swift
//  VuforiaSamples
//
//  Created by Philipp Jenke on 20.11.17.
//  Copyright © 2017 PTC. All rights reserved.
//

import Foundation

/**
 * Represents a vertex in 3-space with position and normal.
 *
 * @author Philipp Jenke
 */
public class Vertex {
    
    /**
     * Vertex position in 3-space.
     */
    private let position = Vector(0, 0, 0);
    
    /**
     * Vertex normal in 3-space.
     */
    private let normal = Vector(0, 1, 0);
    
    /**
     * Color in RBGA format.
     */
    private let color = Vector(0.5, 0.5, 0.5, 1);
    
    init(_ position: Vector) {
        self.position.copy(position);
    }
    
    public init(_ position: Vector, _ normal: Vector) {
        self.position.copy(position);
        self.normal.copy(normal);
    }
    
    public init(_ vertex: Vertex) {
        self.position.copy(vertex.position)
        self.normal.copy(vertex.normal)
        self.color.copy(vertex.color)
    }
    
    public func getPosition() -> Vector{
        return position;
    }
    
    public func getNormal() -> Vector{
        return normal;
    }
    
    public func setNormal(_ normal: Vector) {
        self.normal.copy(normal);
    }
    
    public func getColor() -> Vector{
        return color;
    }
    
    public func setColor(_ color: Vector) {
        self.color.copy(color);
    }
}
