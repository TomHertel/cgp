//
//  TriangleMeshFactory.swift
//  ComputergraphicsAR
//
//  Created by Philipp Jenke on 22.11.17.
//  Copyright © 2017 PTC. All rights reserved.
//

import Foundation

public class TriangleMeshFactory {
    
    /**
     * Create sphere.
     */
    public static func createSphere(_ mesh: ITriangleMesh, _ radius: Double, _ resolution: Int) {
        mesh.clear();
        let dTheta = Double.pi / Double(resolution + 1);
        let dPhi = Double.pi * 2.0 / Double(resolution);
        // Create vertices
        
        // 0-180 degrees: i, theta
        for i in 0 ... resolution-1{
            
            // 0-360 degres: j, phi
            for j in 0 ... resolution-1{
                let p0 = evaluateSpherePoint(Double(i + 1) * dTheta, Double(j) * dPhi, radius);
                _ = mesh.addVertex(p0);
            }
        }
        let leftIndex = mesh.addVertex( Vector(0, 0, radius));
        let rightIndex = mesh.addVertex( Vector(0, 0, -radius));
        // Add triangles
        for i in 0 ... resolution - 2 {
            for j in 0 ... resolution-1 {
                mesh.addTriangle(getSphereIndex(i, j, resolution),
                                 getSphereIndex(i + 1, j, resolution),
                                 getSphereIndex(i + 1, j + 1, resolution));
                mesh.addTriangle(getSphereIndex(i, j, resolution),
                                 getSphereIndex(i + 1, j + 1, resolution),
                                 getSphereIndex(i, j + 1, resolution));
            }
        }
        for j in 0 ... resolution-1 {
            mesh.addTriangle(getSphereIndex(0, j, resolution),
                             getSphereIndex(0, (j + 1) % resolution, resolution), leftIndex);
            mesh.addTriangle(getSphereIndex(resolution - 1, j, resolution),
                             rightIndex,
                             getSphereIndex(resolution - 1, (j + 1) % resolution, resolution));
        }
        
        mesh.computeTriangleNormals();
    }
    
    private static func evaluateSpherePoint(_ theta: Double, _ phi: Double,
                                            _ radius: Double) -> Vector{
        let x = radius * sin(theta) * cos(phi);
        let y = radius * sin(theta) * sin(phi);
        let z = radius * cos(theta);
        return  Vector(x, y, z);
    }
    
    private static func getSphereIndex(_ i: Int, _ j: Int, _ resolution: Int) -> Int{
        return (i % resolution) * resolution + (j % resolution);
    }
    
    public static func createCube(_ mesh: ITriangleMesh) {
        createCube(mesh, Vector(0, 0, 0));
    }
    
    public static func createCube(_ mesh: ITriangleMesh, _ offset: Vector) {
        mesh.clear();
        
        let d = 0.5;
        let v000 = mesh.addVertex( Vector(-d, -d, -d).add(offset));
        let v010 = mesh.addVertex( Vector(-d, d, -d).add(offset));
        let v110 = mesh.addVertex( Vector(d, d, -d).add(offset));
        let v100 = mesh.addVertex( Vector(d, -d, -d).add(offset));
        let v001 = mesh.addVertex( Vector(-d, -d, d).add(offset));
        let v011 = mesh.addVertex( Vector(-d, d, d).add(offset));
        let v111 = mesh.addVertex( Vector(d, d, d).add(offset));
        let v101 = mesh.addVertex( Vector(d, -d, d).add(offset));
        
        // front
        mesh.addTriangle( Triangle(v000, v110, v100));
        mesh.addTriangle( Triangle(v000, v010, v110));
        // right
        mesh.addTriangle( Triangle(v100, v111, v101));
        mesh.addTriangle( Triangle(v100, v110, v111));
        // back
        mesh.addTriangle( Triangle(v101, v011, v001));
        mesh.addTriangle( Triangle(v101, v111, v011));
        // left
        mesh.addTriangle( Triangle(v001, v010, v000));
        mesh.addTriangle( Triangle(v001, v011, v010));
        // top
        mesh.addTriangle( Triangle(v010, v111, v110));
        mesh.addTriangle( Triangle(v010, v011, v111));
        // bottom
        mesh.addTriangle( Triangle(000, v101, v100));
        mesh.addTriangle( Triangle(v000, v001, v101));
        
        mesh.computeTriangleNormals();
    }
}
