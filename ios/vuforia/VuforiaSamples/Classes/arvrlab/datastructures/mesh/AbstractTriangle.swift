//
//  AbstractTriangle.swift
//  VuforiaSamples
//
//  Created by Philipp Jenke on 20.11.17.
//  Copyright © 2017 PTC. All rights reserved.
//

import Foundation

/**
 * Generic triangle representation
 *
 * @author Philipp Jenke
 */
public class AbstractTriangle {
    
    /**
     * Triangle color in RGBA format.
     */
    private var color = Vector(0.5, 0.5, 0.5, 1);
    
    /**
     * Facet normal
     */
    private var normal = Vector(0, 1, 0);
    
    /**
     * Indices of the texture coordinates.
     */
    private var texCoordIndices = [-1, -1, -1];
    
    public init() {
    }
    
    public init( _ triangle: AbstractTriangle) {
        self.color.copy(triangle.color);
        self.normal.copy(triangle.normal);
        texCoordIndices[0] = triangle.texCoordIndices[0];
        texCoordIndices[1] = triangle.texCoordIndices[1];
        texCoordIndices[2] = triangle.texCoordIndices[2];
    }
    
    public init(_ tA: Int, _ tB: Int, _ tC: Int, _ normal: Vector) {
        self.normal.copy(normal);
        texCoordIndices[0] = tA;
        texCoordIndices[1] = tB;
        texCoordIndices[2] = tC;
    }
    
    public init(_ normal: Vector) {
        self.normal.copy(normal);
        texCoordIndices[0] = -1;
        texCoordIndices[1] = -1;
        texCoordIndices[2] = -1;
    }
    
    public func getColor() -> Vector{
        return color;
    }
    
    /**
     * Color must be a 4D vector in RGBA format.
     */
    public func setColor(_ color: Vector) {
        if (color.getDimension() != 4) {
            NSLog("Color must be given in RGBA format!");
        }
        self.color.copy(color);
    }
    
    public func setNormal(_ normal: Vector) {
        self.normal.copy(normal);
    }
    
    public func getNormal() -> Vector{
        return normal;
    }
    
    /**
     * i must be in 0, 1, 2
     */
    public func getTexCoordIndex(_ i: Int) -> Int{
        return texCoordIndices[i];
    }
    
    /**
     * Add an offset to all texture coordinates.
     */
    public func addTexCoordOffset(_ offset: Int) {
        for i in 0 ... 2 {
            texCoordIndices[i] = texCoordIndices[i] + offset;
        }
    }
}
