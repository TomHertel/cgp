//
//  Material.swift
//  ComputergraphicsAR
//
//  Created by Philipp Jenke on 24.11.17.
//  Copyright © 2017 PTC. All rights reserved.
//

import Foundation

/**
 * Represents an OBJ-file material.
 */
public class Material {
    private var name = "";
    private var textureFilename : String?;
    private var color = Vector(1, 1, 1, 1);
    
    public init(_ name: String) {
        self.name = name;
    }
    
    public func getName() -> String{
        return name;
    }
    
    public func getColor() -> Vector{
        return color;
    }
    
    public func setColor(_ color: Vector) {
        self.color.copy(color);
    }
    
    public func getTextureFilename() -> String?{
        return textureFilename;
    }
    
    public func setTextureFilename(_ filename: String?) {
        self.textureFilename = filename;
    }
    
    public func toString() -> String {
        if let t = textureFilename {
            return "\(name): \(color), \(t)";
        } else {
            return "\(name): \(color)";
        }
    }
}
