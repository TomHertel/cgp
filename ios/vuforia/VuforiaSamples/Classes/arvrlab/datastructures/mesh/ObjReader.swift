//
//  ObjReader.swift
//  ComputergraphicsAR
//
//  Created by Philipp Jenke on 24.11.17.
//  Copyright © 2017 PTC. All rights reserved.
//

import Foundation

/**
 * Read OBJ file and fill triangle mesh with the content.
 *
 * @author Philipp Jenke
 */
public class ObjReader {
    
    private static let OP_MAT_LIB = "mtllib";
    private static let OP_VERTEX = "v";
    private static let OP_FACET = "f";
    private static let OP_TEXTURE_COORDINATE = "vt";
    private static let OP_MAT_NEW_MAT = "newmtl";
    private static let OP_MAT_KD = "Kd";
    private static let OP_MAT_TEXTURE = "map_Kd";
    private static let OP_NEW_OBJECT = "o";
    private static let OP_USE_MTL = "usemtl";
    private static let DEFAULT_COLOR = Vector(0.5, 0.5, 0.5, 1);
    
    // Subdirectory used to look for the material file (extracted from mesh file).
    private var directory = ""
    
    // State of the import process
    private var currentMesh: ITriangleMesh?;
    private var currentMaterial : Material?;
    private var materials =  [String: Material]()
    private var meshes = [ITriangleMesh]()
    private var vertexIndexOffset = 0;
    private var texCoordOffset = 0;
    
    public init(){
    }
    
    /**
     * Lesen eines Dreiecksnetzes aus einer OBJ-Datei. Die Information wird in das
     * Dreiecksnetz 'mesh' geschrieben.
     * <p>
     * Returns true if the mesh is successfully read.
     */
    public func read(_ filename: String) -> [ITriangleMesh]{
        // Setup
        meshes = [ITriangleMesh]()
        
        let url = URL(fileURLWithPath: filename)
        let dirUrl = url.deletingLastPathComponent()
        directory = "\(dirUrl.path)/"
        currentMesh = TriangleMesh();
        meshes.append(currentMesh!);
        materials = [String: Material]()
        vertexIndexOffset = 0;
        texCoordOffset = 0;
        
        // Read input
        //System.out.println("Trying to read OBJ file " + filename);
        
        if let objFileText = AssetPath.getInstance().readTextFileToString(filename){
            let lines = objFileText.split(separator: "\n")
            for line in lines {
                parseLine(String(line));
            }
        } else {
            NSLog("Failed to read OBJ file at \(filename)")
            return [ITriangleMesh]()
        }
        
        // Post-process meshes
        var i = 0;
        while i < meshes.count {
            let mesh = meshes[i]
            if (mesh.getNumberOfTriangles() == 0) {
                meshes.remove(at: i)
                i = i - 1
            } else {
                mesh.computeTriangleNormals();
                NSLog("Successfully created triangle mesh with \(mesh.getNumberOfVertices()) vertices and \(mesh.getNumberOfTriangles()) triangles.");
            }
            i = i + 1
        }
        
        if (meshes.count == 0) {
            NSLog("Could not find any meshes in OBJ file \(filename)");
        }
        return meshes;
    }
    
    /**
     * Einlesen einer Zeile aus der OBJ-Datei.
     */
    private func parseLine(_ strLine: String) {
        let line = trim(strLine);
        let op = getOperator(line);
        if op == ObjReader.OP_MAT_LIB {
            // Lesen der Materialdatei (Texturname)
            parseUseMaterial(line);
        } else if  op == ObjReader.OP_NEW_OBJECT  {
            // Neues (Teil-)Objekt
            vertexIndexOffset += currentMesh!.getNumberOfVertices();
            texCoordOffset += currentMesh!.getNumberOfTextureCoordinates();
            currentMesh = TriangleMesh();
            meshes.append(currentMesh!);
        } else if op == ObjReader.OP_USE_MTL {
            let components = line.components(separatedBy: " ");
            currentMaterial = nil;
            if components.count > 1 {
                let materalId = components[1];
                // System.out.println("Assigned material: " + materalId);
                if let mat = materials[materalId] {
                    currentMaterial = mat;
                    currentMesh!.setTextureName(nil)
                    if let t = mat.getTextureFilename() {
                        if t.count > 0 {
                            currentMesh!.setTextureName(t)
                        }
                    }
                }
            }
        } else if op == ObjReader.OP_VERTEX {
            // Lesen eines Vertex
            let position = parseVertex(line);
            if let p = position {
                _ = currentMesh!.addVertex(p);
            }
        } else if op == ObjReader.OP_FACET {
            // Lesen einer Facette (Dreieck)
            parseFacet(line);
        } else if op == ObjReader.OP_TEXTURE_COORDINATE {
            // Lesen einer Texturkoordinate
            if let t = parseTextureCoordinate(line) {
                currentMesh!.addTextureCoordinate(t);
                //textureCoordinates.add(t);
            }
        }
    }
    
    /**
     * Einlesen einer Materialdatei mit Texturinformtion. Liefert den Namen der
     * Texturdatei. Liefert null, falls keine Textur gefunden wurde.
     */
    private func parseUseMaterial(_ line: String) {
        let components = line.components(separatedBy: " ");
        if components.count == 2 {
            let materialFilename = components[1];
            parseMaterialFile(directory + materialFilename);
        }
    }
    
    private func parseMaterialFile(_ materialFilename: String) {
        currentMaterial = nil;
        if let materialFileText = AssetPath.getInstance().readTextFileToString(materialFilename){
            let lines = materialFileText.split(separator: "\n")
            for line in lines {
                parseMaterialLine(String(line));
            }
        }
    }
    
    private func parseMaterialLine(_ strLine: String) {
        let line = trim(strLine);
        let op = getOperator(line);
        if op == ObjReader.OP_MAT_NEW_MAT {
            let components = strLine.components(separatedBy: " ");
            if components.count >= 2 {
                let materialId = components[1];
                currentMaterial = Material(materialId);
                materials[materialId] = currentMaterial;
            }
        } else if op == ObjReader.OP_MAT_KD {
            let components = strLine.components(separatedBy: " ");
            if components.count >= 4 {
                let color = Vector(
                    Double(components[1])!,
                    Double(components[2])!,
                    Double(components[3])!,
                    1);
                currentMaterial!.setColor(color);
            }
        } else if op == ObjReader.OP_MAT_TEXTURE {
            let components = strLine.components(separatedBy: " ");
            if components.count >= 2 {
                var textureFilename = components[1]
                textureFilename = textureFilename.trimmingCharacters(in: .whitespacesAndNewlines)
                if (textureFilename.count != 0) {
                    currentMaterial!.setTextureFilename(textureFilename)
                }
            }
        }
    }
    
    /**
     * Entfernen doppelter Leerzeichen aus einer Zeile.
     */
    private func trim(_ strLine: String) -> String{
        var line = strLine.trimmingCharacters(in: .whitespacesAndNewlines)
        line = line.replacingOccurrences(of: "  ", with: " ")
        var l = line.count
        while (l < line.count) {
            l = line.count
            line = line.replacingOccurrences(of: "  ", with: " ")
        }
        return line;
    }
    
    /**
     * Auslesen einer Zeile, die eine Texturkoordinate beinhaltet.
     */
    private func parseTextureCoordinate(_ strLine: String) -> Vector? {
        let line = trim(strLine);
        let allCoords = line.components(separatedBy: " ");
        var u = 0.0;
        var v = 0.0;
        if allCoords.count >= 3 {
            u = getDoubleValue(allCoords[1]);
            v = getDoubleValue(allCoords[2]);
            return Vector(u, v);
        }
        return nil;
    }
    
    /**
     * String -> float.
     */
    private func getDoubleValue(_ string: String) -> Double {
        if string.count == 0 {
            return 0
        }
        return Double(string)!
    }
    
    /**
     * Lesen einer Zeile, die ein Dreieck repräsentiert (Indices der Eckpunkte,
     * Indices der Texturkoordinaten).
     */
    private func parseFacet(_ strLine: String) {
        let allCoords = strLine.components(separatedBy: " ");
        if allCoords.count == 4 {
            createTriangle(allCoords[1], allCoords[2], allCoords[3]);
        } else if allCoords.count == 5 {
            createTriangle(allCoords[1], allCoords[2], allCoords[3]);
            createTriangle(allCoords[1], allCoords[3], allCoords[4]);
        }
    }
    
    private func createTriangle(_ token1: String, _ token2: String, _ token3: String) {
        let t = Triangle(
            getVertexIndexFromToken(token1),
            getVertexIndexFromToken(token2),
            getVertexIndexFromToken(token3),
            getTexCoordIndexFromToken(token1),
            getTexCoordIndexFromToken(token2),
            getTexCoordIndexFromToken(token3));
        t.setColor(ObjReader.DEFAULT_COLOR);
        if let c = currentMaterial {
            t.setColor(c.getColor());
        }
        currentMesh!.addTriangle(t);
    }
    
    private func getVertexIndexFromToken(_ token: String) -> Int{
        let tokens = token.components(separatedBy: "/");
        if (tokens.count > 0 && String(tokens[0])!.count > 0) {
            return Int(tokens[0])! - 1 - vertexIndexOffset;
        } else {
            return -1;
        }
    }
    
    private func getTexCoordIndexFromToken(_ token: String) -> Int{
        let tokens = token.components(separatedBy: "/");
        if (tokens.count > 1 && String(tokens[1]).count > 0) {
            return Int(tokens[1])! - 1 - texCoordOffset;
        } else {
            return -1;
        }
    }
    
    /**
     * Lesen einer Zeile, die einen Vertex repräsentiert.
     */
    private func parseVertex(_ strLine: String) -> Vector? {
        let components = strLine.components(separatedBy: " ");
        if components.count >= 4 {
            let x = Double(components[1])!
            let y = Double(components[2])!
            let z = Double(components[3])!
            return Vector(x, y, z);
        }
        return nil;
    }
    
    /**
     * Extract the operator char from a line.
     *
     * @param strLine
     * @return String representing the operator
     */
    private func getOperator(_ strLine: String) -> String {
        let components = strLine.components(separatedBy: " ");
        if components.count > 0 {
            return components[0];
        } else {
            return "";
        }
    }
}
