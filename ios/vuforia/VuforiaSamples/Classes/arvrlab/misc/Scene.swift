//
//  Scene.swift
//  ComputergraphicsAR
//
//  Created by Philipp Jenke on 21.11.17.
//  Copyright © 2017 PTC. All rights reserved.
//

import Foundation
import OpenGLES

/**
 * Central frame for all applications - derive from this class.
 *
 * @author Philipp Jenke
 */
public class Scene : NSObject {
    /**
     * Root node of the scene graph
     */
    private var root : RootNode?
    
    /**
     * Current render mode.
     */
    private var currentRenderMode: INode.RenderMode?
    
    /**
     * Set this flag if the projection matrix needs to be updated.
     */
    private var needsUpdateProjectionMatrix = true;
    
    /**
     * Set this flag if the view matrix needs to be updated.
     */
    private var needsUpdateViewMatrix = true;
    
    /**
     * Constructor
     */
    public override init() {
        super.init()
        NSLog("Scene created.")
        let shader = Shader()
        currentRenderMode = INode.RenderMode.REGULAR
        root = RootNode(shader)
    }
    
    
    /**
     * Constructor
     */
    public init(renderMode: INode.RenderMode) {
        NSLog("Scene created.")
        let shader = Shader()
        currentRenderMode = renderMode
        root = RootNode(shader)
    }
    
    /**
     * Override this method with all the scene onSetup, e.g. setting up the scene graph
     */
    public func onSetup(){
        let marker = VuforiaMarkerNode("elphi");
       getRoot().addChild(marker);
        
        // Sphere
//        let mesh = TriangleMesh();
//        TriangleMeshFactory.createSphere(mesh, 1, 7)
//        TriangleMeshTools.placeOnXZPlane(mesh);
//        let node = TriangleMeshNode(mesh);
//        let scaleNode = ScaleNode(0.2)
//        scaleNode.addChild(node)
//        marker.addChild(scaleNode);

        
        // Plane
        let objReader = ObjReader()
        let meshes = objReader.read("meshes/chest.obj")
        let node = InnerNode();
        for mesh in meshes {
            let meshNode = TriangleMeshNode(mesh)
            node.addChild(meshNode)
        }
        let scaleNode = ScaleNode(0.5)
        scaleNode.addChild(node)
        marker.addChild(scaleNode)
    }
    
    /**
     * This method is called once for each scene redraw of OpenGL ES.
     */
    public func onSceneRedraw(){
    }
    
    
    /**
     * This method is called once when the OpenGL context is created.
     */
    public func initScene() {
        NSLog("Init: setting OpenGL parameters.");
        if let versionString = glGetString(GLenum(GL_VERSION)){
            NSLog("OpenGL-Version: \(versionString)");
        } else {
            NSLog("OpenGL-Version cannot be identified.");
        }
        var number: GLint = 0
        glGetIntegerv(GLenum(GL_STENCIL_BITS), &number);
        NSLog("Stencil buffer bits: \(number)");
        glGetIntegerv(GLenum(GL_DEPTH_BITS), &number);
        NSLog("Depth buffer bits: \(number)");
        
        // Stencil test
        //glEnable(GLenum(GL_STENCIL_TEST));
        
        // Culling
        glCullFace(GLenum(GL_BACK));
        glFrontFace(GLenum(GL_CCW));
        
        // Depth Test
        glEnable(GLenum(GL_DEPTH_TEST));
        
        Shader.checkGlError("[before shader compile&link]");
        needsUpdateProjectionMatrix = true;
        root!.getShader().compileAndLink();
        root!.getShader().use();
        ShaderAttributes.getInstance().getAttributes(root!.getShader().getProgram());
        
        glClearColor(1.0, 1.0, 1.0, 1.0)
    }
    
    /**
     * Scene needs to be redrawn.
     */
    public func redraw() {
        onSceneRedraw();
        drawRegular()
    }

    
    /**
     * Render scene regularly
     */
    private func drawRegular() {
        root!.getShader().use();
        ShaderAttributes.getInstance().setCameraEyeParameter(Vector(0, 0, 0));
        ShaderAttributes.getInstance().setLightPositionParameter(root!.getLightPosition());
        Shader.checkGlError();
    
        // No change in stencil buffer
        glStencilOp(GLenum(GL_KEEP), GLenum(GL_KEEP), GLenum(GL_KEEP));
        // Draw always
        glStencilFunc(GLenum(GL_ALWAYS), 0, 255);
    
        getRoot().traverse(INode.RenderMode.REGULAR, Matrix.createIdentityMatrix4());
    }
    
    /**
     * Return the root node of the scene graph.
     */
    public func getRoot() -> RootNode {
        return root!
    }
    
    public func resize(_ width: Int, _ height: Int) {
        Camera.getInstance().setScreenSize(width, height)
    }
    
    public func setProjectionMatrix(_ pm: [Float]){
        let M = Matrix( Double(pm[0]), Double(pm[1]), Double(pm[2]), Double(pm[3]),
                        Double(pm[4]), Double(pm[5]), Double(pm[6]), Double(pm[7]),
                        Double(pm[8]), Double(pm[9]), Double(pm[10]), Double(pm[11]),
                        Double(pm[12]), Double(pm[13]), Double(pm[14]), Double(pm[15]) )
        Camera.getInstance().setProjectionMatrixGL(M)
    }
    
    public func touchDelta(_ x: Int, _ y: Int){
    }
    
    public func zoom(_ factor: Double){
    }
}
