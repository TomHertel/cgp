//
//  AssetPath.swift
//  VuforiaSamples
//
//  Created by Philipp Jenke on 20.11.17.
//  Copyright © 2017 PTC. All rights reserved.
//

import Foundation

class AssetPath {
    static var instance: AssetPath?;
    
    init(){
    }
    
    static func getInstance() -> AssetPath {
        if (instance == nil) {
            instance = AssetPath();
        }
        return instance!;
    }
    
    func readTextFileToString(_ filename: String) -> String? {
        return readTextFileToString(filename, nil)
    }
    
    func readTextFileToString(_ filename: String, _ ext: String?) -> String? {
        if let fileURL = Bundle.main.path(forResource:filename, ofType: ext){
            do {
                let contents = try String(contentsOfFile: fileURL)
                return contents
            } catch {
            }
        }
        if let e = ext {
            NSLog("Failed to read file \(filename).\(e)");
        } else {
            NSLog("Failed to read file \(filename)");
        }
        return nil;
    }
}
