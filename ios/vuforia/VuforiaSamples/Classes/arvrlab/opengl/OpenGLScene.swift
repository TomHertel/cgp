//
//  OpenGLScene.swift
//  ComputergraphicsAR
//
//  Created by Philipp Jenke on 25.11.17.
//  Copyright © 2017 PTC. All rights reserved.
//

import Foundation
import GLKit

public class OpenGLScene: Scene {
    
    /**
     * Override this method with all the scene onSetup, e.g. setting up the scene graph
     */
    public override func onSetup(){


        
        // Sphere
        //        let mesh = TriangleMesh();
        //        TriangleMeshFactory.createSphere(mesh, 1, 7)
        //        TriangleMeshTools.placeOnXZPlane(mesh);
        //        let node = TriangleMeshNode(mesh);
        //        let scaleNode = ScaleNode(0.2)
        //        scaleNode.addChild(node)
        //        marker.addChild(scaleNode);
        
        
        // Plane
        let objReader = ObjReader()
        let meshes = objReader.read("meshes/chest.obj")
        let node = InnerNode();
        for mesh in meshes {
            let meshNode = TriangleMeshNode(mesh)
            node.addChild(meshNode)
        }
        let scaleNode = ScaleNode(0.5)
        scaleNode.addChild(node)
        getRoot().addChild(scaleNode);
    }
    
    public override func resize(_ width: Int, _ height: Int) {
        super.resize(width, height)
        Camera.getInstance().computeProjectionMatrixGL();
    }
    
    public override func redraw() {
        glClear(GLenum(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT));
        glEnable(GLenum(GL_DEPTH_TEST));
        glEnable(GLenum(GL_CULL_FACE));
        glCullFace(GLenum(GL_BACK));
        Camera.getInstance().computeViewMatrixGL()
        super.redraw()
    }
    
    public override func touchDelta(_ x: Int, _ y: Int){
        Camera.getInstance().touchMoved(x, y)
    }
    
    public override func zoom(_ factor: Double){
        Camera.getInstance().zoom(factor)
    }
}
