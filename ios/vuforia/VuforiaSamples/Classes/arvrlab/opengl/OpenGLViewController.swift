//
//  OpenGLViewController.swift
//  ComputergraphicsAR
//
//  Created by Philipp Jenke on 25.11.17.
//  Copyright © 2017 PTC. All rights reserved.
//

import UIKit
import GLKit

public class OpenGLViewController: GLKViewController {
    
    var context: EAGLContext? = nil
    var scene: Scene?
    var lastTouchPosition = Vector(-1,-1)
    var pinchGesture = UIPinchGestureRecognizer()
    
    override public func viewDidLoad() {
        super.viewDidLoad()
        self.scene = OpenGLScene()
        
        context = EAGLContext(api: .openGLES2)
        if context == nil {
            print("Failed to create ES context")
        }
        
        let view = self.view as! GLKView
        view.context = self.context!
        view.drawableDepthFormat = .format24
        
        pinchGesture = UIPinchGestureRecognizer(target:self, action:#selector(OpenGLViewController.pinchDetected))
        self.view.addGestureRecognizer(pinchGesture)
        
        self.setupGL()
        scene!.onSetup()
    }
    
    public func pinchDetected(sender: UIPinchGestureRecognizer) {
        let scale: CGFloat = sender.scale
        if scale < 1 {
            scene!.zoom(-0.01)
        } else {
            scene!.zoom(0.01)
        }
    }
    
    func setupGL() {
        EAGLContext.setCurrent(self.context)
        
        scene!.initScene()
        scene!.resize(Int(view.frame.size.width), Int(view.frame.size.height))
    }
    
    func update() {
    }
    
    // Render
    override public func glkView(_ view: GLKView, drawIn rect: CGRect) {
        scene!.resize(Int(view.drawableWidth), Int(view.drawableHeight))
        scene!.redraw()
    }
    
    override public func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        if let _ = touches.first {
            lastTouchPosition.set(0, -1)
            lastTouchPosition.set(1, -1)
        }
        super.touchesBegan(touches, with: event)
    }
    
    override public func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        if let _ = touches.first {
            lastTouchPosition.set(0, -1)
            lastTouchPosition.set(1, -1)
        }
        super.touchesBegan(touches, with: event)
    }
    
    override public func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        if let touch = touches.first {
            if lastTouchPosition.x() < 0 {
                let x = Double(touch.location(in: view).x)
                let y = Double(touch.location(in: view).y)
                lastTouchPosition.set(0, x)
                lastTouchPosition.set(1, y)
            } else {
                let x = Double(touch.location(in: view).x)
                let y = Double(touch.location(in: view).y)
                let delta = Vector(x,y).subtract(lastTouchPosition)
                lastTouchPosition.set(0, x)
                lastTouchPosition.set(1, y)
                scene!.touchDelta(Int(delta.x()), Int(delta.y()))
            }
        }
        super.touchesBegan(touches, with: event)
    }
}
