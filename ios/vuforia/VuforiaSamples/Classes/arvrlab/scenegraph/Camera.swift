//
//  Camera.swift
//  ComputergraphicsAR
//
//  Created by Philipp Jenke on 22.11.17.
//  Copyright © 2017 PTC. All rights reserved.
//

import Foundation
import GLKit

/**
 * The scene camera (mainly intrinisic camera parameters, since the eye is fixed to the origin in this AR application).
 * <p>
 * @author Philipp Jenke
 */

public class Camera {
    private static var instance: Camera?;
    private var fovyRadiens = (80.0 / 180.0 * Double.pi);
    private var aspect = 0.75;
    private var width = 0;
    private var height = 0;
    private var zNear = 0.1;
    private var zFar = 10.0;
    private var projectionMatrixGL: Matrix?;
    private var viewMatrixGL: Matrix?;
    
    private var eye = Vector(0,0,-2);
    private var ref = Vector(0, 0, 0);
    private var up = Vector(0,1,0);
    
    private init() {
        viewMatrixGL = Matrix.createIdentityMatrix4();
        projectionMatrixGL = Matrix.createIdentityMatrix4();
    }
    
    public static func getInstance() -> Camera{
        if (instance == nil) {
            instance = Camera();
        }
        return instance!;
    }
    
    public func getFovyRadiens() -> Double{
        return fovyRadiens;
    }
    
    public func getFovyDegrees() ->Double{
        return fovyRadiens * 180.0 / Double.pi;
    }
    
    public func getAspectRatio() -> Double{
        return aspect;
    }
    
    public func getZNear() -> Double{
        return zNear;
    }
    
    public func getZFar() ->Double{
        return zFar;
    }
    
    public func setClipping(_ zNear: Double, _ zFar: Double) {
        self.zNear = zNear;
        self.zFar = zFar;
    }
    
    public func setScreenSize(_ width: Int, _ height: Int) {
        self.width = width;
        self.height = height;
        self.aspect = Double( width) / Double( height);
    }
    
    public func setFovy(_ fovyRadiens: Double) {
        self.fovyRadiens = fovyRadiens;
    }
    
    public func getHeight() -> Int {
        return height;
    }
    
    public func getWidth() -> Int{
        return width;
    }
    
    public func getViewMatrixGL() -> Matrix{
        return viewMatrixGL!;
    }
    
    public func setViewMatrixGL(_ viewMatrix: Matrix) {
        self.viewMatrixGL = viewMatrix;
    }
    
    public func computeViewMatrixGL() {
        //        float[] viewMatrixData = new float[16];
        //        android.opengl.Matrix.setLookAtM(viewMatrixData, 0, (float)eye.x(), (float)eye.y(),
        //            (float)eye.z(), (float)ref.x(), (float)ref.y(), (float)ref.z(), (float)up.x(),
        //            (float)up.y(), (float)up.z());
        
        //        NSLog("Check if this works: setViewMatrixFromEyeRefUp")
        //        let z = ref.subtract(eye)
        //        z.normalize()
        //        var y = up;
        //        let x = y.cross(z).getNormalized()
        //        y = z.cross(x).getNormalized()
        //        viewMatrix = Matrix.makeHomogenious(Matrix(x, y, z)).getTransposed();
        
        let glkViewMatrix = GLKMatrix4MakeLookAt(
            Float(eye.x()), Float(eye.y()), Float(eye.z()),
            Float(ref.x()), Float(ref.y()), Float(ref.z()),
            Float(up.x()), Float(up.y()), Float(up.z()))
        viewMatrixGL = Matrix(
            Double(glkViewMatrix.m00), Double(glkViewMatrix.m01), Double(glkViewMatrix.m02), Double(glkViewMatrix.m03),
            Double(glkViewMatrix.m10), Double(glkViewMatrix.m11), Double(glkViewMatrix.m12), Double(glkViewMatrix.m13),
            Double(glkViewMatrix.m20), Double(glkViewMatrix.m21), Double(glkViewMatrix.m22), Double(glkViewMatrix.m23),
            Double(glkViewMatrix.m30), Double(glkViewMatrix.m31), Double(glkViewMatrix.m32), Double(glkViewMatrix.m33))
    }
    
    public func setProjectionMatrixGL(_ projectionMatrix: Matrix) {
        self.projectionMatrixGL = projectionMatrix;
    }
    
    public func getProjectionMatrixGL() ->  Matrix{
        return projectionMatrixGL!;
    }
    
    public func computeProjectionMatrixGL(){
        let p = GLKMatrix4MakePerspective(
            Float(getFovyRadiens()),
            Float(getAspectRatio()),
            Float(getZNear()),
            Float(getZFar()))
        projectionMatrixGL = Matrix(
            Double(p.m00), Double(p.m01), Double(p.m02), Double(p.m03),
            Double(p.m10), Double(p.m11), Double(p.m12), Double(p.m13),
            Double(p.m20), Double(p.m21), Double(p.m22), Double(p.m23),
            Double(p.m30), Double(p.m31), Double(p.m32), Double(p.m33))
    }
    
    public func setup(_ eye: Vector, _ ref: Vector, _ up: Vector) {
        self.eye = eye;
        self.ref = ref;
        self.up = up;
    }
    
    public func getEye() -> Vector{
        return eye;
    }
    
    public func getUp() -> Vector{
        return up;
    }
    
    public func getRef() -> Vector{
        return ref;
    }
    
    public func touchMoved(_ dx: Int, _ dy: Int) {
        let alpha = Double(-dx) / 200.0
        let beta = Double(-dy) / 200.0
        var dir = eye.subtract(ref);
        // Rotate around up-vector
        eye = Matrix.createRotationMatrix3(up, alpha).multiply(dir).add(ref);
        // Rotate around side-vector
        dir = eye.subtract(ref);
        var side = dir.cross(up);
        side.normalize();
        eye = Matrix.createRotationMatrix3(side, -beta).multiply(dir).add(ref);
        // Fix up-vector
        dir = ref.subtract(eye);
        side = dir.cross(up);
        side.normalize();
        up = side.cross(dir);
        up.normalize()
        computeViewMatrixGL()
    }
    
    /**
     * Zoom in/out.
     */
    public func zoom(_ factor: Double) {
        let dir = ref.subtract(eye)
        eye = eye.add(dir.multiply(factor))
        computeViewMatrixGL()
    }
}
