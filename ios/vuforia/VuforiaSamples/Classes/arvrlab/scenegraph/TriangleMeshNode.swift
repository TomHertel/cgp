//
//  TriangleMeshNode.swift
//  ComputergraphicsAR
//
//  Created by Philipp Jenke on 22.11.17.
//  Copyright © 2017 PTC. All rights reserved.
//

import Foundation
import OpenGLES

/**
 * Scene graph node to render ITriangleMesh meshes.
 */
public class TriangleMeshNode : LeafNode {
    /**
     * Enum for the render mode: describes which normals are used: triangle normals (flat shading)
     * or vertex normal (phong shading).
     */
    public enum RenderNormals {
        case PER_TRIANGLE_NORMAL, PER_VERTEX_NORMAL
    }
    
    /**
     * Contained triangle mesh to be rendered.
     */
    private var mesh: ITriangleMesh?
    
    /**
     * Debugging: Show normals.
     */
    private var showNormals = false;
    
    /**
     * Select which normals should be used for rendering: triangle vs. vertex normals.
     */
    private var renderNormals = RenderNormals.PER_TRIANGLE_NORMAL;
    
    /**
     * VBOs: triangles
     */
    private let vbo = VertexBufferObject();
    
    /**
     * VBOs: normals
     */
    private let vboNormals = VertexBufferObject();
    
    public init(_ mesh: ITriangleMesh) {
        super.init()
        self.mesh = mesh;
        vbo.setup(createRenderVertices(), GLenum(GL_TRIANGLES));
        vboNormals.setup(createRenderVerticesNormals(), GLenum(GL_LINES));
    }
    
    /**
     * Set the transparency of the mesh.
     */
    public func setTransparency(_ transparency: Double) {
        if let m = mesh {
            m.setTransparency(transparency);
            updateVbo();
        }
    }
    
    /**
     * Create vbo data for mesh rendering
     */
    private func createRenderVertices() -> [RenderVertex] {
        var renderVertices = [RenderVertex]();
        for i in 0 ... mesh!.getNumberOfTriangles() - 1 {
            let t = mesh!.getTriangle(i);
            for j in 0 ... 2 {
                if let vertex = mesh!.getVertex(t, j) {
                    let normal = (renderNormals == RenderNormals.PER_VERTEX_NORMAL) ? vertex.getNormal() : t.getNormal();
                    var renderVertex: RenderVertex? = nil;
                    if (t.getTexCoordIndex(j) >= 0) {
                        renderVertex = RenderVertex(vertex.getPosition(), normal, t.getColor(), mesh!.getTextureCoordinate(t.getTexCoordIndex(j)));
                    } else {
                        renderVertex = RenderVertex(vertex.getPosition(),normal, t.getColor(), Vector(0, 0));
                    }
                    renderVertices.append(renderVertex!);
                }
            }
        }
        return renderVertices;
    }
    
    /**
     * Create vbo data for normal rendering.
     */
    private func createRenderVerticesNormals() -> [RenderVertex] {
        var renderVertices = [RenderVertex]();
        let normalScale = 0.03;
        let color = Vector(0.5, 0.5, 0.5, 1);
        for i in 0 ... mesh!.getNumberOfTriangles() - 1 {
            let t = mesh!.getTriangle(i)
            let p = mesh!.getVertex(t, 0)!.getPosition()
                    .add(mesh!.getVertex(t, 1)!.getPosition())
                    .add(mesh!.getVertex(t, 2)!.getPosition())
                    .multiply(1.0 / 3.0);
            renderVertices.append(RenderVertex(p, t.getNormal(), color));
            renderVertices.append(RenderVertex(p.add(t.getNormal().multiply(normalScale)), t.getNormal(), color));
        }
        return renderVertices;
    }
    
    public override func drawGL(_ mode: INode.RenderMode, _ modelMatrix: Matrix) {
        // Use texture if texture object != null
        
        if (mesh!.hasTexture()) {
            if let texture = mesh!.getTexture() {
                texture.bind();
                ShaderAttributes.getInstance().setShaderModeParameter(Shader.ShaderMode.TEXTURE);
            } else {
                ShaderAttributes.getInstance().setShaderModeParameter(Shader.ShaderMode.PHONG);
            }
        } else {
            ShaderAttributes.getInstance().setShaderModeParameter(Shader.ShaderMode.PHONG);
        }
        
        if (mode == RenderMode.REGULAR) {
            drawRegular();
        } else {
            preconditionFailure("Only render mode REGULAR supported")
        }
    }
    
    /**
     * Draw mesh regularly.
     */
    public func drawRegular() {
        vbo.draw();
        if (showNormals) {
            vboNormals.draw();
        }
    }
    
    public func setShowNormals(_ showNormals: Bool) {
        self.showNormals = showNormals;
    }
    
    public func setRenderNormals(_ renderNormals: RenderNormals) {
        self.renderNormals = renderNormals;
        vbo.setup(createRenderVertices(), GLenum(GL_TRIANGLES));
    }
    
    public func updateVbo() {
        vbo.setup(createRenderVertices(), GLenum(GL_TRIANGLES));
        vboNormals.setup(createRenderVerticesNormals(), GLenum(GL_LINES));
        vbo.invalidate();
    }
    
    public override func getBoundingBox() -> AxisAlignedBoundingBox? {
        if let m = mesh {
            return m.getBoundingBox()
        }
        return nil
    }
}
