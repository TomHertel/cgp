//
//  ScaleNode.swift
//  ComputergraphicsAR
//
//  Created by Philipp Jenke on 23.11.17.
//  Copyright © 2017 PTC. All rights reserved.
//

import Foundation

/**
 * Scene graph node which scales all its child nodes.
 *
 * @author Philipp Jenke
 */
public class ScaleNode : InnerNode {
    
    /**
     * Scaling factors in x-, y- and z-direction.
     */
    private var scaleMatrix: Matrix
    
    /**
     * Constructor.
     */
    public init(_ scale: Vector) {
        self.scaleMatrix = Matrix.createScaleMatrix4(scale);
    }
    
    /**
     * Constructor.
     */
    public init(_ scale: Double) {
        self.scaleMatrix = Matrix.createScaleMatrix4(Vector(scale, scale, scale));
    }
    
    public override func traverse(_ mode: INode.RenderMode, _ modelMatrix: Matrix) {
        super.traverse(mode, modelMatrix.multiply(scaleMatrix));
    }
    
    public override func getTransformation() -> Matrix?{
        if let p = getParentNode() {
            if let t = p.getTransformation() {
                return t.multiply(scaleMatrix)
            }
        }
        return scaleMatrix
    }
    
    public func setScale(_ scale: Vector) {
        scaleMatrix = Matrix.createScaleMatrix4(scale);
    }
}
