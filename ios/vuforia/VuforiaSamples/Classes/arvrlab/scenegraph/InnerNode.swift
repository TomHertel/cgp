//
//  InnerNode.swift
//  VuforiaSamples
//
//  Created by Philipp Jenke on 19.11.17.
//  Copyright © 2017 PTC. All rights reserved.
//

import Foundation

public class InnerNode : INode {
    
    /**
     * List of child nodes.
     */
    private var children = [INode]();
    
    override func traverse(_ mode: RenderMode, _ modelMatrix: Matrix) {
        if (!isActive()) {
            return
        }
        
        if (mode == RenderMode.SHADOW_VOLUME && mode == RenderMode.DEBUG_SHADOW_VOLUME) {
            //traverseShadowee(mode, modelMatrix);
        } else {
            for child in children {
                child.traverse(mode, modelMatrix);
            }
        }
    }
    
    /**
     * Add new child node.
     **/
    func addChild(_ child: INode) {
        child.setParentNode(self);
        children.append(child);
    }
    
    
    override func getTransformation() -> Matrix? {
        if let p = getParentNode() {
            return p.getTransformation();
        }
        return nil
    }
    
    /**
     * Return the bounding
     *
     * @return
     */
    override func getBoundingBox() -> AxisAlignedBoundingBox?{
        var bbox : AxisAlignedBoundingBox?;
        for child in children {
            if let b = child.getBoundingBox() {
                if (bbox == nil) {
                    bbox = AxisAlignedBoundingBox(b);
                } else {
                    bbox!.add(b);
                }
            }
        }
        return bbox
    }
}
