//
//  LeafNode.swift
//  ComputergraphicsAR
//
//  Created by Philipp Jenke on 22.11.17.
//  Copyright © 2017 PTC. All rights reserved.
//

import Foundation

/**
 * A leaf node allows to draw OpenGl content.
 */
public class LeafNode : INode {
    
    
    public override func traverse(_ mode: INode.RenderMode, _ modelMatrix: Matrix) {
        
        if (!isActive()) {
            return;
        }

        // Model matrix
        let openGLModelMatrix = modelMatrix.getTransposed();
        //let openGLModelMatrix = Matrix.createIdentityMatrix4()
        ShaderAttributes.getInstance().setModelMatrixParameter(openGLModelMatrix);
        //NSLog( "model: \(openGLModelMatrix.description)");
        
        // View matrix
        let viewMatrix = Camera.getInstance().getViewMatrixGL();
        //let viewMatrix = Matrix.createIdentityMatrix4()
        ShaderAttributes.getInstance().setViewMatrixParameter(viewMatrix);
        //NSLog("view: \(viewMatrix.description)");
        
        // Projection matrix
        let projectionMatrix =  Camera.getInstance().getProjectionMatrixGL();
        //let projectionMatrix = Matrix.createIdentityMatrix4()
        ShaderAttributes.getInstance().setProjectionMatrixParameter(projectionMatrix);
        //NSLog("projection: \(projectionMatrix.description)");
        
        drawGL(mode, openGLModelMatrix);
    }
    
    /**
     * Draw GL content.
     */
    public func drawGL(_ mode: INode.RenderMode, _ modelMatrix: Matrix){
        
    }
    
    public override func getTransformation() -> Matrix? {
        if let p = getParentNode() {
            if let t = p.getTransformation() {
                return t
            }
        }
        return nil
    }
    
}
