//
//  VuforiaMarkerNode.swift
//  ComputergraphicsAR
//
//  Created by Philipp Jenke on 21.11.17.
//  Copyright © 2017 PTC. All rights reserved.
//

import Foundation
//#import "VuforiaObjBridge.h"

/**
 * Represents a marker and its pose in 6DOF. The model matrix is controlled by the marker pose.
 * <p>
 * Attention: model matrix from scene graph nodes above is ignored!
 *
 * @author Philipp Jenke
 */

public class VuforiaMarkerNode : InnerNode {
    
    /**
     * This matrix is used to adjust the coordinate system such that the up-vector of the marker
     * points into y-direction.
     */
    private var adjustCoordinateSystem: Matrix
    
    /**
     * Latest transformation from the global coordinate system into the marker coordinate system.
     */
    private var markerCoordinateSystem = Matrix.createIdentityMatrix4();
    
    /**
     * Only the marker with the given target name is handled in this node.
     */
    private var targetName: String;
    private var active = true;
    
    public init(_ targetName: String) {
        self.targetName = targetName;
        adjustCoordinateSystem = Matrix.createRotationMatrix4(
            Vector(-1, 0, 0), Double.pi / 2);
    }
    
    
    public override func traverse(_ mode: INode.RenderMode, _ modelMatrix: Matrix) {
        if (isActive()) {
            if let modelMatrixData = VuforiaObjcBridge.getModelMatrix(targetName) {
                if let modelMatrixFloatData = modelMatrixData as? [Float] {
                    let modelMatrix = Matrix(modelMatrixFloatData);
                    markerCoordinateSystem = (adjustCoordinateSystem.multiply(modelMatrix)).getTransposed()
                    super.traverse(mode, markerCoordinateSystem);
                }
            }
        }
    }
    
    public override func isActive() -> Bool {
        return active;
    }
    
    public func setActive(active: Bool) {
        self.active = active;
    }
    
    /**
     * Special case: transformations above the marker node are ignored
     */
    public override func getTransformation() -> Matrix {
        return markerCoordinateSystem;
    }
}
