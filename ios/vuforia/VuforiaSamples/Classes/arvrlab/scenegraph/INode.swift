//
//  INode.swift
//  VuforiaSamples
//
//  Created by Philipp Jenke on 19.11.17.
//  Copyright © 2017 PTC. All rights reserved.
//

import Foundation

/**
 * Parent class for all scene graph nodes.
 *
 * @author Philipp Jenke
 */
public class INode : NSObject {
    
    private var parentNode : INode?
    
    /**
     * This flag indicates of the node is shown/traversed
     */
    private var active = true;
    
    /**
     * This enum allows to pass different render mode states to the drawing
     * routines. The default state is REGULAR.
     */
    public enum RenderMode {
        case REGULAR, SHADOW_VOLUME, DARK, DEBUG_SHADOW_VOLUME
    }
    
    /**
     * This method is called to draw the node using OpenGL commands. Override in
     * implementing nodes. Do not forget to call the same method for the children.
     */
    func traverse( _ mode: RenderMode, _ modelMatrix: Matrix){
        preconditionFailure("This method must be overridden")
    }

    
    /**
     * Every node must know its root node
     */
    func getRootNode() -> RootNode? {
        if let p = parentNode {
            return p.getRootNode()
        }
        return nil;
    }
    
    /**
     * Every node must know its root node
     */
    func setParentNode( _ parentNode: INode) {
        self.parentNode = parentNode;
    }
    
    func getParentNode() -> INode? {
        return parentNode;
    }
    
    /**
     * Return the combined transformation starting at the root node.
     */
    func getTransformation() -> Matrix? {
        preconditionFailure("This method must be overridden")
    }
    
    func setActive(_ active: Bool) {
        self.active = active;
    }
    
    func isActive() -> Bool {
        return active;
    }
    
    func getBoundingBox() -> AxisAlignedBoundingBox? {
        preconditionFailure("This method must be overridden")
    }
}
