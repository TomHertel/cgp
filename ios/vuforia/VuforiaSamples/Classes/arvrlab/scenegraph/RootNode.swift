//
//  RootNode.swift
//  VuforiaSamples
//
//  Created by Philipp Jenke on 19.11.17.
//  Copyright © 2017 PTC. All rights reserved.
//

import Foundation

public class RootNode : InnerNode {
    /**
     * Currently used shader
     */
    private let shader: Shader?;
    
    /**
     * This flags indicates that the scene should be animated
     */
    private var animated: Bool;
    
    /**
     * Position of the light source
     */
    private var lightPosition: Vector;
    
    /**
     * Background color
     */
    private var backGroundColor = Vector(0.95, 0.95, 0.95);
    
    init(_ shader: Shader) {
        self.shader = shader;
        lightPosition = Vector(1, 1, 0);
        animated = true;
    }
    
    override func traverse(_ mode: RenderMode, _ modelMatrix: Matrix) {
        super.traverse(mode, modelMatrix)
    }
    
    override func getRootNode() -> RootNode?{
        return self;
    }
    
    func getShader() -> Shader{
        return shader!;
    }
    
    func isAnimated() -> Bool{
        return animated;
    }
    
    func setAnimated(_ animated: Bool) {
        self.animated = animated;
    }
    
    func getLightPosition() -> Vector {
        return lightPosition;
    }
    
    func getBackgroundColor() -> Vector {
        return backGroundColor;
    }
    
    func setLightPosition(_ lightPosition: Vector ) {
        self.lightPosition = lightPosition;
    }
    
    func setBackgroundColor(_ backGroundColor: Vector ) {
        self.backGroundColor = backGroundColor;
    }
    
    override func getTransformation() -> Matrix{
        return Matrix.createIdentityMatrix4();
    }
}
