//
//  VuforiaTrackerBridge.h
//  ComputergraphicsAR
//
//  Created by Philipp Jenke on 23.11.17.
//  Copyright © 2017 PTC. All rights reserved.
//

#ifndef VuforiaCppBridge_h
#define VuforiaCppBridge_h

/**
  * Bridge from Vuforia to C++/Objc (.mm)
  */
@interface VuforiaCppBridge : NSObject {
}

// --- Public methods ---
+ (NSArray*)getModelMatrix:(NSString*)trackerId;

@end


#endif /* VuforiaTrackerBridge_h */


