//
//  VuforiaObjcBridge.h
//  ComputergraphicsAR
//
//  Created by Philipp Jenke on 23.11.17.
//  Copyright © 2017 PTC. All rights reserved.
//

#ifndef VuforiaObjBridge_h
#define VuforiaObjBridge_h

#import <Foundation/Foundation.h>

/**
 * Bridge from C++/Objc to Objc (.m)
 */
@interface VuforiaObjcBridge : NSObject

// --- Public methods ---
+ (NSArray*)getModelMatrix:(NSString*)trackerId;

@end

#endif
