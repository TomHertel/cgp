//
//  VuforiaTrackerBridget.m
//  ComputergraphicsAR
//
//  Created by Philipp Jenke on 23.11.17.
//  Copyright © 2017 PTC. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "VuforiaCppBridge.h"
#import <Vuforia/TrackerManager.h>
#import <Vuforia/State.h>
#import <Vuforia/Tool.h>
#import <Vuforia/TrackableResult.h>
#import <Vuforia/StateUpdater.h>
#import <Vuforia/Renderer.h>
#import "string.h"

@implementation VuforiaCppBridge

+ (NSArray*)getModelMatrix:(NSString*)trackerId {
    const Vuforia::State state = Vuforia::TrackerManager::getInstance().getStateUpdater().updateState();
    for (int i = 0; i < state.getNumTrackableResults(); ++i) {
        // Get the trackable
        const Vuforia::TrackableResult* result = state.getTrackableResult(i);
        const Vuforia::Trackable& trackable = result->getTrackable();
        if (!strcmp(trackable.getName(), trackerId.cString )) {
            Vuforia::Matrix44F vuforiaViewMatrix = Vuforia::Tool::convertPose2GLMatrix(result->getPose());
            float* data = vuforiaViewMatrix.data;
            NSArray *viewMatrix = @[@(data[0]), @(data[1]), @(data[2]), @(data[3]),
                            @(data[4]), @(data[5]), @(data[6]), @(data[7]),
                            @(data[8]), @(data[9]), @(data[10]), @(data[11]),
                            @(data[12]), @(data[13]), @(data[14]), @(data[15])];
            return viewMatrix;
        }
    }
    return nil;
}

@end
