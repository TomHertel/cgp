//
//  VuforiaObjBridge.m
//  ComputergraphicsAR
//
//  Created by Philipp Jenke on 23.11.17.
//  Copyright © 2017 PTC. All rights reserved.
//

#import "VuforiaObjcBridge.h"
#import "VuforiaCppBridge.h"

@implementation VuforiaObjcBridge

+ (NSArray*)getModelMatrix:(NSString*)trackerId {
    return [VuforiaCppBridge getModelMatrix:trackerId];
}

@end
