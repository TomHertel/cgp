//
//  VertexBufferObject.swift
//  ComputergraphicsAR
//
//  Created by Philipp Jenke on 22.11.17.
//  Copyright © 2017 PTC. All rights reserved.
//

import Foundation
import OpenGLES

/**
 * Rendering vie vertex buffer objects (VBO).
 */
public class VertexBufferObject {
    /**
     * List containing the fragment vertices to be rendered
     */
    private var renderVertices: [RenderVertex]?;
    
    /**
     * Use this primitive type for rendering. Attentions: This implies the number
     * of vertices, normals and colors required; e.g. triangles require three
     * vertices each.
     */
    private var primitiveType: GLenum = GLenum(GL_TRIANGLES);
    
    private static let FLOAT_SIZE_IN_BYTES = 4;
    private static let INT_SIZE_IN_BYTES = 4;
    
    private var positionBuffer: [Float]?
    private var normalBuffer: [Float]?
    private var colorBuffer: [Float]?
    private var texCoordsBuffer: [Float]?
    private var indexBuffer: [GLuint]?
    
    public init() {
    }
    
    /**
     * Set the data for the Buffer. The format is described together with the
     * vertices, normals and colors attributes.
     */
    public func setup(_ renderVertices: [RenderVertex], _ primitiveType: GLenum) {
        self.renderVertices = renderVertices;
        self.primitiveType = primitiveType;
    }
    
    /**
     * Init VBO, called only once (or if the date changed).
     */
    private func initialize() {
        if (renderVertices == nil || renderVertices!.count == 0) {
            return;
        }
        
        positionBuffer = createPositionBuffer();
        normalBuffer = createNormalBuffer();
        colorBuffer = createColorBuffer();
        texCoordsBuffer = createTexCoordsBuffer();
        indexBuffer = createIndexBuffer();
        Shader.checkGlError();
        //Log.i(Constants.LOGTAG, "Created VBO buffers for " + renderVertices.size() + " render vertices.");
    }
    
    /**
     * Create position buffer from data.
     */
    private func createPositionBuffer() -> [Float] {
        if ( renderVertices == nil || renderVertices?.count == 0 ){
            preconditionFailure("Invalid render vertex list")
        }
        var data = [Float]();
        for i in 0 ... renderVertices!.count - 1 {
            data.append(Float(renderVertices![i].position.x()))
            data.append(Float(renderVertices![i].position.y()));
            data.append(Float(renderVertices![i].position.z()));
        }
        return data
    }
    
    /**
     * Create normal buffer from data.
     */
    private func createNormalBuffer()  -> [Float] {
        if ( renderVertices == nil || renderVertices?.count == 0 ){
            preconditionFailure("Invalid render vertex list")
        }
        var data = [Float]();
        for i in 0 ... renderVertices!.count - 1 {
            data.append(Float(renderVertices![i].normal.x()))
            data.append(Float(renderVertices![i].normal.y()));
            data.append(Float(renderVertices![i].normal.z()));
        }
        return data
    }
    
    /**
     * Create color buffer from data.
     */
    private func createColorBuffer() -> [Float] {
        if ( renderVertices == nil || renderVertices?.count == 0 ){
            preconditionFailure("Invalid render vertex list")
        }
        var data = [Float]();
        for i in 0 ... renderVertices!.count - 1 {
            data.append(Float(renderVertices![i].color.x()))
            data.append(Float(renderVertices![i].color.y()));
            data.append(Float(renderVertices![i].color.z()));
            data.append(Float(renderVertices![i].color.w()));
        }
        return data
    }
    
    /**
     * Create texture coordinates buffer from data.
     */
    private func createTexCoordsBuffer() -> [Float] {
        if ( renderVertices == nil || renderVertices?.count == 0 ){
            preconditionFailure("Invalid render vertex list")
        }
        var data = [Float]();
        for i in 0 ... renderVertices!.count - 1 {
            data.append(Float(renderVertices![i].texCoords.x()))
            data.append(Float(renderVertices![i].texCoords.y()));
        }
        return data
    }
    
    private func createIndexBuffer() -> [GLuint] {
        if ( renderVertices == nil || renderVertices?.count == 0 ){
            preconditionFailure("Invalid render vertex list")
        }
        var data = [GLuint]();
        for i in 0 ... renderVertices!.count - 1 {
            data.append(GLuint(i))
        }
        return data
    }
    
    /**
     * Draw using the VBO
     */
    public func draw() {
        if (renderVertices == nil || renderVertices?.count == 0 ){
            NSLog("No render vertices provided")
            return
        }
        
        if (positionBuffer == nil || normalBuffer == nil || colorBuffer == nil) {
            initialize();
        }
        
        glEnableVertexAttribArray(
            GLuint(ShaderAttributes.getInstance().getVertexLocation()));
        glEnableVertexAttribArray(
            GLuint(ShaderAttributes.getInstance().getNormalLocation()));
        glEnableVertexAttribArray(
            GLuint(ShaderAttributes.getInstance().getColorLocation()));
        glEnableVertexAttribArray(
            GLuint(ShaderAttributes.getInstance().getTexCoordsLocation()));
        
        glVertexAttribPointer( GLuint(ShaderAttributes.getInstance().getVertexLocation()), 3, GLenum(GL_FLOAT), GLboolean(false), 0, positionBuffer);
        glVertexAttribPointer( GLuint(ShaderAttributes.getInstance().getNormalLocation()), 3, GLenum(GL_FLOAT), GLboolean(false), 0, normalBuffer);
        glVertexAttribPointer( GLuint(ShaderAttributes.getInstance().getColorLocation()), 4, GLenum(GL_FLOAT), GLboolean(false), 0, colorBuffer);
        glVertexAttribPointer( GLuint(ShaderAttributes.getInstance().getTexCoordsLocation()), 2, GLenum(GL_FLOAT), GLboolean(false), 0, texCoordsBuffer);
        
        glDrawElements(primitiveType, GLsizei(renderVertices!.count), GLenum(GL_UNSIGNED_INT), indexBuffer);
        
        Shader.checkGlError();
    }
    
    /**
     * Updates the values in the position buffer from the data array.
     *
     * @param newPositions Float array containing the positions in xyzxyy... format
     */
    public func updatePositionBuffer(_ newPositions: [Float], _ newTextureCoordinates: [Float]) {
        self.positionBuffer = newPositions
        self.texCoordsBuffer = newTextureCoordinates
    }
    
    /**
     * Delete all buffers.
     */
    public func invalidate() {
        positionBuffer = nil;
        normalBuffer = nil;
        colorBuffer = nil;
        texCoordsBuffer = nil;
    }
}
