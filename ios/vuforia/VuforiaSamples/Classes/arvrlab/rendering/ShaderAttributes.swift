//
//  ShaderAttributes.swift
//  ComputergraphicsAR
//
//  Created by Philipp Jenke on 21.11.17.
//  Copyright © 2017 PTC. All rights reserved.
//

import Foundation
import OpenGLES

/**
 * Singleton class to provide access to the shader program locations.
 *
 * @author Philipp Jenke
 */
public class ShaderAttributes {
    
    /**
     * Uniform and attribute parameter location in the shader program.
     */
    private static var locationShaderMode: Int32 = -1;
    private static var locationCameraPosition: Int32 = -1;
    private static var locationLightPosition: Int32 = -1;
    private static var locationModelMatrix: Int32 = -1;
    private static var locationViewMatrix: Int32 = -1;
    private static var locationProjectionMatrix: Int32 = -1;
    private static var locationVertex: Int32 = -1;
    private static var locationColor: Int32 = -1;
    private static var locationNormal: Int32 = -1;
    private static var locationTexCoords: Int32 = -1;
    
    /**
     * Singleton instance
     */
    static private var instance: ShaderAttributes?
    
    private init() {
    }
    
    /**
     * Getter for the singleton instance.
     */
    public static func getInstance() -> ShaderAttributes{
        if (instance == nil) {
            instance = ShaderAttributes();
        }
        return instance!;
    }
    
    /**
     * Read the current attribute locations from the shader, must be called after
     * shader link!
     */
    public func getAttributes(_ shaderProgram: GLuint) {
        assert (shaderProgram >= 0);
        ShaderAttributes.locationVertex = glGetAttribLocation(shaderProgram, "inVertex");
        Shader.checkGlError("shader location error: inVertex");
        ShaderAttributes.locationNormal = glGetAttribLocation(shaderProgram, "inNormal");
        Shader.checkGlError("shader location error: inNormal");
        ShaderAttributes.locationColor = glGetAttribLocation(shaderProgram, "inColor");
        Shader.checkGlError("shader location error: inColor");
        ShaderAttributes.locationTexCoords = glGetAttribLocation(shaderProgram, "inTexCoords");
        Shader.checkGlError("shader location error: inTexCoords");
        ShaderAttributes.locationCameraPosition = glGetUniformLocation(shaderProgram, "camera_position");
        Shader.checkGlError("shader location error: camera_position");
        ShaderAttributes.locationShaderMode = glGetUniformLocation(shaderProgram, "shaderMode");
        Shader.checkGlError("shader location error: shaderMode");
        ShaderAttributes.locationLightPosition = glGetUniformLocation(shaderProgram, "lightPosition");
        Shader.checkGlError("shader location error: lightPosition");
        ShaderAttributes.locationModelMatrix = glGetUniformLocation(shaderProgram, "modelMatrix");
        Shader.checkGlError("shader location error: modelMatrix");
        ShaderAttributes.locationViewMatrix = glGetUniformLocation(shaderProgram, "viewMatrix");
        Shader.checkGlError("shader location error: viewMatrix");
        ShaderAttributes.locationProjectionMatrix = glGetUniformLocation(shaderProgram, "projectionMatrix");
        Shader.checkGlError("shader location error: projectionMatrix");
    }
    
    public func getVertexLocation() -> Int32{
        return ShaderAttributes.locationVertex;
    }
    
    public func getNormalLocation() -> Int32 {
        return ShaderAttributes.locationNormal;
    }
    
    public func getColorLocation() -> Int32{
        return ShaderAttributes.locationColor;
    }
    
    public func getTexCoordsLocation() -> Int32{
        return ShaderAttributes.locationTexCoords;
    }
    
    public func setCameraEyeParameter(_ eye: Vector) {
        if (ShaderAttributes.locationCameraPosition >= 0) {
            glUniform3f(ShaderAttributes.locationCameraPosition, Float(eye.x()), Float(eye.y()), Float(eye.z()));
            Shader.checkGlError();
        }
    }
    
    public func setLightPositionParameter(_ lightPosition: Vector) {
        if (ShaderAttributes.locationLightPosition >= 0) {
            glUniform3f(ShaderAttributes.locationLightPosition, Float(lightPosition.x()), Float(lightPosition.y()), Float(lightPosition.z()));
            Shader.checkGlError();
        }
    }
    
    public func setModelMatrixParameter(_ modelMatrix: Matrix) {
        if (ShaderAttributes.locationModelMatrix >= 0) {
            glUniformMatrix4fv(ShaderAttributes.locationModelMatrix, 1, GLboolean(false), modelMatrix.floatData());
            // System.out.println("model matrix " + modelMatrix);
            Shader.checkGlError();
        }
    }
    
    public func setViewMatrixParameter(_ viewMatrix: Matrix) {
        if (ShaderAttributes.locationViewMatrix >= 0) {
            glUniformMatrix4fv(ShaderAttributes.locationViewMatrix, 1, GLboolean(false), viewMatrix.floatData());
            // System.out.println("view matrix " + viewMatrix);
            Shader.checkGlError();
        }
    }
    
    public func setProjectionMatrixParameter(_ projectionMatrix: Matrix) {
        if (ShaderAttributes.locationProjectionMatrix >= 0) {
            glUniformMatrix4fv(ShaderAttributes.locationProjectionMatrix, 1, GLboolean(false), projectionMatrix.floatData());
            // System.out.println("projection matrix " + projectionMatrix);
            Shader.checkGlError();
        }
    }
    
    public func setShaderModeParameter(_ mode: Shader.ShaderMode) {
        if (ShaderAttributes.locationShaderMode < 0) {
            return;
        }
        var value: Int32 = 0;
        switch (mode) {
        case Shader.ShaderMode.PHONG:
            value = 0;
            break;
        case Shader.ShaderMode.TEXTURE:
            value = 1;
            break;
        case Shader.ShaderMode.NO_LIGHTING:
            value = 2;
            break;
        case Shader.ShaderMode.AMBIENT_ONLY:
            value = 3;
            break;
        }
        glUniform1i(ShaderAttributes.locationShaderMode, value);
        Shader.checkGlError();
    }
}
