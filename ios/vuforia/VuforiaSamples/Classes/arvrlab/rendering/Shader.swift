//
//  Shader.swift
//  VuforiaSamples
//
//  Created by Philipp Jenke on 20.11.17.
//  Copyright © 2017 PTC. All rights reserved.
//

import Foundation
import OpenGLES

/**
 * Representation of GLSL shader.
 *
 * @author Philipp Jenke
 */
public class Shader {
    /**
     * Shader constants.
     */
    static let COMPILE_STATUS_OK = 1;
    
    public enum ShaderType {
        case VERTEX, FRAGMENT
    }
    
    public enum ShaderMode {
        case PHONG, TEXTURE, NO_LIGHTING, AMBIENT_ONLY
    }
    
    /**
     * Flag for the state of the shaders
     */
    private var compiled = false;
    
    /**
     * ID of the shader program.
     */
    private var shaderProgram: GLuint = 0;
    
    /**
     * Filename of the vertex shader source
     */
    private var vertexShaderFilename = "";
    
    /**
     * Filename of the pixel shader source
     */
    private var fragmentShaderFilename = "";
    
    init() {
        self.vertexShaderFilename = "shader/vertex_shader.glsl";
        self.fragmentShaderFilename = "shader/fragment_shader.glsl";
    }
    
    init(_ vertexShaderFilename: String, _ fragmentShaderFilename: String) {
        self.vertexShaderFilename = vertexShaderFilename;
        self.fragmentShaderFilename = fragmentShaderFilename;
    }
    
    /**
     * Compile and link the shaders.
     */
    func compileAndLink() {
        compiled = true;
    
        Shader.checkGlError("before compile shader");
    
        // Compile
        let v = compileShader(getGlShaderType(Shader.ShaderType.VERTEX), vertexShaderFilename);
        let f = compileShader(getGlShaderType(Shader.ShaderType.FRAGMENT), fragmentShaderFilename);
        if (v < 0 || f < 0) {
            NSLog("Shader not created.");
            return;
        }
    
        Shader.checkGlError("before link shader");
    
        // Link
        shaderProgram = linkProgram(v, f);
        if (shaderProgram == 0) {
            NSLog("Shader not created.");
            return;
        }
    
        Shader.checkGlError("after link shader");
    
        NSLog("Successfully created shader from vertex shader filename "
            + vertexShaderFilename + " and fragment shader fileame "
            + fragmentShaderFilename);
    }
    
    /**
     * Activate the shader
     */
    func use() {
        if (!isCompiled()) {
            compileAndLink();
            Shader.checkGlError();
        }
        Shader.checkGlError();
        glUseProgram(shaderProgram);
    }
    
    /**
     * Getter.
     */
    func isCompiled() -> Bool{
        return compiled;
    }
    
    /**
     * Read a shader code from a source file to a String.
     */
    static func readShaderSource(_ shaderFilename: String) -> String? {
        return AssetPath.getInstance().readTextFileToString(shaderFilename)
    }
    
    /**
     * Convert to GL shader constants.
     */
    func getGlShaderType(_ type: ShaderType) -> GLuint {
        if (type == Shader.ShaderType.VERTEX) {
            return GLuint(GL_VERTEX_SHADER);
        } else if (type == Shader.ShaderType.FRAGMENT) {
            return GLuint(GL_FRAGMENT_SHADER);
        } else {
            return 0;
        }
    }
    
    /**
     * Link the vertex and fragment shaders.
     */
    func linkProgram(_ vertexShaderId: GLuint, _ fragmentShaderId: GLuint) -> GLuint{
        Shader.checkGlError("before link shader");
        let shaderProgram = glCreateProgram();
        Shader.checkGlError("before attach shader");
        glAttachShader(shaderProgram, vertexShaderId);
        glAttachShader(shaderProgram, fragmentShaderId);
        Shader.checkGlError("before link shader program");
        glLinkProgram(shaderProgram);
        Shader.checkGlError("before validate shader program");
        glValidateProgram(shaderProgram);
        Shader.checkGlError("after shader link");
        return shaderProgram;
    }
    
    /**
     * Compile the specified shader from the filename and return the OpenGL id.
     */
    func compileShader(_ shaderType: GLuint, _ shaderFilename: String) -> GLuint {
        if let src = Shader.readShaderSource(shaderFilename){
            let id = compileShaderFromSource(shaderType, src);
            if (id == 0) {
                NSLog("Compile error in shader file \(shaderFilename) of type \(shaderType)");
            }
            return id;
        }
        NSLog("Compile error in shader file \(shaderFilename) of type \(shaderType)");
        return 0;
    }
    
    /**
     * Compile the specified shader from the filename and return the OpenGL id.
     */
    func compileShaderFromSource(_ shaderType: GLuint, _ shaderSource: String) -> GLuint{
        let id = glCreateShader(shaderType);
        let shaderStringUTF8 = shaderSource.cString(using: String.defaultCStringEncoding)
        var shaderStringUTF8Pointer = UnsafePointer<GLchar>(shaderStringUTF8)
        glShaderSource(id, GLsizei(1), &shaderStringUTF8Pointer, nil)
        glCompileShader(id);
        if (Shader.checkCompileError(id)) {
            let errorMsg = getCompileErrorMessage(id);
            NSLog("Shader \(shaderType) compile error!\n \(errorMsg)");
            return 0;
        }
        return id;
    }
    
    /**
     * Extract the error message.
     */
    func getCompileErrorMessage(_ id: GLuint) -> String {
        var logLength: GLint = 1;
        glGetShaderiv(id, GLenum(GL_INFO_LOG_LENGTH), &logLength);
        let errorMessage = "";
        if (logLength > 0) {
            var compileLog = [CChar](repeating:0, count:Int(logLength))
            glGetShaderInfoLog(id, logLength, &logLength, &compileLog)
            return "\(compileLog)";
        }
        return errorMessage;
    }
    
    /**
     * Check if a compile error (vertex or fragment shader) occurred?
     */
    static func checkCompileError(_ id: GLuint) -> Bool {
        var intBuffer: GLint = 1;
        glGetShaderiv(id, GLenum(GL_COMPILE_STATUS), &intBuffer);
        return intBuffer != COMPILE_STATUS_OK;
    }
    
    static func checkGlError() {
        checkGlError("");
    }
    
    static func checkGlError(_ msg: String) {
        var glErrorMap = [Int32: String]();
        glErrorMap[GL_NO_ERROR] = "GL_NO_ERROR";
        glErrorMap[GL_INVALID_ENUM] = "GL_INVALID_ENUM";
        glErrorMap[GL_INVALID_VALUE] = "GL_INVALID_VALUE";
        glErrorMap[GL_INVALID_OPERATION] = "GL_INVALID_OPERATION";
        glErrorMap[GL_OUT_OF_MEMORY] = "GL_OUT_OF_MEMORY";
        glErrorMap[GL_INVALID_FRAMEBUFFER_OPERATION] = "GL_INVALID_FRAMEBUFFER_OPERATION";
        var err = GL_NO_ERROR;
        repeat {
            err = Int32(glGetError());
            if (err != GL_NO_ERROR) {
                if (glErrorMap[err] != nil) {
                    NSLog("OpenGL ES error \(msg): \(glErrorMap[err]!)");
                } else {
                    NSLog("OpenGL ES error \(msg): \(err)");
                }
            }
        } while (err != GL_NO_ERROR);
    }
    
    func getProgram() -> GLuint{
        return shaderProgram;
    }
}
