//
//  Texture.swift
//  VuforiaSamples
//
//  Created by Philipp Jenke on 20.11.17.
//  Copyright © 2017 PTC. All rights reserved.
//

import Foundation
import OpenGLES
import GLKit

/**
 * Represents an OpenGL ES texture.
 */
public class Texture {
    
    private var textureId: GLuint = 0;
    private var filename : String?
    
    init(_ filename: String) {
        self.filename = filename;
    }
    
    /**
     * Bind the texture as current texture.
     */
    func bind() {
        if (textureId == 0) {
            loadTexture();
        }
        glBindTexture(GLenum(GL_TEXTURE_2D), textureId);
        Shader.checkGlError();
    }
    
    /**
     * Load a texture from a resource id, return the texture id:
     *
     * @return Texture id
     */
    func loadTexture() {
        let path = Bundle.main.path(forResource: filename, ofType: nil)!
        let option = [ GLKTextureLoaderOriginBottomLeft: true]
        do {
            let info = try GLKTextureLoader.texture(withContentsOfFile: path, options: option as [String : NSNumber]?)
            NSLog("Loaded texture with size \(info.width)x\(info.height).")
            textureId = info.name
        } catch {
        }

//        GLES20.glGenTextures(1, textureHandle, 0);
//        GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, textureHandle[0]);
//        GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_WRAP_S, GLES20.GL_REPEAT);
//        GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_WRAP_T, GLES20.GL_REPEAT);
//        GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_MAG_FILTER, GLES20.GL_NEAREST);
//        GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_MIN_FILTER, GLES20.GL_NEAREST);
//        GLUtils.texImage2D(GLES20.GL_TEXTURE_2D, 0, bitmap, 0);
//        NSLog("Successfully created texture bitmap of size \(bitmap.getWidth()) x \(bitmap.getHeight()).");
//        Shader.checkGlError("loadTexture");
//        return textureHandle[0];
    }
}
