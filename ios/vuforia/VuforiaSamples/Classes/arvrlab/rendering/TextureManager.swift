//
//  TextureManager.swift
//  ComputergraphicsAR
//
//  Created by Philipp Jenke on 22.11.17.
//  Copyright © 2017 PTC. All rights reserved.
//

import Foundation

/**
 * This managers keeps track of all registered textures which can be used in a scene.
 *
 * @author Philipp Jenke
 */
public class TextureManager {
    
    private var textures = [String:Texture]();
    
    private static var instance: TextureManager?;
    
    private init() {
    
    }
    
    public static func getInstance() -> TextureManager {
        if (instance == nil) {
            instance = TextureManager();
        }
        return instance!;
    }
    
    /**
     * Return the texure for the texture name.
     */
    public func getTexture(_ textureName: String) -> Texture{
        if textures[textureName] == nil {
            textures[textureName] = Texture(textureName)
        }
        return textures[textureName]!
    }
}
