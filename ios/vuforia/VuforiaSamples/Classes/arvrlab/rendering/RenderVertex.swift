//
//  RenderVertex.swift
//  ComputergraphicsAR
//
//  Created by Philipp Jenke on 22.11.17.
//  Copyright © 2017 PTC. All rights reserved.
//

import Foundation

/**
 * Helping data structure to represent a render vertex in a @VertexBufferObject.
 *
 * @author Philipp Jenke
 */
public class RenderVertex {
    /**
     * 3D position.
     */
    public let position = Vector(3);
    
    /**
     * 3D normal.
     */
    public let normal = Vector(3);
    
    /**
     * 4D color.
     */
    public let color = Vector(4);
    
    /**
     * 2D Texture coordinate
     */
    public let texCoords = Vector(2);
    
    public init(_ position: Vector, _ normal: Vector, _ color: Vector) {
        self.position.copy(position);
        self.normal.copy(normal);
        self.color.copy(color);
        self.texCoords.copy(Vector(0,0));
    }
    
    public init(_ position: Vector, _ normal: Vector, _ color: Vector, _ texCoords: Vector) {
        self.position.copy(position);
        self.normal.copy(normal);
        self.color.copy(color);
        self.texCoords.copy(texCoords);
    }
    
}
