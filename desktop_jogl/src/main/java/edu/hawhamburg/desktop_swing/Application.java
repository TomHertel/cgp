package edu.hawhamburg.desktop_swing;


import java.util.Timer;

import com.jogamp.opengl.GLCapabilities;
import com.jogamp.opengl.GLProfile;
import com.jogamp.opengl.glu.GLU;
import edu.hawhamburg.shared.misc.Scene;
import edu.hawhamburg.shared.platformer.PlatformerScene;
import edu.hawhamburg.shared.rendering.OpenGL;
import edu.hawhamburg.shared.rendering.TextureManager;

import javax.swing.JFrame;

/**
 * Central frame for all applications - derive from this class.
 *
 * @author Philipp Jenke
 */
public class Application extends JFrame {
    private static final long serialVersionUID = 2322862744369751274L;

    /**
     * 3D view object.
     */
    private final ComputergraphicsWindow view;

    /**
     * Timer object to create a game loop.
     */
    private Timer timer = new Timer();

    /**
     * Time.
     */
    private int timerCounter = 0;

    /**
     * Indicates a timer tick.
     */
    private boolean timerUpdate = false;

    /**
     * GLU instance
     */
    private GLU glu = null;

    private Scene scene = null;

    /**
     * Constructor
     */
    public Application() {
        glu = new GLU();


        edu.hawhamburg.shared.misc.AssetPath.getInstance().setup(new AssetPathPlatformJogl());
        edu.hawhamburg.shared.misc.Logger.getInstance().setup(
                msg -> System.out.println("[" + edu.hawhamburg.shared.misc.Constants.LOGTAG + "] " + msg));
        OpenGL.instance().setup(new OpenGLPlatformJogl());
        TextureManager.getInstance().setup(new TextureManagerPlatformJogl());

        //scene = new DefaultOpenGLScene(
        //       "shader/vertex_shader_jogl.glsl",
        //        "shader/fragment_shader_jogl.glsl");
        scene = new PlatformerScene("shader/vertex_shader_jogl.glsl",
                "shader/fragment_shader_jogl.glsl");

        GLCapabilities capabilities = new GLCapabilities(GLProfile.getDefault());
        //capabilities.setStencilBits(8);
        view = new ComputergraphicsWindow(capabilities, scene);

        getContentPane().add(view);
        view.requestFocusInWindow();

        // Setup JFrame
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setTitle("WP Computergrafik");
        setSize(960, 540);
        setVisible(true);
    }

    /**
     * Program entry point.
     */
    public static void main(String[] args) {
        new Application();
    }
}

