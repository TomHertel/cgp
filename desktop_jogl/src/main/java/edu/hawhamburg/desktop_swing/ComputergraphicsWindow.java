/**
 * Prof. Philipp Jenke
 * Hochschule für Angewandte Wissenschaften (HAW), Hamburg
 * <p>
 * Base framework for "WP Computergrafik".
 */
package edu.hawhamburg.desktop_swing;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;

import com.jogamp.opengl.GL2;
import com.jogamp.opengl.GLAutoDrawable;
import com.jogamp.opengl.GLCapabilities;
import com.jogamp.opengl.GLEventListener;
import com.jogamp.opengl.awt.GLJPanel;
import com.jogamp.opengl.math.FloatUtil;
import com.jogamp.opengl.math.Matrix4;
import com.jogamp.opengl.util.FPSAnimator;
import edu.hawhamburg.shared.math.Matrix;
import edu.hawhamburg.shared.math.Vector;
import edu.hawhamburg.shared.misc.Scene;
import edu.hawhamburg.shared.misc.Logger;
import edu.hawhamburg.shared.rendering.InteractionController;
import edu.hawhamburg.shared.rendering.ObserverInteractionController;
import edu.hawhamburg.shared.rendering.OpenGL;
import edu.hawhamburg.shared.rendering.Shader;
import edu.hawhamburg.shared.scenegraph.Camera;
import edu.hawhamburg.shared.scenegraph.SceneGraphEvent;

/**
 * This class represents a view for 3D content.
 *
 * @author Philipp Jenke
 */
public class ComputergraphicsWindow extends GLJPanel implements GLEventListener, MouseListener,
        MouseMotionListener, KeyListener {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    private InteractionController controller = new ObserverInteractionController();

    /**
     * Last coordinates of the mouse
     */
    private Vector lastMouseCoordinates = new Vector(-1, -1, 0);

    /**
     * Remember the current button.
     */
    private int currentButton = -1;

    private final Scene scene;

    private boolean glReady = false;

    /**
     * Constructor
     */
    public ComputergraphicsWindow(GLCapabilities capabilities, Scene scene) {
        super(capabilities);
        this.scene = scene;

        addGLEventListener(this);
        addMouseListener(this);
        addMouseMotionListener(this);
        addKeyListener(this);

        // Start the Gl loop.
        FPSAnimator animator = new FPSAnimator(this, 60, false);
        animator.start();
    }

    @Override
    public void init(GLAutoDrawable drawable) {
        GL2 gl = drawable.getGL().getGL2();
        OpenGL.instance().platform().setGLObject(gl);

        gl.glColorMask(true, true, true, false);
        gl.glDepthMask(true);
        gl.glClearColor(1.0f, 1.0f, 1.0f, 1.0f);

        scene.init();
    }

    @Override
    public void reshape(GLAutoDrawable drawable, int x, int y, int w, int h) {
        GL2 gl = drawable.getGL().getGL2();
        OpenGL.instance().platform().setGLObject(gl);
        Logger.getInstance().log("Screen resize: " + w + "x" + h);
        Camera.getInstance().setScreenSize(w, h);
        gl.glViewport(0, 0, Camera.getInstance().getWidth(), Camera.getInstance().getHeight());
        scene.resize(w, h);
        if ( !glReady){
            scene.onSetup(scene.getRoot());
            glReady = true;
        }
    }

    @Override
    public void display(GLAutoDrawable drawable) {
        if ( !glReady ){
            return;
        }
        GL2 gl = drawable.getGL().getGL2();
        OpenGL.instance().platform().setGLObject(gl);

        //gl.glViewport(0, 0, Camera.getInstance().getWidth(), Camera.getInstance().getHeight());

        gl.glClear(GL2.GL_COLOR_BUFFER_BIT);
        gl.glClear(GL2.GL_DEPTH_BUFFER_BIT);

        gl.glLoadIdentity();

        Camera cam = Camera.getInstance();

        float[] projData = new float[16];
        FloatUtil.makePerspective(projData, 0, true, cam.getFovyRadiens(), cam.getAspectRatio(),
                cam.getZNear(), cam.getZFar());
        Matrix cgProjectionMatrix = new Matrix(projData);
        Camera.getInstance().setProjectionMatrix(cgProjectionMatrix);

        Camera.getInstance().setViewMatrixFromEyeRefUp();
        scene.redraw();
        gl.glFlush();
    }

    @Override
    public void dispose(GLAutoDrawable arg0) {
    }

    @Override
    public void mouseClicked(MouseEvent event) {
    }

    @Override
    public void mouseEntered(MouseEvent e) {
    }

    @Override
    public void mouseExited(MouseEvent e) {
    }

    @Override
    public void mousePressed(MouseEvent event) {
        currentButton = event.getButton();
        if (event.getButton() == MouseEvent.BUTTON1) {
            lastMouseCoordinates.set(0, event.getX());
            lastMouseCoordinates.set(1, event.getY());
        }
    }

    @Override
    public void mouseReleased(MouseEvent event) {
        lastMouseCoordinates = new Vector(event.getX(), event.getY(), 0);
        currentButton = -1;
    }

    @Override
    public void mouseDragged(MouseEvent event) {
        if (currentButton == MouseEvent.BUTTON1) {
            if ((lastMouseCoordinates.get(0) > 0)
                    && (lastMouseCoordinates.get(1) > 0)) {
                int deltaX =
                        (int) (event.getX() - lastMouseCoordinates.get(0));
                int deltaY =
                        (int) (event.getY() - lastMouseCoordinates.get(1));
                controller.touchMoved(deltaX, deltaY);
            }
            lastMouseCoordinates.set(0, event.getX());
            lastMouseCoordinates.set(1, event.getY());
        } else if (currentButton == MouseEvent.BUTTON3) {
            if ((lastMouseCoordinates.get(0) > 0)
                    && (lastMouseCoordinates.get(1) > 0)) {
                int deltaY = (int) (event.getY() - lastMouseCoordinates.get(1));
                // TODO
                // scene.zoom(deltaY);
            }
            lastMouseCoordinates.set(0, event.getX());
            lastMouseCoordinates.set(1, event.getY());
        }
    }

    @Override
    public void mouseMoved(MouseEvent event) {

    }

    @Override
    public void keyPressed(KeyEvent e) {
        scene.onKeyPressed(e.getKeyCode());
    }

    @Override
    public void keyReleased(KeyEvent e) {
        scene.onKeyReleased(e.getKeyCode());
    }

    @Override
    public void keyTyped(KeyEvent e) {
    }

}