package edu.hawhamburg.desktop_swing;

import com.jogamp.opengl.GL2;
import com.jogamp.opengl.GLException;
import com.jogamp.opengl.util.texture.TextureIO;
import edu.hawhamburg.shared.misc.AssetPath;
import edu.hawhamburg.shared.rendering.OpenGL;
import edu.hawhamburg.shared.rendering.Texture;
import edu.hawhamburg.shared.rendering.TextureManagerPlatform;
import edu.hawhamburg.shared.misc.Logger;

import java.io.File;
import java.io.IOException;

public class TextureManagerPlatformJogl implements TextureManagerPlatform {
    @Override
    public Texture getTexture(String textureName) {
        return new Texture(load(textureName));
    }

    /**
     * Load texture image from file and create GL texture object.
     */
    public int load(String filename) {
        com.jogamp.opengl.util.texture.Texture texture = null;
        try {
            texture = TextureIO.newTexture(new File(AssetPath.getInstance().getPathToAsset(filename)), true);
        } catch (GLException | IOException e) {
            System.out.println("Failed to load texture from image.");
            return -1;
        }
        if (texture == null) {
            System.out.println("Failed to load texture from image.");
            return -1;
        }

        GL2 gl = (GL2)OpenGL.instance().platform().getOpenGLObject();

        int textureId = texture.getTextureObject(gl);

        gl.glEnable(GL2.GL_TEXTURE_2D);
        gl.glBindTexture(GL2.GL_TEXTURE_2D, textureId);
        gl.glTexParameteri(GL2.GL_TEXTURE_2D, GL2.GL_TEXTURE_WRAP_S, GL2.GL_REPEAT);
        gl.glTexParameteri(GL2.GL_TEXTURE_2D, GL2.GL_TEXTURE_WRAP_T, GL2.GL_REPEAT);
        gl.glTexParameteri(GL2.GL_TEXTURE_2D, GL2.GL_TEXTURE_MAG_FILTER, GL2.GL_NEAREST);
        gl.glTexParameteri(GL2.GL_TEXTURE_2D, GL2.GL_TEXTURE_MIN_FILTER, GL2.GL_NEAREST);
        Logger.getInstance().log("Texture " + filename + " loaded.");
        return textureId;
    }
}
