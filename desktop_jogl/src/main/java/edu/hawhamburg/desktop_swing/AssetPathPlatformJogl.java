package edu.hawhamburg.desktop_swing;

import edu.hawhamburg.shared.misc.AssetPathPlatform;
import edu.hawhamburg.shared.misc.Logger;

import java.io.*;

public class AssetPathPlatformJogl implements AssetPathPlatform {

    private static String[] assetPaths = {"assets/", "../android/assets/", "assets/meshes/", "assets/textures/"};

    @Override
    public void init(Object manager) {
        throw new IllegalArgumentException("not implemented yet");
    }

    @Override
    public String readTextFileToString(String relativeFilename) {

        String path = getPathToAsset(relativeFilename);
        if (path == null) {
            throw new IllegalArgumentException("Failed to read file " + relativeFilename);
        }
        BufferedReader br = null;
        try {
            br = new BufferedReader(new FileReader(path));
            StringBuilder sb = new StringBuilder();
            String line = br.readLine();
            while (line != null) {
                sb.append(line);
                sb.append("\n");
                line = br.readLine();
            }
            br.close();
            return sb.toString();
        } catch (FileNotFoundException e) {
        } catch (IOException e) {
        }
        throw new IllegalArgumentException("Failed to read file " + relativeFilename);
    }

    @Override
    public InputStream readTextFileToStream(String relativeFilename) {
        String path = getPathToAsset(relativeFilename);
        if (path == null) {
            throw new IllegalArgumentException("Failed to read file " + relativeFilename);
        }
        File initialFile = new File(path);
        try {
            InputStream stream = new FileInputStream(initialFile);
            return stream;
        } catch (FileNotFoundException e) {
            throw new IllegalArgumentException("Failed to open file " + relativeFilename);
        }
    }

    @Override
    public String getPathToAsset(String filename) {
        for (String assetPath : assetPaths) {
            String path = assetPath + filename;
            File file = new File(path);
            if (file.exists()) {
                return path;
            }
        }
        return null;
    }
}
