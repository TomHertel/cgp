package edu.hawhamburg.desktop_swing;

import edu.hawhamburg.shared.math.Vector;
import edu.hawhamburg.shared.misc.Scene;
import edu.hawhamburg.shared.scenegraph.*;

import java.util.List;

/**
 * Dummy scene with rather simple content.
 *
 * @author Philipp Jenke
 */
public class DefaultOpenGLScene extends Scene {
    public DefaultOpenGLScene(String vertexShaderFilename, String fragmentShaderFilename) {
        super(100, INode.RenderMode.REGULAR, vertexShaderFilename, fragmentShaderFilename);
    }

    @Override
    public void onSetup(InnerNode rootNode) {
        VuforiaMarkerSimulationNode vuforiaMarkerSimulationNode = new VuforiaMarkerSimulationNode();
        rootNode.addChild(vuforiaMarkerSimulationNode);

        double radius = 0.25;
        SphereNode sphereNode = new SphereNode(radius, 10);
        TranslationNode t = new TranslationNode(new Vector(0,radius,0));
        t.addChild(sphereNode);
        vuforiaMarkerSimulationNode.addChild(t);

        // Test triangle mesh functionality
//        ObjReader reader = new ObjReader();
//        List<ITriangleMesh> meshes = reader.read(new TriangleMeshFactory(), "meshes/xwing.obj");
//        for ( ITriangleMesh mesh : meshes ){
//            rootNode.addChild(new TriangleMeshNode(mesh));
//        }
    }

    @Override
    public void onTimerTick(int counter) {
    }

    @Override
    public void onSceneRedraw() {

    }

    @Override
    public void onKeyPressed(int keyCode) {

    }

    @Override
    public void onKeyReleased(int keyCode) {

    }
}
