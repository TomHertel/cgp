package edu.hawhamburg.desktop_swing;

import edu.hawhamburg.shared.math.AxisAlignedBoundingBox;
import edu.hawhamburg.shared.math.Matrix;
import edu.hawhamburg.shared.math.Vector;
import edu.hawhamburg.shared.rendering.*;
import edu.hawhamburg.shared.scenegraph.INode;
import edu.hawhamburg.shared.scenegraph.LeafNode;
import edu.hawhamburg.shared.scenegraph.SceneGraphEvent;

import java.util.ArrayList;
import java.util.List;

/**
 * Simulates a vuforia marker node controlled by keyboard events
 */
public class VuforiaMarkerSimulationNode extends LeafNode {

    private static final double SIDE_LENGTH = 0.5;
    private Texture texture = null;
    private Matrix transformation = Matrix.createIdentityMatrix4();
    double[] rollPitchYaw = {0, 0, 0};
    Vector translation = new Vector(0, -0.5, 0);

    /**
     * List of child nodes.
     */
    private List<INode> children = new ArrayList<INode>();

    /**
     * VBO.
     */
    private VertexBufferObject vbo = new VertexBufferObject();

    public VuforiaMarkerSimulationNode() {
        updateTransformation();
        createVbo();
    }

    private void createVbo() {
        List<RenderVertex> renderVertices = new ArrayList<RenderVertex>();
        Vector p0 = new Vector(-SIDE_LENGTH, 0, -SIDE_LENGTH);
        Vector t0 = new Vector(0, 0);
        Vector p1 = new Vector(SIDE_LENGTH, 0, -SIDE_LENGTH);
        Vector t1 = new Vector(1, 0);
        Vector p2 = new Vector(SIDE_LENGTH, 0, SIDE_LENGTH);
        Vector t2 = new Vector(1, 1);
        Vector p3 = new Vector(-SIDE_LENGTH, 0, SIDE_LENGTH);
        Vector t3 = new Vector(0, 1);
        Vector n = new Vector(0, 1, 0);
        Vector color = new Vector(0.25, 0.75, 0.25, 1);
        renderVertices.add(new RenderVertex(p0, n, color, t0));
        renderVertices.add(new RenderVertex(p3, n, color, t3));
        renderVertices.add(new RenderVertex(p1, n, color, t1));
        renderVertices.add(new RenderVertex(p3, n, color, t3));
        renderVertices.add(new RenderVertex(p2, n, color, t2));
        renderVertices.add(new RenderVertex(p1, n, color, t1));
        vbo.setup(renderVertices, OpenGLPlatform.Primitive.TRIANGLES);
    }

    @Override
    public void drawGL(RenderMode mode, Matrix modelMatrix) {
        if (texture == null) {
            texture = TextureManager.getInstance().getTexture("textures/elphi.png");

        }
        texture.bind();
        ShaderAttributes.getInstance().setShaderModeParameter(Shader.ShaderMode.TEXTURE);
        vbo.draw();
    }

    @Override
    public AxisAlignedBoundingBox getBoundingBox() {
        return new AxisAlignedBoundingBox(new Vector(-SIDE_LENGTH, -SIDE_LENGTH, -SIDE_LENGTH),
                new Vector(SIDE_LENGTH, SIDE_LENGTH, SIDE_LENGTH));
    }

//    @Override
//    public void eventHappened(SceneGraphEvent event) {
//        double translationOffset = 0.05;
//        double rotationOffset = 0.1;
//        if (event.isKeyEvent()){
//            switch (event.getKey()){
//                case 'a':
//                    rollPitchYaw[0] += rotationOffset;
//                    updateTransformation();
//                    break;
//                case 'b':
//                    rollPitchYaw[1] += rotationOffset;
//                    updateTransformation();
//                    break;
//                case 'c':
//                    rollPitchYaw[2] += rotationOffset;
//                    updateTransformation();
//                    break;
//                case 'd':
//                    rollPitchYaw[0] -= rotationOffset;
//                    updateTransformation();
//                    break;
//                case 'e':
//                    rollPitchYaw[1] -= rotationOffset;
//                    updateTransformation();
//                    break;
//                case 'f':
//                    rollPitchYaw[2] -= rotationOffset;
//                    updateTransformation();
//                    break;
//
//                case 'u':
//                    translation.addSelf(new Vector(translationOffset,0,0));
//                    updateTransformation();
//                    break;
//                case 'v':
//                    translation.addSelf(new Vector(0,translationOffset,0));
//                    updateTransformation();
//                    break;
//                case 'w':
//                    translation.addSelf(new Vector(0, 0, translationOffset));
//                    updateTransformation();
//                    break;
//                case 'x':
//                    translation.addSelf(new Vector(-translationOffset,0,0));
//                    updateTransformation();
//                    break;
//                case 'y':
//                    translation.addSelf(new Vector(0,-translationOffset,0));
//                    updateTransformation();
//                    break;
//                case 'z':
//                    translation.addSelf(new Vector(0,0,translationOffset));
//                    updateTransformation();
//                    break;
//            }
//        }
//
//        for (int i = 0; i < children.size(); i++) {
//            children.get(i).eventHappened(event);
//        }
//    }

    @Override
    public synchronized void traverse(RenderMode mode, Matrix modelMatrix) {
        if (!isActive()) {
            return;
        }

        // Leaf node -> draw
        getParentNode().getCombinedTransformation().multiply(transformation);
        super.traverse(mode, getParentNode().getCombinedTransformation().multiply(transformation));

        for (int i = 0; i < children.size(); i++) {
            children.get(i).traverse(mode, getParentNode().getCombinedTransformation().multiply(transformation));
        }
    }

    /**
     * Add new child node.
     **/
    public synchronized void addChild(INode child) {
        child.setParentNode(this);
        children.add(child);
    }

    /**
     * Remove child from children list.
     */
    public synchronized void removeChild(INode node) {
        children.remove(node);
    }

    private void updateTransformation() {
        transformation = Matrix.createTranslationMatrix4(translation).multiply(
                Matrix.createRotationMatrix4(new Vector(1, 0, 0), rollPitchYaw[0])).multiply(
                Matrix.createRotationMatrix4(new Vector(0, 1, 0), rollPitchYaw[1])).multiply(
                Matrix.createRotationMatrix4(new Vector(0, 0, 1), rollPitchYaw[2]));
    }
}
