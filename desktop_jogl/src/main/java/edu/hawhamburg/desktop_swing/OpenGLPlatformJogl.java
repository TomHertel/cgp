package edu.hawhamburg.desktop_swing;

import com.jogamp.opengl.GL2;
import com.jogamp.opengl.math.FloatUtil;
import com.jogamp.opengl.math.Matrix4;
import edu.hawhamburg.shared.math.Matrix;
import edu.hawhamburg.shared.math.Vector;
import edu.hawhamburg.shared.rendering.OpenGLPlatform;
import edu.hawhamburg.shared.misc.Logger;
import edu.hawhamburg.shared.rendering.Shader;

import java.nio.ByteBuffer;
import java.nio.FloatBuffer;
import java.nio.IntBuffer;
import java.util.HashMap;
import java.util.Map;

/**
 * OpenGL platform context for JOGL.
 */
public class OpenGLPlatformJogl implements OpenGLPlatform {

    private GL2 gl = null;

    @Override
    public Vector fixTexCoord(Vector texCoord) {
        return new Vector(texCoord.x(), texCoord.y());
    }

    @Override
    public String getVersionString() {
        String version_string = gl.glGetString(GL2.GL_VERSION);
        String result = "OpenGL-Version: " + version_string + ", ";
        int[] number = {0};
        gl.glGetIntegerv(GL2.GL_STENCIL_BITS, number, 0);
        result += "Stencil buffer bits: " + number[0] + ", ";
        gl.glGetIntegerv(GL2.GL_DEPTH_BITS, number, 0);
        result += "Depth buffer bits: " + number[0] + ", ";
        gl.glGetIntegerv(GL2.GL_RED_BITS, number, 0);
        result += "Red buffer bits: " + number[0] + ", ";
        gl.glGetIntegerv(GL2.GL_GREEN_BITS, number, 0);
        result += "Green buffer bits: " + number[0] + ", ";
        gl.glGetIntegerv(GL2.GL_BLUE_BITS, number, 0);
        result += "Blue buffer bits: " + number[0];
        return result;
    }

    @Override
    public void enableStencilTest(boolean flag) {
        if (flag) {
            gl.glEnable(GL2.GL_STENCIL_TEST);
            Logger.getInstance().log("Stencil test: on");
        } else {
            gl.glDisable(GL2.GL_STENCIL_TEST);
            Logger.getInstance().log("Stencil test: off");
        }
    }

    @Override
    public void setupCulling() {
        gl.glCullFace(GL2.GL_BACK);
        gl.glFrontFace(GL2.GL_CCW);
    }

    @Override
    public void enableDepthTest(boolean flag) {
        if (flag) {
            gl.glEnable(GL2.GL_DEPTH_TEST);
            Logger.getInstance().log("Depth test: on");
        } else {
            gl.glDisable(GL2.GL_DEPTH_TEST);
            Logger.getInstance().log("Depth test: off");
        }
    }

    @Override
    public void enableCulling(boolean flag) {
        if (flag) {
            gl.glEnable(GL2.GL_CULL_FACE);
            Logger.getInstance().log("Culling: on");
        } else {
            gl.glDisable(GL2.GL_CULL_FACE);
            Logger.getInstance().log("Culling: off");
        }
    }

    @Override
    public void enableBlending(boolean flag) {
        if (flag) {
            gl.glEnable(GL2.GL_BLEND);
            gl.glBlendFunc(GL2.GL_SRC_ALPHA, GL2.GL_ONE_MINUS_SRC_ALPHA);
            Logger.getInstance().log("Blending: on");
        } else {
            gl.glDisable(GL2.GL_BLEND);
            Logger.getInstance().log("Blending: off");
        }
    }

    @Override
    public void setupBlending() {
        gl.glBlendFunc(GL2.GL_SRC_ALPHA, GL2.GL_ONE_MINUS_SRC_ALPHA);
    }

    @Override
    public void setGLObject(Object gl) {
        if (gl instanceof GL2) {
            this.gl = (GL2) gl;
        } else {
            Logger.getInstance().log("Invalid GL2 object");
        }
    }

    @Override
    public void enableVertexAttribArray(int location) {
        gl.glEnableVertexAttribArray(location);
    }

    @Override
    public void glVertexAttribPointer(int location, int size, DataType type, boolean normalized,
                                      int stride, FloatBuffer buffer) {
        gl.glVertexAttribPointer(
                location, size, convert2JoglDataType(type),
                normalized, stride, buffer);
    }

    @Override
    public void glDrawElements(Primitive primitiveType, int size, DataType dataType, IntBuffer buffer) {
        gl.glDrawElements(convert2JoglPrimitiveType(primitiveType), size, convert2JoglDataType(dataType), buffer);
    }

    @Override
    public void glUseProgram(int program) {
        gl.glUseProgram(program);
    }

    private int convert2JoglDataType(DataType type) {
        switch (type) {
            case FLOAT:
                return GL2.GL_FLOAT;
            case UNSIGNED_INT:
                return GL2.GL_UNSIGNED_INT;
            default:
                throw new IllegalArgumentException("Invalid data type: " + type);
        }
    }

    private int convert2JoglPrimitiveType(Primitive primitive) {
        switch (primitive) {
            case TRIANGLES:
                return GL2.GL_TRIANGLES;
            case LINE_LOOP:
                return GL2.GL_LINE_LOOP;
            case LINES:
                return GL2.GL_LINES;
            case POINTS:
                return GL2.GL_POINTS;
            default:
                throw new IllegalArgumentException("Unsupported primitive: " + primitive);
        }
    }

    /**
     * Shader constants.
     */
    private static final int COMPILE_STATUS_OK = 1;

    @Override
    public int glCreateProgram() {
        return gl.glCreateProgram();
    }

    @Override
    public void glAttachShader(int shaderProgram, int shaderId) {
        gl.glAttachShader(shaderProgram, shaderId);
    }

    @Override
    public void glLinkProgram(int shaderProgram) {
        gl.glLinkProgram(shaderProgram);
    }

    @Override
    public void glValidateProgram(int shaderProgram) {
        gl.glValidateProgram(shaderProgram);
    }

    @Override
    public int glCreateShader(Shader.ShaderType shaderType) {
        switch (shaderType) {
            case VERTEX:
                return gl.glCreateShader(GL2.GL_VERTEX_SHADER);
            case FRAGMENT:
                return gl.glCreateShader(GL2.GL_FRAGMENT_SHADER);
            default:
                throw new IllegalArgumentException("Invalid shader type");
        }
    }

    @Override
    public void glShaderSource(int id, String shaderSource) {
        gl.glShaderSource(id, 1, new String[]{shaderSource}, (int[]) null, 0);
    }

    @Override
    public void glCompileShader(int id) {
        gl.glCompileShader(id);
    }

    @Override
    public boolean checkCompileError(int id) {
        IntBuffer intBuffer = IntBuffer.allocate(1);
        gl.glGetShaderiv(id, GL2.GL_COMPILE_STATUS, intBuffer);
        boolean error = intBuffer.get(0) != COMPILE_STATUS_OK;
        if (error) {
            Logger.getInstance().log(getCompileErrorMessage(id));
        }
        return error;

    }

    @Override
    public int glGetAttribLocation(int shaderProgram, String name) {
        return gl.glGetAttribLocation(shaderProgram, name);
    }

    @Override
    public int glGetUniformLocation(int shaderProgram, String name) {
        return gl.glGetUniformLocation(shaderProgram, name);
    }

    @Override
    public void glUniform3f(int locationCameraPosition, float x, float y, float z) {
        gl.glUniform3f(locationCameraPosition, x, y, z);
    }

    @Override
    public void glUniformMatrix4fv(int location, int i, boolean b, float[] floats, int i1) {
        gl.glUniformMatrix4fv(location, i, b, floats, i1);
    }

    @Override
    public void glUniform1i(int location, int value) {
        gl.glUniform1i(location, value);
    }

    @Override
    public Matrix generateViewMatrix(Vector eye, Vector ref, Vector up) {
        float[] m = new float[16];
        float[] tmp = new float[16];
        FloatUtil.makeLookAt(m, 0, eye.floatData(), 0, ref.floatData(),
                0, up.floatData(), 0, tmp);
        return new Matrix(m);
    }

    @Override
    public Matrix generateOrthoMatrix(int i, int i1, int i2, int i3, int i4, int i5, int i6) {
        //Matrix4 ortho = new Matrix4();
        //ortho.makeOrtho(1, i1, i2, i3, i4, i5, i6, edu.hawhamburg.shared.scenegraph.Camera.getInstance().getZNear(), edu.hawhamburg.shared.scenegraph.Camera.getInstance().getZFar());
        throw new IllegalArgumentException("not implemented yet.");
    }

    @Override
    public void glLineWidth(int width) {
        gl.glLineWidth(width);
    }

    @Override
    public void checkGlError() {
        checkGlError("");
    }

    @Override
    public void clearColorBuffer() {
        gl.glClear(GL2.GL_COLOR_BUFFER_BIT);
    }

    @Override
    public void glBindTexture(int textureId) {
        gl.glBindTexture(GL2.GL_TEXTURE_2D, textureId);
    }

    @Override
    public void glDeleteTextures(int textureId) {
        final int[] textureHandle = new int[1];
        textureHandle[0] = textureId;
        gl.glDeleteTextures(1, textureHandle, 0);
    }

    @Override
    public GL2 getOpenGLObject() {
        return gl;
    }

    public void checkGlError(String msg) {
        Map<Integer, String> glErrorMap = new HashMap<Integer, String>();
        glErrorMap.put(GL2.GL_NO_ERROR, "GL_NO_ERROR");
        glErrorMap.put(GL2.GL_INVALID_ENUM, "GL_INVALID_ENUM");
        glErrorMap.put(GL2.GL_INVALID_VALUE, "GL_INVALID_VALUE");
        glErrorMap.put(GL2.GL_INVALID_OPERATION, "GL_INVALID_OPERATION");
        glErrorMap.put(GL2.GL_OUT_OF_MEMORY, "GL_OUT_OF_MEMORY");
        glErrorMap.put(GL2.GL_INVALID_FRAMEBUFFER_OPERATION,
                "GL_INVALID_FRAMEBUFFER_OPERATION");
        int err = GL2.GL_NO_ERROR;
        do {
            err = gl.glGetError();
            if (err != GL2.GL_NO_ERROR) {
                if (glErrorMap.containsKey(err)) {
                    Logger.getInstance().log("OpenGL ES error (" + msg + "): " + glErrorMap.get(err));
                } else {
                    Logger.getInstance().log("OpenGL ES error (" + msg + "): " + err);
                }
            }
        } while (err != GL2.GL_NO_ERROR);
    }

    /**
     * Extract the error message.
     */
    private String getCompileErrorMessage(int id) {
        IntBuffer intBuffer = IntBuffer.allocate(1);
        gl.glGetShaderiv(id, GL2.GL_INFO_LOG_LENGTH, intBuffer);
        int size = intBuffer.get(0);
        String errorMessage = "";
        if (size > 0) {
            ByteBuffer byteBuffer = ByteBuffer.allocate(size);
            gl.glGetShaderInfoLog(id, size, intBuffer, byteBuffer);
            for (byte b : byteBuffer.array()) {
                errorMessage += (char) b;
            }
        }
        return errorMessage;
    }
}
