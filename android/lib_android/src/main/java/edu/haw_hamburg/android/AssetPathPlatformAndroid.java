package edu.haw_hamburg.android;

import android.content.res.AssetManager;
import android.util.Log;

import java.io.IOException;
import java.io.InputStream;
import java.util.Scanner;

import edu.hawhamburg.shared.misc.AssetPathPlatform;
import edu.hawhamburg.shared.misc.Constants;

/**
 * Android-specific implementation of the AssetPathPlatform.
 */
public class AssetPathPlatformAndroid implements AssetPathPlatform {
    private AssetManager assetManager;

    @Override
    public void init(Object manager) {
        if (manager instanceof AssetManager) {
            this.assetManager = (AssetManager)manager;
        } else {
            throw new IllegalArgumentException("Invalid AssetManager object");
        }
    }

    @Override
    public String readTextFileToString(String relativeFilename) {
        if (assetManager == null) {
            throw new IllegalArgumentException("AssetManager must be set first!");
        }

        try {
            InputStream stream = assetManager.open(relativeFilename);
            Scanner s = new Scanner(stream).useDelimiter("\\A");
            String result = s.hasNext() ? s.next() : "";
            stream.close();
            return result;
        } catch (IOException e) {
            Log.e(Constants.LOGTAG, "Failed to open file " + relativeFilename + " in assets folder.");
            return null;
        }
    }

    @Override
    public InputStream readTextFileToStream(String relativeFilename) {
        try {
            InputStream stream = assetManager.open(relativeFilename);
            return stream;
        } catch (IOException e) {
            Log.e(Constants.LOGTAG, "Failed to open file " + relativeFilename + " in assets folder.");
            return null;
        }
    }

    @Override
    public String getPathToAsset(String filename) {
        throw new IllegalArgumentException("Not available on Android platform.");
    }
}
