package edu.haw_hamburg.android;

import android.opengl.GLES20;

import edu.hawhamburg.shared.math.Matrix;
import edu.hawhamburg.shared.math.Vector;
import edu.hawhamburg.shared.rendering.OpenGLPlatform;
import edu.hawhamburg.shared.rendering.Shader;
import edu.hawhamburg.shared.misc.Logger;

import java.nio.ByteBuffer;
import java.nio.FloatBuffer;
import java.nio.IntBuffer;
import java.util.HashMap;
import java.util.Map;

/**
 * OpenGL context access.
 */
public class OpenGLPlatformES implements OpenGLPlatform {

    /**
     * Shader constants.
     */
    private static final int COMPILE_STATUS_OK = 1;

    @Override
    public int glCreateProgram() {
        return GLES20.glCreateProgram();
    }

    @Override
    public void glAttachShader(int shaderProgram, int vertexShaderId) {
        GLES20.glAttachShader(shaderProgram, vertexShaderId);
    }

    @Override
    public void glLinkProgram(int shaderProgram) {
        GLES20.glLinkProgram(shaderProgram);
    }

    @Override
    public void glValidateProgram(int shaderProgram) {
        GLES20.glValidateProgram(shaderProgram);
    }

    @Override
    public int glCreateShader(Shader.ShaderType shaderType) {
        switch (shaderType) {
            case VERTEX:
                return GLES20.glCreateShader(GLES20.GL_VERTEX_SHADER);
            case FRAGMENT:
                return GLES20.glCreateShader(GLES20.GL_FRAGMENT_SHADER);
            default:
                throw new IllegalArgumentException("Invalid shader type");
        }
    }

    @Override
    public void glShaderSource(int id, String shaderSource) {
        GLES20.glShaderSource(id, shaderSource);
    }

    @Override
    public void glCompileShader(int id) {
        GLES20.glCompileShader(id);
    }

    @Override
    public boolean checkCompileError(int id) {
        IntBuffer intBuffer = IntBuffer.allocate(1);
        GLES20.glGetShaderiv(id, GLES20.GL_COMPILE_STATUS, intBuffer);
        boolean error = intBuffer.get(0) != COMPILE_STATUS_OK;
        if (error) {
            Logger.getInstance().log(getCompileErrorMessage(id));
        }
        return error;

    }

    @Override
    public int glGetAttribLocation(int shaderProgram, String name) {
        return GLES20.glGetAttribLocation(shaderProgram, name);
    }

    @Override
    public int glGetUniformLocation(int shaderProgram, String name) {
        return GLES20.glGetUniformLocation(shaderProgram, name);
    }

    @Override
    public void glUniform3f(int locationCameraPosition, float x, float y, float z) {
        GLES20.glUniform3f(locationCameraPosition, x, y, z);
    }

    @Override
    public void glUniformMatrix4fv(int location, int i, boolean b, float[] floats, int i1) {
        GLES20.glUniformMatrix4fv(location, i, b, floats, i1);
    }

    @Override
    public void glUniform1i(int location, int value) {
        GLES20.glUniform1i(location, value);
    }

    @Override
    public Matrix generateViewMatrix(Vector eye, Vector ref, Vector up) {
        float[] viewMatrixData = new float[16];
        android.opengl.Matrix.setLookAtM(viewMatrixData, 0, (float) eye.x(), (float) eye.y(),
                (float) eye.z(), (float) ref.x(), (float) ref.y(), (float) ref.z(), (float) up.x(),
                (float) up.y(), (float) up.z());
        return new Matrix(viewMatrixData);
    }

    @Override
    public Matrix generateOrthoMatrix(int i, int i1, int i2, int i3, int i4, int i5, int i6) {
        float[] orthoMatrixData = new float[16];
        android.opengl.Matrix.orthoM(orthoMatrixData, i, i1, i2, i3, i4, i5, i6);
        return new Matrix(orthoMatrixData);
    }

    @Override
    public void glLineWidth(int width) {
        GLES20.glLineWidth(width);
    }

    @Override
    public void checkGlError() {
        checkGlError("");
    }

    public void checkGlError(String msg) {
        Map<Integer, String> glErrorMap = new HashMap<Integer, String>();
        glErrorMap.put(GLES20.GL_NO_ERROR, "GL_NO_ERROR");
        glErrorMap.put(GLES20.GL_INVALID_ENUM, "GL_INVALID_ENUM");
        glErrorMap.put(GLES20.GL_INVALID_VALUE, "GL_INVALID_VALUE");
        glErrorMap.put(GLES20.GL_INVALID_OPERATION, "GL_INVALID_OPERATION");
        glErrorMap.put(GLES20.GL_OUT_OF_MEMORY, "GL_OUT_OF_MEMORY");
        glErrorMap.put(GLES20.GL_INVALID_FRAMEBUFFER_OPERATION,
                "GL_INVALID_FRAMEBUFFER_OPERATION");
        int err = GLES20.GL_NO_ERROR;
        do {
            err = GLES20.glGetError();
            if (err != GLES20.GL_NO_ERROR) {
                if (glErrorMap.containsKey(err)) {
                    Logger.getInstance().log("OpenGL ES error (" + msg + "): " + glErrorMap.get(err));
                } else {
                    Logger.getInstance().log("OpenGL ES error (" + msg + "): " + err);
                }
            }
        } while (err != GLES20.GL_NO_ERROR);
    }

    @Override
    public void clearColorBuffer() {
        GLES20.glClear(GLES20.GL_COLOR_BUFFER_BIT);
    }

    /**
     * Extract the error message.
     */
    private String getCompileErrorMessage(int id) {
        IntBuffer intBuffer = IntBuffer.allocate(1);
        GLES20.glGetShaderiv(id, GLES20.GL_INFO_LOG_LENGTH, intBuffer);
        int size = intBuffer.get(0);
        String errorMessage = "";
        if (size > 0) {
            ByteBuffer byteBuffer = ByteBuffer.allocate(size);
            return GLES20.glGetShaderInfoLog(id);
        }
        return errorMessage;
    }

    @Override
    public String getVersionString() {
        String version_string = GLES20.glGetString(GLES20.GL_VERSION);
        String result = "OpenGL-Version: " + version_string + "\n";
        int[] number = {0};
        GLES20.glGetIntegerv(GLES20.GL_STENCIL_BITS, number, 0);
        result += "Stencil buffer bits: " + number[0] + "\n";
        GLES20.glGetIntegerv(GLES20.GL_DEPTH_BITS, number, 0);
        result += "Depth buffer bits: " + number[0];
        return result;
    }

    @Override
    public void enableStencilTest(boolean flag) {
        if (flag) {
            GLES20.glEnable(GLES20.GL_STENCIL_TEST);
        } else {
            GLES20.glDisable(GLES20.GL_STENCIL_TEST);
        }
    }

    @Override
    public void setupCulling() {
        GLES20.glCullFace(GLES20.GL_BACK);
        GLES20.glFrontFace(GLES20.GL_CCW);
    }

    @Override
    public void enableDepthTest(boolean flag) {
        if (flag) {
            GLES20.glEnable(GLES20.GL_DEPTH_TEST);
        } else {
            GLES20.glDisable(GLES20.GL_DEPTH_TEST);
        }
    }

    @Override
    public void enableCulling(boolean flag) {
        if (flag) {
            GLES20.glEnable(GLES20.GL_CULL_FACE);
        } else {
            GLES20.glDisable(GLES20.GL_CULL_FACE);
        }
    }

    @Override
    public void enableBlending(boolean flag) {
        if (flag) {
            GLES20.glEnable(GLES20.GL_BLEND);
            GLES20.glBlendFunc(GLES20.GL_SRC_ALPHA, GLES20.GL_ONE_MINUS_SRC_ALPHA);
        } else {
            GLES20.glDisable(GLES20.GL_BLEND);
        }
    }

    @Override
    public void setupBlending() {
        GLES20.glBlendFunc(GLES20.GL_SRC_ALPHA, GLES20.GL_ONE_MINUS_SRC_ALPHA);
    }

    @Override
    public void setGLObject(Object gl) {
        // Not used in android
    }

    @Override
    public void enableVertexAttribArray(int location) {
        GLES20.glEnableVertexAttribArray(location);
    }


    @Override
    public void glVertexAttribPointer(int location, int size, DataType type, boolean normalized, int stride, FloatBuffer buffer
    ) {
        GLES20.glVertexAttribPointer(
                location, size, convert2GLES20DataType(type),
                normalized, stride, buffer);
    }


    @Override
    public void glDrawElements(Primitive primitiveType, int size, DataType dataType, IntBuffer buffer) {
        GLES20.glDrawElements(convert2GLES20PrimitiveType(primitiveType), size, convert2GLES20DataType(dataType), buffer);
    }

    private int convert2GLES20PrimitiveType(Primitive primitive) {
        switch (primitive) {
            case TRIANGLES:
                return GLES20.GL_TRIANGLES;
            case LINE_LOOP:
                return GLES20.GL_LINE_LOOP;
            case LINES:
                return GLES20.GL_LINES;
            case POINTS:
                return GLES20.GL_POINTS;
            default:
                throw new IllegalArgumentException("Unsupported primitive: " + primitive);
        }
    }

    private int convert2GLES20DataType(DataType type) {
        switch (type) {
            case FLOAT:
                return GLES20.GL_FLOAT;
            case UNSIGNED_INT:
                return GLES20.GL_UNSIGNED_INT;
            default:
                throw new IllegalArgumentException("Invalid data type: " + type);
        }
    }

    @Override
    public void glUseProgram(int program) {
        GLES20.glUseProgram(program);
    }

    @Override
    public Vector fixTexCoord(Vector texCoord) {
        return new Vector(texCoord.x(), 1.0 - texCoord.y());
    }

    /**
     * Convert to GL shader constants.
     */
    private int getGlShaderType(Shader.ShaderType type) {
        if (type == Shader.ShaderType.VERTEX) {
            return GLES20.GL_VERTEX_SHADER;
        } else if (type == Shader.ShaderType.FRAGMENT) {
            return GLES20.GL_FRAGMENT_SHADER;
        } else {
            return -1;
        }
    }

    @Override
    public void glBindTexture(int textureId) {
        GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, textureId);
    }

    @Override
    public void glDeleteTextures(int textureId) {
        final int[] textureHandle = new int[1];
        textureHandle[0] = textureId;
        GLES20.glDeleteTextures(1, textureHandle, 0);
    }

    @Override
    public Object getOpenGLObject() {
        throw new IllegalArgumentException("Not available for OpenGL ES.");
    }
}
