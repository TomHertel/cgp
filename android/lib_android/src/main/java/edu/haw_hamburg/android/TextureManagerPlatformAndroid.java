package edu.haw_hamburg.android;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.ColorSpace;
import android.opengl.GLES20;
import android.opengl.GLUtils;
import android.util.Log;

import edu.hawhamburg.shared.misc.Constants;
import edu.hawhamburg.shared.rendering.Texture;
import edu.hawhamburg.shared.rendering.TextureManagerPlatform;

/**
 * Functionality to load textures on the android platform
 */

public class TextureManagerPlatformAndroid implements TextureManagerPlatform {

    private Context context = null;

    private String modulePackageName = null;

    public void setup(String modulePackageName, Context context) {
        this.modulePackageName = modulePackageName;
        this.context = context;
    }

    @Override
    public Texture getTexture(String textureName) {
        if (context == null) {
            Log.i(Constants.LOGTAG, "Context must be set first!");
            return null;
        }

        if (modulePackageName == null) {
            Log.i(Constants.LOGTAG, "Module package name must be set first.");
            return null;
        }

        String noExtensionFilename = textureName;
        if (noExtensionFilename.trim().toUpperCase().contains(".JPG") ||
                noExtensionFilename.trim().toUpperCase().contains(".PNG") ||
                noExtensionFilename.trim().toUpperCase().contains(".JPEG")) {
            int index = noExtensionFilename.lastIndexOf('.');
            noExtensionFilename = noExtensionFilename.substring(0, index).trim();
        }

        // Try to create the texture
        int resourceId = context.getResources().getIdentifier(noExtensionFilename, Constants.TEXURE_DIR, modulePackageName);

        if (resourceId == 0) {
            Log.i(Constants.LOGTAG, "Invalid resource id, did not load texture " + textureName);
            return null;
        }

        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inScaled = false;   // No pre-scaling
        final Bitmap bitmap = BitmapFactory.decodeResource(context.getResources(), resourceId, options);
        Log.i(Constants.LOGTAG, "Successfully read texture bitmap from " + textureName + ".");

        Texture texture = new Texture(loadTexture(bitmap));
        return texture;
    }

        /**
         * Load a texture from a resource id, return the texture id:
         *
         * @return Texture id
         */
    private int loadTexture(Bitmap bitmap) {
        final int[] textureHandle = new int[1];
        GLES20.glGenTextures(1, textureHandle, 0);
        GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, textureHandle[0]);
        GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_WRAP_S, GLES20.GL_REPEAT);
        GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_WRAP_T, GLES20.GL_REPEAT);
        GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_MAG_FILTER, GLES20.GL_NEAREST);
        GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_MIN_FILTER, GLES20.GL_NEAREST);
        GLUtils.texImage2D(GLES20.GL_TEXTURE_2D, 0, bitmap, 0);
        Log.i(Constants.LOGTAG, "Successfully created texture bitmap of size " + bitmap.getWidth() + "x" + bitmap.getHeight() + ".");
        return textureHandle[0];
    }

}
