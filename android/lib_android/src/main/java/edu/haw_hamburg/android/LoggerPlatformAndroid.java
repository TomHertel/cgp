package edu.haw_hamburg.android;

import android.util.Log;

import edu.hawhamburg.shared.misc.Constants;
import edu.hawhamburg.shared.misc.PlatformLogger;

/**
 * Created by hilipp Jenke
 */

public class LoggerPlatformAndroid implements PlatformLogger {
    @Override
    public void log(String message) {
        Log.i(Constants.LOGTAG, message);
    }
}
