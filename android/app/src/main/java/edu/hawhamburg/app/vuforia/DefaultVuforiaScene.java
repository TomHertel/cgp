package edu.hawhamburg.app.vuforia;

import edu.hawhamburg.shared.math.Vector;
import edu.hawhamburg.shared.misc.Scene;
import edu.hawhamburg.shared.scenegraph.CubeNode;
import edu.hawhamburg.shared.scenegraph.INode;
import edu.hawhamburg.shared.scenegraph.InnerNode;
import edu.hawhamburg.shared.scenegraph.TranslationNode;
import edu.hawhamburg.vuforia.VuforiaMarkerNode;

/**
 * Dummy implementation of a scene with a Vuforia marker
 *
 * @author Philipp Jenke
 */
public class DefaultVuforiaScene extends Scene {

    public DefaultVuforiaScene() {
        super(100, INode.RenderMode.REGULAR);
    }

    @Override
    public void onSetup(InnerNode rootNode) {
        VuforiaMarkerNode marker = new VuforiaMarkerNode("elphi");
        double sideLength = 0.25;
        CubeNode cubeNode = new CubeNode(sideLength);
        TranslationNode translationNode = new TranslationNode(new Vector(0, sideLength, 0));
        translationNode.addChild(cubeNode);
        marker.addChild(translationNode);
        rootNode.addChild(marker);
    }

    @Override
    public void onTimerTick(int counter) {
        // Timer tick event
    }

    @Override
    public void onSceneRedraw() {

    }
}
