/**
 * Diese Datei gehört zum Android/Java Framework zur Veranstaltung "Computergrafik für
 * Augmented Reality" von Prof. Dr. Philipp Jenke an der Hochschule für Angewandte
 * Wissenschaften (HAW) Hamburg. Weder Teile der Software noch das Framework als Ganzes dürfen
 * ohne die Einwilligung von Philipp Jenke außerhalb von Forschungs- und Lehrprojekten an der HAW
 * Hamburg verwendet werden.
 * <p>
 * This file is part of the Android/Java framework for the course "Computer graphics for augmented
 * reality" by Prof. Dr. Philipp Jenke at the University of Applied (UAS) Sciences Hamburg. Neither
 * parts of the framework nor the complete framework may be used outside of research or student
 * projects at the UAS Hamburg.
 * <p>
 * This software was built upon the Google ARCore sample code:
 * <p>
 * Copyright 2017 Google Inc. All Rights Reserved.
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.hawhamburg.app.arcore;

import android.opengl.GLES20;
import android.opengl.GLSurfaceView;
import android.os.Bundle;
import android.support.design.widget.BaseTransientBottomBar;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.Toast;

import com.google.ar.core.Anchor;
import com.google.ar.core.ArCoreApk;
import com.google.ar.core.Config;
import com.google.ar.core.Frame;
import com.google.ar.core.HitResult;
import com.google.ar.core.Plane;
import com.google.ar.core.Point;
import com.google.ar.core.PointCloud;
import com.google.ar.core.Session;
import com.google.ar.core.Trackable;
import com.google.ar.core.TrackingState;
import com.google.ar.core.exceptions.UnavailableApkTooOldException;
import com.google.ar.core.exceptions.UnavailableArcoreNotInstalledException;
import com.google.ar.core.exceptions.UnavailableSdkTooOldException;
import com.google.ar.core.exceptions.UnavailableUserDeclinedInstallationException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.concurrent.ArrayBlockingQueue;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

import edu.haw_hamburg.android.LoggerPlatformAndroid;
import edu.haw_hamburg.android.AssetPathPlatformAndroid;
import edu.haw_hamburg.android.OpenGLPlatformES;
import edu.haw_hamburg.android.TextureManagerPlatformAndroid;
import edu.hawhamburg.shared.rendering.TextureManager;
import edu.hawhamburg.app.R;
import edu.hawhamburg.arcore.ARCoreAnchorNode;
import edu.hawhamburg.arcore.ARCoreScene;
import edu.hawhamburg.arcore.CameraPermissionHelper;
import edu.hawhamburg.arcore.rendering.BackgroundRenderer;
import edu.hawhamburg.arcore.rendering.DisplayRotationHelper;
import edu.hawhamburg.arcore.rendering.ObjectRenderer;
import edu.hawhamburg.arcore.rendering.ObjectRenderer.BlendMode;
import edu.hawhamburg.arcore.rendering.PlaneRenderer;
import edu.hawhamburg.arcore.rendering.PointCloudRenderer;
import edu.hawhamburg.shared.math.Matrix;
import edu.hawhamburg.shared.misc.AssetPath;
import edu.hawhamburg.shared.misc.Constants;
import edu.hawhamburg.shared.misc.Logger;
import edu.hawhamburg.shared.rendering.OpenGL;
import edu.hawhamburg.shared.scenegraph.Camera;

/**
 * This is a simple example that shows how to create an augmented reality (AR) application using
 * the ARCore API. The application will display any detected planes and will allow the user to
 * tap on a plane to place a 3d model of the Android robot.
 */
public class ARCoreActivity extends AppCompatActivity implements GLSurfaceView.Renderer {

    private static final String PACKAGE_NAME = "edu.hawhamburg.app";

    private static final String TAG = ARCoreActivity.class.getSimpleName();

    // Rendering. The Renderers are created here, and initialized when the GL surface is created.
    private GLSurfaceView mSurfaceView;

    private boolean installRequested;

    //private Config mDefaultConfig;
    private Session session;
    private BackgroundRenderer mBackgroundRenderer = new BackgroundRenderer(R.raw.screenquad_vertex, R.raw.screenquad_fragment_oes);
    private GestureDetector mGestureDetector;
    private Snackbar mLoadingMessageSnackbar = null;
    private DisplayRotationHelper displayRotationHelper;

    private ObjectRenderer mVirtualObject = new ObjectRenderer(R.raw.object_vertex, R.raw.object_fragment);
    private ObjectRenderer mVirtualObjectShadow = new ObjectRenderer(R.raw.object_vertex, R.raw.object_fragment);
    private PlaneRenderer mPlaneRenderer = new PlaneRenderer(R.raw.plane_vertex, R.raw.plane_fragment);
    private final PointCloudRenderer pointCloud = new PointCloudRenderer(R.raw.point_cloud_vertex, R.raw.passthrough_fragment);

    // Temporary matrix allocated here to reduce number of allocations for each frame.
    private final float[] mAnchorMatrix = new float[16];
    private Snackbar messageSnackbar;

    // Tap handling and UI.
    private ArrayBlockingQueue<MotionEvent> mQueuedSingleTaps = new ArrayBlockingQueue<>(16);
    private ArrayList<Anchor> mTouches = new ArrayList<>();

    private boolean showDetectedPoints = false;
    private ARCoreScene scene;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Application specific scene - must be set exactly here
        AssetPathPlatformAndroid assetPathAndroid = new AssetPathPlatformAndroid();
        assetPathAndroid.init(getAssets());
        AssetPath.getInstance().setup(assetPathAndroid);
        Logger.getInstance().setup(new LoggerPlatformAndroid());
        OpenGL.instance().setup(new OpenGLPlatformES());

        scene = new DefaultARCoreScene();

        mSurfaceView = (GLSurfaceView) findViewById(R.id.surfaceview);
        displayRotationHelper = new DisplayRotationHelper(/*context=*/ this);

        //session = new Session(/*context=*/this);

        // Create default config, check is supported, create session from that config.
//        mDefaultConfig = Config.createDefaultConfig();
//        if (!mSession.isSupported(mDefaultConfig)) {
//            Toast.makeText(this, "This device does not support AR", Toast.LENGTH_LONG).show();
//            finish();
//            return;
//        }

        // Set up tap listener.
        mGestureDetector = new GestureDetector(this, new GestureDetector.SimpleOnGestureListener() {
            @Override
            public boolean onSingleTapUp(MotionEvent e) {
                if (!scene.onTouchDown(e.getX(), e.getY())) {
                    // Ignore touch event, which are handled by the scene.
                    onSingleTap(e);
                }
                return true;
            }

            @Override
            public boolean onDown(MotionEvent e) {
                return true;
            }
        });

        mSurfaceView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                return mGestureDetector.onTouchEvent(event);
            }
        });

        // Set up renderer.
        mSurfaceView.setPreserveEGLContextOnPause(true);
        mSurfaceView.setEGLContextClientVersion(2);
        mSurfaceView.setEGLConfigChooser(8, 8, 8, 8, 16, 0); // Alpha used for plane blending.
        mSurfaceView.setRenderer(this);
        mSurfaceView.setRenderMode(GLSurfaceView.RENDERMODE_CONTINUOUSLY);
        installRequested = false;

        TextureManagerPlatformAndroid platform = new TextureManagerPlatformAndroid();
        platform.setup(PACKAGE_NAME, getApplicationContext());
        TextureManager.getInstance().setup(platform);

        scene.onSetup(scene.getRoot());
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (session == null) {
            Exception exception = null;
            String message = null;
            try {
                switch (ArCoreApk.getInstance().requestInstall(this, !installRequested)) {
                    case INSTALL_REQUESTED:
                        installRequested = true;
                        return;
                    case INSTALLED:
                        break;
                }

                // ARCore requires camera permissions to operate. If we did not yet obtain runtime
                // permission on Android M and above, now is a good time to ask the user for it.
                if (!CameraPermissionHelper.hasCameraPermission(this)) {
                    CameraPermissionHelper.requestCameraPermission(this);
                    return;
                }

                session = new Session(/* context= */ this);
            } catch (UnavailableArcoreNotInstalledException
                    | UnavailableUserDeclinedInstallationException e) {
                message = "Please install ARCore";
                exception = e;
                Log.i(Constants.LOGTAG, "Please install ARCore");
            } catch (UnavailableApkTooOldException e) {
                message = "Please update ARCore";
                exception = e;
                Log.i(Constants.LOGTAG, "Please update ARCore");
            } catch (UnavailableSdkTooOldException e) {
                message = "Please update this app";
                exception = e;
                Log.i(Constants.LOGTAG, "Please update this app");
            } catch (Exception e) {
                message = "This device does not support AR";
                exception = e;
                Log.i(Constants.LOGTAG, "This device does not support AR");
            }

            if (message != null) {
                showSnackbarMessage(message, true);
                Log.e(TAG, "Exception creating session", exception);
                return;
            }

            // Create default config and check if supported.
            Config config = new Config(session);
            if (!session.isSupported(config)) {
                showSnackbarMessage("This device does not support AR", true);
                Log.i(Constants.LOGTAG, "This device does not support AR");
            }
            session.configure(config);
        }
        showLoadingMessage();
        // Note that order matters - see the note in onPause(), the reverse applies here.
        session.resume();
        mSurfaceView.onResume();
        displayRotationHelper.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        if (session != null) {
            // Note that the order matters - GLSurfaceView is paused first so that it does not try
            // to query the session. If Session is paused before GLSurfaceView, GLSurfaceView may
            // still call session.update() and get a SessionPausedException.
            displayRotationHelper.onPause();
            mSurfaceView.onPause();
            session.pause();
        }
        // OpenGL context might be lost - delete all textures
        TextureManager.getInstance().deleteAllTextures();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] results) {
        if (!CameraPermissionHelper.hasCameraPermission(this)) {
            Toast.makeText(this,
                    "Camera permission is needed to run this application", Toast.LENGTH_LONG).show();
            finish();
        }
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        if (hasFocus) {
            // Standard Android full-screen functionality.
            getWindow().getDecorView().setSystemUiVisibility(
                    View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                            | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                            | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_FULLSCREEN
                            | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        }
    }

    private void onSingleTap(MotionEvent e) {
        // Queue tap if there is space. Tap is lost if queue is full.
        mQueuedSingleTaps.offer(e);
    }

    @Override
    public void onSurfaceCreated(GL10 gl, EGLConfig config) {
        GLES20.glClearColor(0.1f, 0.1f, 0.1f, 1.0f);

        // Create the texture and pass it to ARCore session to be filled during update().
        mBackgroundRenderer.createOnGlThread(/*context=*/this);
        session.setCameraTextureName(mBackgroundRenderer.getTextureId());

        // Prepare the other rendering objects.
        try {
            mVirtualObject.createOnGlThread(/*context=*/this, "meshes/andy.obj", "textures/andy.png");
            mVirtualObject.setMaterialProperties(0.0f, 3.5f, 1.0f, 6.0f);

            mVirtualObjectShadow.createOnGlThread(/*context=*/this,
                    "meshes/andy_shadow.obj", "textures/andy_shadow.png");
            mVirtualObjectShadow.setBlendMode(BlendMode.Shadow);
            mVirtualObjectShadow.setMaterialProperties(1.0f, 0.0f, 0.0f, 1.0f);
        } catch (IOException e) {
            Log.e(TAG, "Failed to read obj file");
        }
        try {
            mPlaneRenderer.createOnGlThread(/*context=*/this, "textures/trigrid.png");
        } catch (IOException e) {
            Log.e(TAG, "Failed to read plane texture");
        }
        this.pointCloud.createOnGlThread(/*context=*/this);

        scene.init();
    }

    @Override
    public void onSurfaceChanged(GL10 gl, int width, int height) {
        displayRotationHelper.onSurfaceChanged(width, height);
        GLES20.glViewport(0, 0, width, height);
        Camera.getInstance().setScreenSize(width, height);
    }

    @Override
    public void onDrawFrame(GL10 gl) {
        // Clear screen to notify driver it should not load any pixels from previous frame.
        GLES20.glClear(GLES20.GL_COLOR_BUFFER_BIT | GLES20.GL_DEPTH_BUFFER_BIT);

        if (session == null) {
            return;
        }

        displayRotationHelper.updateSessionIfNeeded(session);

        try {
            // Obtain the current frame from ARSession. When the configuration is set to
            // UpdateMode.BLOCKING (it is by default), this will throttle the rendering to the
            // camera framerate.
            Frame frame = session.update();
            com.google.ar.core.Camera camera = frame.getCamera();

            MotionEvent tap = mQueuedSingleTaps.poll();
            if (tap != null && camera.getTrackingState() == TrackingState.TRACKING) {
                for (HitResult hit : frame.hitTest(tap)) {
                    // Check if any plane was hit, and if it was hit inside the plane polygon
                    Trackable trackable = hit.getTrackable();
                    // Creates an anchor if a plane or an oriented point was hit.
                    if ((trackable instanceof Plane && ((Plane) trackable).isPoseInPolygon(hit.getHitPose()))
                            || (trackable instanceof Point
                            && ((Point) trackable).getOrientationMode()
                            == Point.OrientationMode.ESTIMATED_SURFACE_NORMAL)) {
                        // Hits are sorted by depth. Consider only closest hit on a plane or oriented point.
                        // Cap the number of objects created. This avoids overloading both the
                        // rendering system and ARCore.
                        if (mTouches.size() >= 20) {
                            mTouches.get(0).detach();
                            mTouches.remove(0);
                        }
                        // Adding an Anchor tells ARCore that it should track this position in
                        // space. This anchor is created on the Plane to place the 3D model
                        // in the correct position relative both to the world and to the plane.
                        Anchor anchor = hit.createAnchor();
                        mTouches.add(anchor);

                        ARCoreAnchorNode anchorNode = new ARCoreAnchorNode(anchor);
                        scene.getRoot().addChild(anchorNode);
                        scene.anchorCreated(anchorNode);

                        break;
                    }
                }
            }


            // Draw background.
            mBackgroundRenderer.draw(frame);

            // If not tracking, don't draw 3d objects.
            if (camera.getTrackingState() == TrackingState.PAUSED) {
                return;
            }

            // Get projection matrix.
            float[] projmtx = new float[16];
            camera.getProjectionMatrix(projmtx, 0, 0.1f, 100.0f);
            Matrix cgProjectionMatrix = new Matrix(
                    projmtx[0], projmtx[1], projmtx[2], projmtx[3],
                    projmtx[4], projmtx[5], projmtx[6], projmtx[7],
                    projmtx[8], projmtx[9], projmtx[10], projmtx[11],
                    projmtx[12], projmtx[13], projmtx[14], projmtx[15]
            );
            Camera.getInstance().setProjectionMatrix(cgProjectionMatrix);

            // Get camera matrix and draw.
            float[] viewmtx = new float[16];
            camera.getViewMatrix(viewmtx, 0);
            Matrix viewMatrix = new Matrix(
                    viewmtx[0], viewmtx[1], viewmtx[2], viewmtx[3],
                    viewmtx[4], viewmtx[5], viewmtx[6], viewmtx[7],
                    viewmtx[8], viewmtx[9], viewmtx[10], viewmtx[11],
                    viewmtx[12], viewmtx[13], viewmtx[14], viewmtx[15]
            );
            Camera.getInstance().setViewMatrix(viewMatrix);

            // Compute lighting from average intensity of the image.
            final float lightIntensity = frame.getLightEstimate().getPixelIntensity();

            // Visualize tracked points.
            if (showDetectedPoints) {
                PointCloud pointCloud = frame.acquirePointCloud();
                this.pointCloud.update(pointCloud);
                this.pointCloud.draw(viewmtx, projmtx);
                pointCloud.release();
            }

            // Check if we detected at least one plane. If so, hide the loading message.
            if (messageSnackbar != null) {
                for (Plane plane : session.getAllTrackables(Plane.class)) {
                    if (plane.getType() == com.google.ar.core.Plane.Type.HORIZONTAL_UPWARD_FACING
                            && plane.getTrackingState() == TrackingState.TRACKING) {
                        hideLoadingMessage();
                        break;
                    }
                }
            }

            // Visualize planes.
            if (scene.showDetectedPlanes()) {
                mPlaneRenderer.drawPlanes(
                        session.getAllTrackables(Plane.class), camera.getDisplayOrientedPose(), projmtx);
            }


            // Visualize anchors created by touch.
//            float scaleFactor = 1.0f;
//            for (Anchor anchor : mTouches) {
//                if (anchor.getTrackingState() != TrackingState.TRACKING) {
//                    continue;
//                }
//                // Get the current pose of an Anchor in world space. The Anchor pose is updated
//                // during calls to session.update() as ARCore refines its estimate of the world.
//                anchor.getPose().toMatrix(mAnchorMatrix, 0);
//
//                // Update and draw the model and its shadow.
//                mVirtualObject.updateModelMatrix(mAnchorMatrix, scaleFactor);
//                mVirtualObjectShadow.updateModelMatrix(mAnchorMatrix, scaleFactor);
//                mVirtualObject.draw(viewmtx, projmtx, lightIntensity);
//                mVirtualObjectShadow.draw(viewmtx, projmtx, lightIntensity);
//            }

            scene.redraw();

        } catch (Throwable t) {
            // Avoid crashing the application due to unhandled exceptions.
            Log.e(TAG, "Exception on the OpenGL thread", t);
        }
    }

    private void showLoadingMessage() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mLoadingMessageSnackbar = Snackbar.make(
                        ARCoreActivity.this.findViewById(android.R.id.content),
                        "Searching for surfaces...", Snackbar.LENGTH_INDEFINITE);
                mLoadingMessageSnackbar.getView().setBackgroundColor(0xbf323232);
                mLoadingMessageSnackbar.show();
            }
        });
    }

    private void hideLoadingMessage() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mLoadingMessageSnackbar.dismiss();
                mLoadingMessageSnackbar = null;
            }
        });
    }

    private void showSnackbarMessage(String message, boolean finishOnDismiss) {
        messageSnackbar =
                Snackbar.make(
                        ARCoreActivity.this.findViewById(android.R.id.content),
                        message,
                        Snackbar.LENGTH_INDEFINITE);
        messageSnackbar.getView().setBackgroundColor(0xbf323232);
        if (finishOnDismiss) {
            messageSnackbar.setAction(
                    "Dismiss",
                    new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            messageSnackbar.dismiss();
                        }
                    });
            messageSnackbar.addCallback(
                    new BaseTransientBottomBar.BaseCallback<Snackbar>() {
                        @Override
                        public void onDismissed(Snackbar transientBottomBar, int event) {
                            super.onDismissed(transientBottomBar, event);
                            finish();
                        }
                    });
        }
        messageSnackbar.show();
    }
}
