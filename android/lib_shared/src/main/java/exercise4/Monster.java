package exercise4;

import edu.hawhamburg.shared.datastructures.mesh.ITriangleMesh;
import edu.hawhamburg.shared.datastructures.mesh.ObjReader;
import edu.hawhamburg.shared.datastructures.mesh.TriangleMeshFactory;
import edu.hawhamburg.shared.math.Vector;
import edu.hawhamburg.shared.platformer.GameObject;
import edu.hawhamburg.shared.scenegraph.TransformationNode;
import edu.hawhamburg.shared.scenegraph.TriangleMeshNode;

import java.util.ArrayList;
import java.util.List;

public class Monster extends GameObject {
    private TransformationNode playerOrientationNode;
    private List<Vector> hermiteCurveList;
    private HermiteSpline hs;
    private Vector startPos = new Vector(0, 0, 0);
    private double t = 0;
    private Vector originPos;


    private TransformationNode orientationNode = new TransformationNode();
    public Monster(Vector pos) {
        super(Type.MONSTER, pos);
        this.originPos = pos;
        startPos.copy(pos);
        double s = 0.2;
        hermiteCurveList = new ArrayList<>();
        hermiteCurveList.add(new Vector(0, 0, 0).add(startPos));
        hermiteCurveList.add(new Vector(s / 2.0, s / 2.0, 0).add(startPos));
        hermiteCurveList.add(new Vector(0, s, 0).add(startPos));
        hermiteCurveList.add(new Vector(-s / 2.0, s / 2.0, 0).add(startPos));

        hs = new HermiteSpline(hermiteCurveList);

        // Test triangle mesh functionality
        ObjReader reader = new ObjReader();
        List<ITriangleMesh> meshes = reader.read(new TriangleMeshFactory(), "meshes/monster.obj");
        if (meshes.size() != 1) {
            throw new IllegalArgumentException("Illegal monster mesh");
        }
        TriangleMeshNode node = new TriangleMeshNode(meshes.get(0));
        setup(meshes.get(0).getBoundingBox());
        orientationNode.addChild(node);
        addChild(orientationNode);
    }

    @Override
    public void updateGameState() {

        if (t > 1) {
            t = 0;
        }
        HermiteCurve curve = hs.getSegment(hs.getIndexOfSegment(t));
        setPosition(curve.berechneKurve(hs.gibLokalesT(t)));
        t += 0.01;

    }

        public Vector getOriginPos() {

            return originPos;

        }

}

