package exercise4;

import edu.hawhamburg.shared.math.Vector;

public class HermiteCurve{

    private Vector p0;
    private Vector m0;
    private Vector m1;
    private Vector p1;

    public HermiteCurve(Vector p0, Vector m0, Vector m1, Vector p1) {

        this.p0 = p0;
        this.m0 = m0;
        this.m1 = m1;
        this.p1 = p1;

    }

    private double h0(double t) {
        return (1-t)*(1-t)*(1+2*t);
    }

    private double h1(double t) {
        return t*(1-t)*(1-t);
    }

    private double h2(double t) {
        return -(t*t)*(1-t);
    }

    private double h3(double t) {
        return (3-2*t)*t*t;
    }

    private double h0Ableitung(double t) {
        return 6*t*(t-1);
    }

    private double h1Ableitung(double t) {
        return 3*t*t-4*t+1;
    }

    private double h2Ableitung(double t) {
        return 3*t*t-2*t;
    }

    private double h3Ableitung(double t) {
        return 6*t*(1-t);
    }

    public Vector berechneKurve(double t) {

        if((t > 1.0) || (t < 0.0)) {

            return null;

        }

        Vector p0H0 = p0.multiply(h0(t));
        Vector m0H1 = m0.multiply(h1(t));
        Vector m1H2 = m1.multiply(h2(t));
        Vector p1H3 = p1.multiply(h3(t));
        Vector result = p0H0.add(m0H1).add(m1H2).add(p1H3);

        return result;

    }

    public Vector berechneTangente(double t) {

        if((t > 1.0) || (t < 0.0)) {

            return null;

        }

        Vector p0H0 = p0.multiply(h0Ableitung(t));
        Vector m0H1 = m0.multiply(h1Ableitung(t));
        Vector m1H2 = m1.multiply(h2Ableitung(t));
        Vector p1H3 = p1.multiply(h3Ableitung(t));
        Vector result = p0H0.add(m0H1).add(m1H2).add(p1H3);

        return result;

    }

}
