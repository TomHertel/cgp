package exercise4;

import java.util.ArrayList;
import java.util.List;

import edu.hawhamburg.shared.math.Vector;

public class HermiteSpline {

    private ArrayList<HermiteCurve> spline;

    private double deltaT;

    public HermiteSpline(List<Vector> kontrollpunkte){

        spline = new ArrayList<HermiteCurve>();
        deltaT = 1.0/kontrollpunkte.size();
        double s = 0.2;

        int anzahlKontrollpunkte = kontrollpunkte.size();

        for(int i = 0; i < anzahlKontrollpunkte;i++){
            Vector vorgaenger = kontrollpunkte.get(i>0 ? i-1 : anzahlKontrollpunkte-1);
            Vector p0 = kontrollpunkte.get(i);
            Vector p1 = kontrollpunkte.get((i+1)%anzahlKontrollpunkte);
            Vector nachfolger = kontrollpunkte.get((i+2)%anzahlKontrollpunkte);
            Vector m0 = p1.subtract(vorgaenger).getNormalized().multiply(s);
            Vector m1 = nachfolger.subtract(p0).getNormalized().multiply(s);

            spline.add(new HermiteCurve(p0,m0,m1,p1));

        }

    }

    public int getIndexOfSegment(double t){
        return (int) (t/deltaT)%spline.size();

    }

    public double gibLokalesT(double t){
        return (t-getIndexOfSegment(t)*deltaT)/deltaT;

    }

    public HermiteCurve getSegment(int i){
        return spline.get(i);
    }


}
