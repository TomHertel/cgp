package exercise7;

import edu.hawhamburg.shared.platformer.levelgenerator.IRule;

import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Rule implements IRule {

    private static final String regex = "(?<pred>.) --> (?<succ>.*) : (?<dir>.)";
    private static final Pattern regexPattern = Pattern.compile(regex);

    private String pred;
    private List<String> succ;
    private DIR dir;

    private Rule(String pred, List<String> succ, DIR dir) {

        this.pred = pred;
        this.succ = succ;
        this.dir = dir;

    }

    @Override
    public boolean canBeAppliedTo(String symbol) {

        return pred.equals(symbol);

    }

    @Override
    public List<String> getSucc() {

        return succ;

    }

    @Override
    public DIR getDir() {

        return dir;

    }

    public static Rule fromString(String grammarLine) {

        Matcher matcher = regexPattern.matcher(grammarLine);
        Rule rule = null;

        if (matcher.matches()) {

            String pred = matcher.group("pred");
            List<String> succ = Arrays.asList(matcher.group("succ").split(" "));
            DIR dir = (matcher.group("dir").equals("X")) ? DIR.X : DIR.Z;
            rule = new Rule(pred, succ, dir);

        }

        return rule;

    }

}
