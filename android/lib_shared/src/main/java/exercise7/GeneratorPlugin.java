package exercise7;

import edu.hawhamburg.shared.math.Vector;
import edu.hawhamburg.shared.platformer.*;
import edu.hawhamburg.shared.scenegraph.INode;
import exercise4.Monster;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class GeneratorPlugin extends PlatformerPlugin {

    private List<Brick> brickList;
    private World world;
    private List<Vector> coinPos;
    private List<Vector> monsterPos;

    @Override
    public void init() {
        coinPos = new ArrayList<>();
        monsterPos = new ArrayList<>();
        brickList = new ArrayList<>();
        world = game.getWorld();
        int rows = world.getNumberOfRows();
        int col = world.getNumberOfCols();
        int off = world.getMaxHeightOffset();
        for (int r = 0; r < rows; r++) {
            for (int c = 0; c < col; c++) {
                for (int o = 0; o < off; o++) {
                    Brick brick = world.getBrick(r, c, o);
                    if (brick != null) {
                        brickList.add(brick);
                    }
                }
            }
        }
    }

    @Override
    public void updateGameState() {
        if (coinPos.size() < 5) {
            Vector pos = getRandomBrickPos();
            Coin c = new Coin(pos);
            game.addGameObject(c);
            coinPos.add(pos);
        }
        if (monsterPos.size() < 2) {
            Vector pos = getRandomBrickPos();

            Monster m = new Monster(pos);
            game.addGameObject(m);
            monsterPos.add(pos);
        }
    }

    private Vector getRandomBrickPos() {
        Collections.shuffle(brickList);
        Vector pos = world.getBrickTopCenter(brickList.get(0));
        while (coinPos.contains(pos) || monsterPos.contains(pos)) {
            Collections.shuffle(brickList);
            pos = world.getBrickTopCenter(brickList.get(0));
        }
        return pos;

    }

    @Override
    public INode getSceneGraphContent() {
        return null;
    }

    @Override
    public void handleJson(String key, Object value) {

    }

    @Override
    public String getPluginName() {
        return "GeneratorPlugin";
    }

    @Override
    public void handleGameEvent(GameEvent gameEvent) {
        if (gameEvent.getType() == GameEvent.Type.COLLISION_COIN) {
            coinPos.remove(((Coin) gameEvent.getPayload()).getPosition());
        } else if (gameEvent.getType() == GameEvent.Type.HAZELNUT_HIT_MONSTER) {
            monsterPos.remove(((Monster) gameEvent.getPayload()).getOriginPos());
        }
    }
}

