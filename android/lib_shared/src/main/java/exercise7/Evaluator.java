package exercise7;

import edu.hawhamburg.shared.platformer.levelgenerator.Gamefield;
import edu.hawhamburg.shared.platformer.levelgenerator.Grammar;
import edu.hawhamburg.shared.platformer.levelgenerator.IRule;

public class Evaluator {

    private Grammar grammar;

    public Evaluator(Grammar grammar) {
        this.grammar = grammar;
    }

    public Gamefield derive(Gamefield gamefield, int numberOfIterations) {
        for (int num = 0; num < numberOfIterations; num++) {
            for (int i = 0; i < gamefield.getResX(); i++) {
                for (int j = 0; j < gamefield.getResZ(); j++) {
                    String symbol = gamefield.getSymbol(i, j);
                    if (symbol != null && !gamefield.isNew(i, j)) {
                        for (int ruleIndex = 0; ruleIndex < grammar.rules.size(); ruleIndex++) {
                            IRule rule = grammar.rules.get(ruleIndex);
                            if (rule.canBeAppliedTo(symbol) && rule.getDir().equals(IRule.DIR.X)
                                    && rule.getSucc().size() + i <= gamefield.getResX()) {
                                for (int succIndex = 0; succIndex < rule.getSucc().size(); succIndex++) {
                                    gamefield.set(i + succIndex, j, rule.getSucc().get(succIndex));
                                }
                            } else if (rule.canBeAppliedTo(symbol) && rule.getDir().equals(IRule.DIR.Z)
                                    && rule.getSucc().size() + j <= gamefield.getResZ()) {
                                for (int succIndex = 0; succIndex < rule.getSucc().size(); succIndex++) {
                                    gamefield.set(i, j + succIndex, rule.getSucc().get(succIndex));
                                }
                            }
                        }
                    }
                }
            }
            gamefield.resetIsNew();
        }
        return gamefield;
    }
}
