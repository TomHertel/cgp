package exercise5;

import edu.hawhamburg.shared.math.AxisAlignedBoundingBox;
import edu.hawhamburg.shared.math.Intervall;
import edu.hawhamburg.shared.math.Matrix;
import edu.hawhamburg.shared.math.Vector;
import edu.hawhamburg.shared.misc.Logger;
import edu.hawhamburg.shared.platformer.*;
import edu.hawhamburg.shared.scenegraph.INode;

public class CollisionPlugin extends PlatformerPlugin {

    @Override
    public void init() {

    }

    @Override
    public void updateGameState() {

        GameEventQueue gameEventQueue = GameEventQueue.getInstance();

        Player player = game.getPlayer();
        Matrix playerMatrix = game.getPlayer().getCombinedTransformation();
        AxisAlignedBoundingBox huellkoerperPlayer = player.getBoundingBox().clone();
        huellkoerperPlayer.transform(playerMatrix);

        Hazelnut hazelnut = game.getHazelnut();

        Vector ur1 = huellkoerperPlayer.getUR();
        Vector ll1 = huellkoerperPlayer.getLL();

        boolean collision;
        Vector ur2;
        Vector ll2;

        for (int i = 0; i < game.getNumberOfGameObjects(); i++) {

            if (game.getGameObject(i).getType()!= GameObject.Type.RIGID_BODY_BRICK) {

                GameObject gameObject = game.getGameObject(i);
                Matrix matrix = game.getGameObject(i).getCombinedTransformation();

                AxisAlignedBoundingBox huellkoerper = gameObject.getBoundingBox().clone();
                huellkoerper.transform(matrix);

                ur2 = huellkoerper.getUR();
                ll2 = huellkoerper.getLL();

                collision = (ur2.x() > ll1.x() && ur1.x() > ll2.x());
                collision = collision &&(ur2.y() > ll1.y() && ur1.y() > ll2.y());
                collision = collision && (ur2.z() > ll1.z() && ur1.z() > ll2.z());

                if (gameObject.getType() == GameObject.Type.COIN && collision) {

                    gameEventQueue.emitEvent(new GameEvent(GameEvent.Type.COLLISION_COIN, gameObject));

                }

                if (gameObject.getType() == GameObject.Type.MONSTER) {

                    if (collision) {

                        gameEventQueue.emitEvent(new GameEvent(GameEvent.Type.COLLISION_MONSTER, gameObject));

                    }

                    if (hazelnut != null) {

                        Vector direction = new Vector(hazelnut.getDirection());
                        Vector position  = new Vector(hazelnut.getPosition());

                        Intervall tx = new Intervall();

                        if (direction.x() > 0) {

                            tx.set((ur2.x() - position.x()) / direction.x(), (ll2.x() - position.x()) / direction.x());

                        }

                        if ((tx.getT0() > 0 && tx.getT1() < 0) && (position.y() >= ll2.y() && position.y() <= ur2.y())) {

                            gameEventQueue.emitEvent(new GameEvent(GameEvent.Type.HAZELNUT_HIT_MONSTER, gameObject));

                        }

                    }

                }

            }

        }

    }

    @Override
    public INode getSceneGraphContent() {
        return null;
    }

    @Override
    public void handleJson(String key, Object value) {

    }

    @Override
    public String getPluginName() {
        return "CollisionPlugin";
    }

    @Override
    public void handleGameEvent(GameEvent gameEvent) {

        if (gameEvent.getType() == GameEvent.Type.COLLISION_COIN) {

            game.removeGameObject((GameObject) gameEvent.getPayload());

        } else if (gameEvent.getType() == GameEvent.Type.COLLISION_MONSTER) {

            Logger.getInstance().log("COLLISION_MONSTER");
        } else if (gameEvent.getType() == GameEvent.Type.HAZELNUT_HIT_MONSTER) {

            Logger.getInstance().log("HAZELNUT_HIT_MONSTER");
            game.removeGameObject((GameObject) gameEvent.getPayload());

        }

    }

}
