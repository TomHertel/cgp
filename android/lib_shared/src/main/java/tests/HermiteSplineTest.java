package tests;
import exercise4.HermiteSpline;
import edu.hawhamburg.shared.math.Vector;
import org.junit.Test;

import java.util.ArrayList;

import static junit.framework.TestCase.*;

public class HermiteSplineTest {

    private HermiteSpline hermiteSpline;

    public HermiteSplineTest(){

        Vector p0 = new Vector(0,0,0);
        Vector p1 = new Vector(2,2,0);
        Vector p2 = new Vector(0,1,0);
        Vector p3 = new Vector(-2,2,0);

        ArrayList<Vector> kontrollpunkte = new ArrayList<Vector>();

        kontrollpunkte.add(p0);
        kontrollpunkte.add(p1);
        kontrollpunkte.add(p2);
        kontrollpunkte.add(p3);

        hermiteSpline = new HermiteSpline(kontrollpunkte);

    }

    @Test
    //Testfall 1:
    public void testeGibSegment(){

        int index1 = hermiteSpline.getIndexOfSegment(0);
        int index2 = hermiteSpline.getIndexOfSegment(1);

        assertEquals(index1,index2);

    }

}
