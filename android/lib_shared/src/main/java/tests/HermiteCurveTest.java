package tests;

import exercise4.HermiteCurve;
import edu.hawhamburg.shared.math.Vector;
import org.junit.Test;

import static junit.framework.TestCase.*;

public class HermiteCurveTest {

    private Vector p0 = new Vector(0, 0, 0);
    private Vector m0 = new Vector(1, 1, 1);
    private Vector m1 = new Vector(1, 2, 1);
    private Vector p1 = new Vector(2, 2, 1);

    private HermiteCurve hermite = new HermiteCurve(p0,m0,m1,p1);

    @Test
    //Testfall 1: Kurvenberechnung, Testung der Ober- und Untergrenze.
    public void testeBerechneKurve()
    {
        double t = -1.0;

        assertEquals(null, hermite.berechneKurve(t));

        t = 1.1;

        assertEquals(null, hermite.berechneKurve(t));

        t = 0.0;

        assertEquals(hermite.berechneKurve(t), p0);

        t = 1.0;

        assertEquals(hermite.berechneKurve(t), p1);

        for(double i = 0.0; i <= 1.0;i += 0.1) {

            assertNotNull(hermite.berechneKurve(i));

        }

    }

    @Test
    //Testfall 2: Tangentenberechnung, Testung der Ober- und Untergrenze.
    public void testeBerechneTangente() {

        double t = -1.0;

        assertEquals(null, hermite.berechneTangente(t));

        t = 1.1;
        assertEquals(null, hermite.berechneTangente(t));

        t = 0.0;

        assertEquals(hermite.berechneTangente(t), m0);

        t = 1.0;

        assertEquals(hermite.berechneTangente(t), m1);

        for(double i = 0.0; i <= 1.0;i += 0.1) {

            assertNotNull(hermite.berechneTangente(i));

        }

    }

}
