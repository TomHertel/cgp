/**
 * Diese Datei gehört zum Android/Java Framework zur Veranstaltung "Computergrafik für
 * Augmented Reality" von Prof. Dr. Philipp Jenke an der Hochschule für Angewandte
 * Wissenschaften (HAW) Hamburg. Weder Teile der Software noch das Framework als Ganzes dürfen
 * ohne die Einwilligung von Philipp Jenke außerhalb von Forschungs- und Lehrprojekten an der HAW
 * Hamburg verwendet werden.
 * <p>
 * This file is part of the Android/Java framework for the course "Computer graphics for augmented
 * reality" by Prof. Dr. Philipp Jenke at the University of Applied (UAS) Sciences Hamburg. Neither
 * parts of the framework nor the complete framework may be used outside of research or student
 * projects at the UAS Hamburg.
 */
package edu.hawhamburg.shared.misc;

import java.io.IOException;
import java.io.InputStream;
import java.util.Scanner;

/**
 * Computes the relative path to an asset - uses a platform specific implementation.
 *
 * @author Philipp Jenke
 */
public class AssetPath {

    private static AssetPath instance;

    private AssetPathPlatform platform;

    private AssetPath() {
    }

    public static AssetPath getInstance() {
        if (instance == null) {
            instance = new AssetPath();
        }
        return instance;
    }

    public void setup(AssetPathPlatform platform) {
        this.platform = platform;
    }

    public String readTextFileToString(String relativeFilename) {
        return platform.readTextFileToString(relativeFilename);
    }

    public InputStream readTextFileToStream(String relativeFilename) {
        return platform.readTextFileToStream(relativeFilename);
    }

    public String getPathToAsset(String filename) {
        return platform.getPathToAsset(filename);
    }
}