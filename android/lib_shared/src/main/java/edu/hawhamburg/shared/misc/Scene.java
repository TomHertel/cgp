/**
 * Diese Datei gehört zum Android/Java Framework zur Veranstaltung "Computergrafik für
 * Augmented Reality" von Prof. Dr. Philipp Jenke an der Hochschule für Angewandte
 * Wissenschaften (HAW) Hamburg. Weder Teile der Software noch das Framework als Ganzes dürfen
 * ohne die Einwilligung von Philipp Jenke außerhalb von Forschungs- und Lehrprojekten an der HAW
 * Hamburg verwendet werden.
 * <p>
 * This file is part of the Android/Java framework for the course "Computer graphics for augmented
 * reality" by Prof. Dr. Philipp Jenke at the University of Applied (UAS) Sciences Hamburg. Neither
 * parts of the framework nor the complete framework may be used outside of research or student
 * projects at the UAS Hamburg.
 */
package edu.hawhamburg.shared.misc;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import edu.hawhamburg.shared.math.Matrix;
import edu.hawhamburg.shared.math.Vector;
import edu.hawhamburg.shared.rendering.*;
import edu.hawhamburg.shared.scenegraph.Camera;
import edu.hawhamburg.shared.scenegraph.InnerNode;
import edu.hawhamburg.shared.scenegraph.RootNode;
import edu.hawhamburg.shared.scenegraph.INode.RenderMode;

/**
 * Central frame for all applications - derive from this class.
 *
 * @author Philipp Jenke
 */
public abstract class Scene {
    /**
     * Timer object to create a game loop.
     */
    private Timer timer = new Timer();

    /**
     * Root node of the scene graph
     */
    private RootNode root;

    /**
     * Time.
     */
    private int timerCounter = 0;

    /**
     * Current render mode.
     */
    private RenderMode currentRenderMode;

    /**
     * Indicates a timer tick.
     */
    private boolean timerUpdate = false;

    /**
     * Set this flag if the projection matrix needs to be updated.
     */
    boolean needsUpdateProjectionMatrix = true;

    /**
     * Mapping between resource id and texture id;
     */
    Map<Integer, Integer> textureIds = new HashMap<Integer, Integer>();

    /**
     * Set this flag if the view matrix needs to be updated.
     */
    boolean needsUpdateViewMatrix = true;

    /*
     * List of buttons in the scene (only Android platform)
     */
    private List<IButton> buttons = new ArrayList();

    /**
     * Camera interactionController.
     */
    private InteractionController interactionController = new ObserverInteractionController();

    public Scene() {
        this(100, RenderMode.REGULAR);
    }

    /**
     * Constructor
     */
    public Scene(int timerTimeout, RenderMode renderMode, String vertexShaderFilename, String fragmentShaderFilename) {
        Shader shader = new Shader(vertexShaderFilename,
                fragmentShaderFilename);
        currentRenderMode = renderMode;
        root = new RootNode(shader);
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                timerUpdate = true;
                timerCounter++;
            }
        }, timerTimeout, timerTimeout);
    }


    /**
     * Constructor
     */
    public Scene(int timerTimeout, RenderMode renderMode) {
        this(timerTimeout, renderMode, "shader/vertex_shader.glsl",
                "shader/fragment_shader.glsl");
    }

    // +++ THESE METHODS MUST BE IMPLEMENTED IN ALL CLASSE DERIVING FROM SCENE +++

    /**
     * Override this method with all the scene onSetup, e.g. setting up the scene graph
     */
    public abstract void onSetup(InnerNode rootNode);

    /**
     * This method is called at each timer tick.
     */
    public abstract void onTimerTick(int counter);

    /**
     * This method is called once for each scene redraw of OpenGL ES.
     */
    public abstract void onSceneRedraw();

    /**
     * Key pressed event
     */
    public void onKeyPressed(int keyCode) {
        switch (keyCode) {
            case 70: // 'f'
                interactionController.fitToBoundingBox(getRoot().getBoundingBox());
        }
    }

    /**
     * Key pressed event
     */
    public void onKeyReleased(int keyCode) {
    }

    // +++ END: THESE METHODS MUST BE IMPLEMENTED IN ALL CLASSE DERIVING FROM SCENE +++

    /**
     * Add a button to the scene.
     */
    public void addButton(IButton button) {
        buttons.add(button);
    }

    /**
     * This method is called once when the OpenGL context is created.
     */
    public void init() {
        Logger.getInstance().log(OpenGL.instance().platform().getVersionString());

        // Stencil test
        OpenGL.instance().platform().enableStencilTest(false);

        // Culling
        OpenGL.instance().platform().setupCulling();

        // Depth Test
        OpenGL.instance().platform().enableDepthTest(true);
        OpenGL.instance().platform().enableCulling(true);
        OpenGL.instance().platform().enableBlending(true);
        OpenGL.instance().platform().enableStencilTest(false);
        needsUpdateProjectionMatrix = true;
        root.getShader().compileAndLink();
        root.getShader().use();
        ShaderAttributes.getInstance().getAttributes(root.getShader().getProgram());
    }

    /**
     * Scene needs to be redrawn.
     */
    public void redraw() {
        if (timerUpdate && root.isAnimated()) {
            root.timerTick(timerCounter);
            onTimerTick(timerCounter);
            timerUpdate = false;
        }

        // Inform inherited scene.
        onSceneRedraw();

        drawRegular();

        // Draw buttons
        if (buttons.size() > 0) {
            OpenGL.instance().platform().enableDepthTest(false);
            // GLES20.glEnable(GLES20.GL_BLEND);
            OpenGL.instance().platform().setupBlending();
            Camera.getInstance().setButtonCameraTransformation();
            ShaderAttributes.getInstance().setShaderModeParameter(Shader.ShaderMode.TEXTURE);
            for (IButton button : buttons) {
                renderButton(button);
            }
            OpenGL.instance().platform().enableDepthTest(true);
        }
    }

    private void renderButton(IButton button) {
        button.draw();
    }

    /**
     * Render scene regularly
     */
    private void drawRegular() {
        root.getShader().use();
        ShaderAttributes.getInstance().setCameraEyeParameter(Camera.getInstance().getEye());
        ShaderAttributes.getInstance().setLightPositionParameter(root.getLightPosition());

        // No change in stencil buffer
        //GLES20.glStencilOp(GLES20.GL_KEEP, GLES20.GL_KEEP, GLES20.GL_KEEP);
        // Draw always
        //GLES20.glStencilFunc(GLES20.GL_ALWAYS, 0, 255);

        getRoot().traverse(RenderMode.REGULAR, Matrix.createIdentityMatrix4());
    }

    /**
     * Return the root node of the scene graph.
     */
    public RootNode getRoot() {
        return root;
    }

    public void resize(int width, int height) {
        for (IButton botton : buttons) {
            botton.invalidate();
        }
    }

    /**
     * A touch event occurred. Returns true, if the touch was handled.
     */
    public boolean onTouchDown(float x, float y) {
        // Convert to button coordinate system
        double buttonX = x / Camera.getInstance().getWidth() * 2 - 1;
        double buttonY = -y / Camera.getInstance().getHeight() * 2 + 1;
        for (IButton button : buttons) {
            if (button.wasTouched(buttonX, buttonY)) {
                button.handleTouch();
                return true;
            }
        }
        return false;
    }
}
