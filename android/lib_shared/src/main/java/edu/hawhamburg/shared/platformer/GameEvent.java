/**
 * Diese Datei gehört zum Android/Java Framework zur Veranstaltung "Computergrafik für
 * Augmented Reality" von Prof. Dr. Philipp Jenke an der Hochschule für Angewandte
 * Wissenschaften (HAW) Hamburg. Weder Teile der Software noch das Framework als Ganzes dürfen
 * ohne die Einwilligung von Philipp Jenke außerhalb von Forschungs- und Lehrprojekten an der HAW
 * Hamburg verwendet werden.
 * <p>
 * This file is part of the Android/Java framework for the course "Computer graphics for augmented
 * reality" by Prof. Dr. Philipp Jenke at the University of Applied (UAS) Sciences Hamburg. Neither
 * parts of the framework nor the complete framework may be used outside of research or student
 * projects at the UAS Hamburg.
 */

package edu.hawhamburg.shared.platformer;

/**
 * Events in the game are encapsulated by instances of this class
 */
public class GameEvent {
    /**
     * Each event has a unique type.
     */
    public enum Type {
        SWITCH, COLLISION_COIN, COLLISION_MONSTER, HAZELNUT_HIT_MONSTER, BREAKING_BRICK, REGENERATE_WORLD,
        PLAYER_STATE_CHANGED
    }

    /**
     * Event type.
     */
    private final Type type;

    /**
     * The payload may contain additional information about the event.
     */
    private Object payload;

    public GameEvent(Type type) {
        this(type, null);
    }

    public GameEvent(Type type, Object payload) {
        this.type = type;
        this.payload = payload;
    }

    public Object getPayload() {
        return payload;
    }

    public Type getType() {
        return type;
    }
}
