/**
 * Diese Datei gehört zum Android/Java Framework zur Veranstaltung "Computergrafik für
 * Augmented Reality" von Prof. Dr. Philipp Jenke an der Hochschule für Angewandte
 * Wissenschaften (HAW) Hamburg. Weder Teile der Software noch das Framework als Ganzes dürfen
 * ohne die Einwilligung von Philipp Jenke außerhalb von Forschungs- und Lehrprojekten an der HAW
 * Hamburg verwendet werden.
 * <p>
 * This file is part of the Android/Java framework for the course "Computer graphics for augmented
 * reality" by Prof. Dr. Philipp Jenke at the University of Applied (UAS) Sciences Hamburg. Neither
 * parts of the framework nor the complete framework may be used outside of research or student
 * projects at the UAS Hamburg.
 */

package edu.hawhamburg.shared.platformer;

import edu.hawhamburg.shared.math.AxisAlignedBoundingBox;
import edu.hawhamburg.shared.math.Vector;
import edu.hawhamburg.shared.scenegraph.TranslationNode;

/**
 * Base class for all game object (e.g. player, monster, ...) in the game. The game object can be directly put
 * in the scene graph.
 */
public abstract class GameObject extends TranslationNode {
    /**
     * Current position of the player. The position is on the ground (below the players feet).
     */
    protected Vector pos = new Vector(0, 0, 0);

    /**
     * Available types of game objects.
     */
    public enum Type {
        PLAYER, MONSTER, COIN, HASELNUT, RIGID_BODY_BRICK
    }

    /**
     * Type of the game object.
     */
    private final Type type;

    /**
     * Bounding box of the coin mesh.
     */
    protected AxisAlignedBoundingBox bbox;

    public GameObject(Type type, Vector pos) {
        super(Vector.ZERO_3);
        this.type = type;
        this.pos.copy(pos);
        setTranslation(pos);
    }

    /**
     * Call this method to setup the bounding box of the mesh.
     */
    protected void setup(AxisAlignedBoundingBox bbox) {
        this.bbox = bbox;
    }

    public Vector getPosition() {
        return pos;
    }

    public void setPosition(Vector newPos) {
        pos.copy(newPos);
        setTranslation(pos);
    }

    public void updatePosition(Vector offset) {
        pos.addSelf(offset);
        setTranslation(pos);
    }

    public Type getType() {
        return type;
    }

    /**
     * Game state update callback
     */
    public abstract void updateGameState();

    @Override
    public AxisAlignedBoundingBox getBoundingBox() {
        return bbox;
    }
}
