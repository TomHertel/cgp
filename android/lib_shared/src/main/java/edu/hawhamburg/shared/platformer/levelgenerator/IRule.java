/**
 * Diese Datei gehört zum Android/Java Framework zur Veranstaltung "Computergrafik für
 * Augmented Reality" von Prof. Dr. Philipp Jenke an der Hochschule für Angewandte
 * Wissenschaften (HAW) Hamburg. Weder Teile der Software noch das Framework als Ganzes dürfen
 * ohne die Einwilligung von Philipp Jenke außerhalb von Forschungs- und Lehrprojekten an der HAW
 * Hamburg verwendet werden.
 * <p>
 * This file is part of the Android/Java framework for the course "Computer graphics for augmented
 * reality" by Prof. Dr. Philipp Jenke at the University of Applied (UAS) Sciences Hamburg. Neither
 * parts of the framework nor the complete framework may be used outside of research or student
 * projects at the UAS Hamburg.
 */

package edu.hawhamburg.shared.platformer.levelgenerator;

import java.util.List;

/**
 * Shared interface for all rules of the form pred --> succ : dir
 */
public interface IRule {

    /**
     * Expand directions for rule successors.
     */
    enum DIR {
        X, Z
    }

    /**
     * Returns true if the rule can be applied to the symbol (symbol equals rule predecessor).
     */
    boolean canBeAppliedTo(String symbol);

    /**
     * Returns the List of sucessor symbols
     */
    List<String> getSucc();

    /**
     * Returns the expand direction of the successors.
     */
    DIR getDir();
}
