package edu.hawhamburg.shared.misc;

/**
 * Implementations of this interface provide platform-specific logging functionality.
 */
public interface PlatformLogger {
    /**
     * Log a message
     */
    public void log(String message);
}
