/**
 * Diese Datei gehört zum Android/Java Framework zur Veranstaltung "Computergrafik für
 * Augmented Reality" von Prof. Dr. Philipp Jenke an der Hochschule für Angewandte
 * Wissenschaften (HAW) Hamburg. Weder Teile der Software noch das Framework als Ganzes dürfen
 * ohne die Einwilligung von Philipp Jenke außerhalb von Forschungs- und Lehrprojekten an der HAW
 * Hamburg verwendet werden.
 * <p>
 * This file is part of the Android/Java framework for the course "Computer graphics for augmented
 * reality" by Prof. Dr. Philipp Jenke at the University of Applied (UAS) Sciences Hamburg. Neither
 * parts of the framework nor the complete framework may be used outside of research or student
 * projects at the UAS Hamburg.
 */

package edu.hawhamburg.shared.platformer.levelgenerator;

/**
 * The Gamefield represents a word in the grammar and contains a grid of symbols.
 */
public class Gamefield {

    /**
     * Data structure for the grid cells
     */
    private class Cell {
        public String symbol = null;
        public boolean isNew = false;
    }


    /**
     * z x x cells.
     */
    public Cell[][] cells;

    public Gamefield(int resX, int resZ) {
        cells = new Cell[resZ][resX];
        for (int x = 0; x < cells.length; x++) {
            for (int z = 0; z < cells[0].length; z++) {
                cells[z][x] = new Cell();
            }
        }
        set(0, 0, "B");
        set(1, 0, "→");
        set(2, 0, "E");
        resetIsNew();
    }

    @Override
    public String toString() {
        String result = "\n+";
        for (int x = 0; x < cells.length; x++) {
            result += "-";
        }
        result += "> z\n|\n";
        for (int x = 0; x < cells.length; x++) {
            result += "|   ";
            for (int z = 0; z < cells[0].length; z++) {
                result += getSymbol(cells[z][x].symbol);
            }
            result += "\n";
        }
        result += "v\nx\n";
        return result;
    }

    /**
     * Returns the symbol used to print the game field cell.
     */
    private String getSymbol(String s) {
        return (s == null) ? " " : s;
    }

    /**
     * Sets the cell value at (x,z)
     */
    public void set(int x, int z, String symbol) {
        cells[z][x].symbol = symbol;
        cells[z][x].isNew = true;
    }

    /**
     * Return the resolution of the field in z-direction.
     */
    public int getResZ() {
        return cells.length;
    }

    /**
     * Return the resolution of the field in x-direction.
     */
    public int getResX() {
        return cells[0].length;
    }

    public String getSymbol(int x, int z) {
        return cells[z][x].symbol;
    }

    /**
     * Check if this cell was newly created in the current derivation.
     */
    public boolean isNew(int x, int z) {
        return cells[z][x].isNew;
    }

    /**
     * Reset all isNew flags.
     */
    public void resetIsNew() {
        for (int x = 0; x < cells.length; x++) {
            for (int z = 0; z < cells[0].length; z++) {
                cells[x][z].isNew = false;
            }
        }
    }
}
