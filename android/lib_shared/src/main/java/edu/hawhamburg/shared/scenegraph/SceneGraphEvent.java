package edu.hawhamburg.shared.scenegraph;

/**
 * This class encapsulates events which are passed thru the scene graph (e.g. keyboard or touch/mouse event).
 */
public class SceneGraphEvent {

    public enum EventType {
        KEY
    }

    private char key;

    private EventType type;

    public SceneGraphEvent(char keyChar) {
        this.key = keyChar;
        this.type = EventType.KEY;
    }

    public boolean isKeyEvent(){
        return type == EventType.KEY;
    }

    public char getKey(){
        return key;
    }
}
