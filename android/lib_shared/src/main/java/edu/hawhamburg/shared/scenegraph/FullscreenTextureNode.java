package edu.hawhamburg.shared.scenegraph;

import edu.hawhamburg.shared.datastructures.mesh.ITriangleMesh;
import edu.hawhamburg.shared.datastructures.mesh.Triangle;
import edu.hawhamburg.shared.datastructures.mesh.TriangleMesh;
import edu.hawhamburg.shared.math.AxisAlignedBoundingBox;
import edu.hawhamburg.shared.math.Matrix;
import edu.hawhamburg.shared.math.Vector;
import edu.hawhamburg.shared.rendering.ShaderAttributes;

/**
 * Renders a texture as a fullscreen image.
 */
public class FullscreenTextureNode extends LeafNode {

    /**
     * The texture is mapped to a simple 2-triangle-mesh, which is represented in this node.
     */
    private TriangleMeshNode meshNode;

    public FullscreenTextureNode(String filename) {
        double z = 0.99;
        ITriangleMesh fullscreenBackgroundMesh = new TriangleMesh();
        fullscreenBackgroundMesh.addVertex(new Vector(-1, -1, z));
        fullscreenBackgroundMesh.addVertex(new Vector(1, -1, z));
        fullscreenBackgroundMesh.addVertex(new Vector(1, 1, z));
        fullscreenBackgroundMesh.addVertex(new Vector(-1, 1, z));
        fullscreenBackgroundMesh.addTextureCoordinate(new Vector(0, 0));
        fullscreenBackgroundMesh.addTextureCoordinate(new Vector(1, 0));
        fullscreenBackgroundMesh.addTextureCoordinate(new Vector(1, 1));
        fullscreenBackgroundMesh.addTextureCoordinate(new Vector(0, 1));
        fullscreenBackgroundMesh.addTriangle(new Triangle(0, 1, 2, 0, 1, 2));
        fullscreenBackgroundMesh.addTriangle(new Triangle(0, 2, 3, 0, 2, 3));
        fullscreenBackgroundMesh.computeTriangleNormals();
        fullscreenBackgroundMesh.setTextureName(filename);
        meshNode = new TriangleMeshNode(fullscreenBackgroundMesh);
        meshNode.setTextureLighting(TriangleMeshNode.TextureLighting.NO_LIGHTING);
    }

    @Override
    public void traverse(RenderMode mode, Matrix modelMatrix) {
        if (!isActive()) {
            return;
        }
        Matrix identity = Matrix.createIdentityMatrix4();
        // Model matrix
        ShaderAttributes.getInstance().setModelMatrixParameter(identity);
        // Projection matrix
        ShaderAttributes.getInstance().setProjectionMatrixParameter(
                identity);
        // View matrix
        ShaderAttributes.getInstance().setViewMatrixParameter(identity);
        drawGL(mode, identity);
    }

    @Override
    public void drawGL(RenderMode mode, Matrix modelMatrix) {
        meshNode.drawGL(RenderMode.REGULAR, null);
    }

    @Override
    public AxisAlignedBoundingBox getBoundingBox() {
        return null;
    }
}
