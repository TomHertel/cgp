package edu.hawhamburg.shared.misc;

import java.io.InputStream;

/**
 * Created by Philipp Jenke
 *
 * Platform-specific asset path manager
 */

public interface AssetPathPlatform {
    void init(Object manager);

    String readTextFileToString(String relativeFilename);

    InputStream readTextFileToStream(String relativeFilename);

    String getPathToAsset(String filename);
}
