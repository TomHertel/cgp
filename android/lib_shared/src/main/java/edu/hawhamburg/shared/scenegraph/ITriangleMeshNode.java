package edu.hawhamburg.shared.scenegraph;

import edu.hawhamburg.shared.datastructures.mesh.ITriangleMesh;

/**
 * Shared interface for scene graph nodes which render ITriangleMesh objects.
 */
public abstract class ITriangleMeshNode extends LeafNode {
    /**
     * Set the triangle mesh object.
     */
    public abstract void setup(ITriangleMesh mesh);

    /**
     * Recreate the render structrures.
     */
    public abstract void updateVbo();

    /**
     * Setter for the transparency of the mesh.
     */
    public abstract void setTransparency(double v);
}
