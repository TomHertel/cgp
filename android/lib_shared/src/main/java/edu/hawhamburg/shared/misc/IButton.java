package edu.hawhamburg.shared.misc;

/**
 * Shared interface for all button types.
 *
 * @author Philipp Jenke
 */
public interface IButton {

    /**
     * Draw the button.
     */
    void draw();

    /**
     * Enforce recreation of the button render structures.
     */
    void invalidate();

    /**
     * This callback is called by the UI to inform the button that it was touched at the give coordinates.
     */
    boolean wasTouched(double buttonX, double buttonY);

    /**
     * Touch-Handler.
     */
    void handleTouch();
}
