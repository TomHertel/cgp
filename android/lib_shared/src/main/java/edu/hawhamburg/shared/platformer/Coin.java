/**
 * Diese Datei gehört zum Android/Java Framework zur Veranstaltung "Computergrafik für
 * Augmented Reality" von Prof. Dr. Philipp Jenke an der Hochschule für Angewandte
 * Wissenschaften (HAW) Hamburg. Weder Teile der Software noch das Framework als Ganzes dürfen
 * ohne die Einwilligung von Philipp Jenke außerhalb von Forschungs- und Lehrprojekten an der HAW
 * Hamburg verwendet werden.
 * <p>
 * This file is part of the Android/Java framework for the course "Computer graphics for augmented
 * reality" by Prof. Dr. Philipp Jenke at the University of Applied (UAS) Sciences Hamburg. Neither
 * parts of the framework nor the complete framework may be used outside of research or student
 * projects at the UAS Hamburg.
 */

package edu.hawhamburg.shared.platformer;

import edu.hawhamburg.shared.datastructures.mesh.ITriangleMesh;
import edu.hawhamburg.shared.datastructures.mesh.ObjReader;
import edu.hawhamburg.shared.datastructures.mesh.TriangleMeshFactory;
import edu.hawhamburg.shared.math.Matrix;
import edu.hawhamburg.shared.math.Vector;
import edu.hawhamburg.shared.scenegraph.TransformationNode;
import edu.hawhamburg.shared.scenegraph.TriangleMeshNode;

import java.util.List;

/**
 * Coins are places in the world an can be collected.
 */
public class Coin extends GameObject {
    private static final String JSON_BRICK = "brick";

    /**
     * Coin animation rotation.
     */
    private TransformationNode orientationNode = new TransformationNode();

    /**
     * Coin animation angle.
     */
    private double alpha = 0;

    /**
     * Coin animation.
     */
    private final Matrix R = Matrix.createRotationMatrix4(Vector.VECTOR_3_Y, 0.001);


    public Coin(Vector pos) {
        super(Type.COIN, pos);

        // Test triangle mesh functionality
        ObjReader reader = new ObjReader();
        List<ITriangleMesh> meshes = reader.read(new TriangleMeshFactory(), "meshes/coin.obj");
        if (meshes.size() != 1) {
            throw new IllegalArgumentException("Illegal coin mesh");
        }
        TriangleMeshNode node = new TriangleMeshNode(meshes.get(0));
        setup(meshes.get(0).getBoundingBox());
        orientationNode.addChild(node);
        addChild(orientationNode);
    }

    public void updateGameState() {
        alpha += 0.05;
        orientationNode.setTransformation(Matrix.createRotationMatrix4(Vector.VECTOR_3_Y, alpha));
    }
}
