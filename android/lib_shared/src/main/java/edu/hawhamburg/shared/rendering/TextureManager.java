/**
 * Diese Datei gehört zum Android/Java Framework zur Veranstaltung "Computergrafik für
 * Augmented Reality" von Prof. Dr. Philipp Jenke an der Hochschule für Angewandte
 * Wissenschaften (HAW) Hamburg. Weder Teile der Software noch das Framework als Ganzes dürfen
 * ohne die Einwilligung von Philipp Jenke außerhalb von Forschungs- und Lehrprojekten an der HAW
 * Hamburg verwendet werden.
 * <p>
 * This file is part of the Android/Java framework for the course "Computer graphics for augmented
 * reality" by Prof. Dr. Philipp Jenke at the University of Applied (UAS) Sciences Hamburg. Neither
 * parts of the framework nor the complete framework may be used outside of research or student
 * projects at the UAS Hamburg.
 */
package edu.hawhamburg.shared.rendering;

import java.util.HashMap;
import java.util.Map;

import edu.hawhamburg.shared.misc.Logger;

/**
 * This managers keeps track of all registered textures which can be used in a scene.
 *
 * @author Philipp Jenke
 */
public class TextureManager {

    private Map<String, Texture> textures = new HashMap<String, Texture>();

    private static TextureManager instance = null;

    private TextureManagerPlatform platform;

    private TextureManager() {
    }

    public static TextureManager getInstance() {
        if (instance == null) {
            instance = new TextureManager();
        }
        return instance;
    }

    public void setup(TextureManagerPlatform platform) {
        this.platform = platform;
    }

    public TextureManagerPlatform getPlatform() {
        return platform;
    }

    /**
     * Return the texure for the texture name.
     */
    public Texture getTexture(String textureName) {
        if (!textures.containsKey(textureName)) {
            Texture texture = platform.getTexture(textureName);
            textures.put(textureName, texture);
        }
        return textures.get(textureName);
    }

    public void deleteAllTextures() {
        for (Texture texture : textures.values()) {
            if (texture != null) {
                texture.delete();
            }
        }
        textures.clear();
        Logger.getInstance().log("Deleted all textures.");
    }
}
