package edu.hawhamburg.shared.rendering;

/**
 * Shared interface for platform specific texture manager features
 */
public interface TextureManagerPlatform {
    /**
     * Creates a texture for the given filename.
     */
    Texture getTexture(String textureName);
}
