package edu.hawhamburg.shared.rendering;

import edu.hawhamburg.shared.math.Matrix;
import edu.hawhamburg.shared.math.Vector;

import java.nio.FloatBuffer;
import java.nio.IntBuffer;

/**
 * Generic interface to encapsulate platform specific OpenGL commands.
 */
public interface OpenGLPlatform {

    int glCreateProgram();

    void glAttachShader(int shaderProgram, int vertexShaderId);

    void glLinkProgram(int shaderProgram);

    void glValidateProgram(int shaderProgram);

    int glCreateShader(Shader.ShaderType shaderType);

    void glShaderSource(int id, String shaderSource);

    void glCompileShader(int id);

    boolean checkCompileError(int id);

    int glGetAttribLocation(int shaderProgram, String name);

    int glGetUniformLocation(int shaderProgram, String name);

    void glUniform3f(int location, float x, float y, float z);

    void glUniformMatrix4fv(int location, int i, boolean b, float[] floats, int i1);

    void glUniform1i(int location, int value);

    Matrix generateViewMatrix(Vector eye, Vector ref, Vector up);

    Matrix generateOrthoMatrix(int i, int i1, int i2, int i3, int i4, int i5, int i6);

    void glLineWidth(int width);

    void checkGlError();

    void clearColorBuffer();

    void glBindTexture(int textureId);

    void glDeleteTextures(int textureId);

    Object getOpenGLObject();

    enum DataType {
        UNSIGNED_INT, FLOAT
    }

    public enum Primitive {
        TRIANGLES,
        LINES, POINTS, LINE_LOOP
    }

    /**
     * Return the version string.
     */
    String getVersionString();

    /**
     * Enable/Disable stencil
     */
    void enableStencilTest(boolean flag);

    /**
     * Setup (backface) culling
     */
    void setupCulling();

    /**
     * Enable/disable depth
     */
    void enableDepthTest(boolean flag);

    /**
     * Enable/disable culling
     */
    void enableCulling(boolean flag);

    /**
     * Enable/disable blending
     */
    void enableBlending(boolean flag);

    /**
     * Setup blending function
     */
    void setupBlending();

    /**
     * Set the GL object (if it is not used as as singleton.
     */
    void setGLObject(Object gl);

    /**
     * Enable vertex array.
     */
    void enableVertexAttribArray(int location);

    void glVertexAttribPointer(int location, int size, DataType type, boolean normalized, int stride, FloatBuffer positionBuffer);

    void glDrawElements(Primitive primitiveType, int size, DataType glUnsignedInt, IntBuffer indexBuffer);

    void glUseProgram(int program);

    /**
     * Android seems to use a flipped texture coordinate system y axis. This method coorects this.
     */
    Vector fixTexCoord(Vector texCoord);
}
