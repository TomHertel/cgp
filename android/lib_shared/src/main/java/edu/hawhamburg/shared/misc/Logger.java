package edu.hawhamburg.shared.misc;

/**
 * Generic logger for all platforms
 */
public class Logger {
    /**
     * Singleton instance.
     */
    private static Logger instance = null;

    private PlatformLogger platformLogger = null;

    private Logger() {
    }

    public void setup(PlatformLogger platformLogger) {
        this.platformLogger = platformLogger;
    }

    public static Logger getInstance() {
        if (instance == null) {
            instance = new Logger();
        }
        return instance;
    }

    public void log(String msg) {
        if (platformLogger != null) {
            platformLogger.log(msg);
        } else {
            System.out.println("Attention: set platform-specific logger via setup() method.");
        }
    }
}
