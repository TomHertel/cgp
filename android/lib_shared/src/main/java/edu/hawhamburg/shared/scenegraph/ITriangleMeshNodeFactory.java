package edu.hawhamburg.shared.scenegraph;

import edu.hawhamburg.shared.datastructures.mesh.ITriangleMesh;

/**
 * This interface describes factories for TriangleMeshNodes (Scene graph nodes which are able to
 * render ITriangleMesh object).
 */
public interface ITriangleMeshNodeFactory {
    /**
     * Generate a triangle mesh node for the given mesh.
     */
    public ITriangleMeshNode createTriangleMeshNode(ITriangleMesh mesh);
}
