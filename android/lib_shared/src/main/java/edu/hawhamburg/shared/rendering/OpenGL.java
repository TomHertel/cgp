package edu.hawhamburg.shared.rendering;

/**
 * Access to the platorm-specific OpenGL-context
 */
public class OpenGL {
    private static OpenGL instance = null;

    private OpenGLPlatform platform = null;

    private OpenGL() {
    }

    public static OpenGL instance() {
        if (instance == null) {
            instance = new OpenGL();
        }
        return instance;
    }

    public void setup(OpenGLPlatform platform) {
        this.platform = platform;
    }

    public OpenGLPlatform platform() {
        return platform;
    }
}
