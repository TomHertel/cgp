/**
 * Diese Datei gehört zum Android/Java Framework zur Veranstaltung "Computergrafik für
 * Augmented Reality" von Prof. Dr. Philipp Jenke an der Hochschule für Angewandte
 * Wissenschaften (HAW) Hamburg. Weder Teile der Software noch das Framework als Ganzes dürfen
 * ohne die Einwilligung von Philipp Jenke außerhalb von Forschungs- und Lehrprojekten an der HAW
 * Hamburg verwendet werden.
 * <p>
 * This file is part of the Android/Java framework for the course "Computer graphics for augmented
 * reality" by Prof. Dr. Philipp Jenke at the University of Applied (UAS) Sciences Hamburg. Neither
 * parts of the framework nor the complete framework may be used outside of research or student
 * projects at the UAS Hamburg.
 */
package edu.hawhamburg.shared.rendering;

import edu.hawhamburg.shared.math.AxisAlignedBoundingBox;
import edu.hawhamburg.shared.math.Vector;
import edu.hawhamburg.shared.scenegraph.Camera;

/**
 * The interaction controller handles touch input on a GLView.
 */
public abstract class InteractionController {

    /**
     * Fitting factor for automatic camera adjustment.
     */
    double FIT_FACTOR = 1.5;

    /**
     * Called once at the very beginning.
     */
    public abstract void init();

    /**
     * A touch event was detected which moved across the screen.
     */
    public abstract void touchMoved(int dx, int dy);

    /**
     * Stop view controller
     */
    abstract void stop();

    /**
     * Zoom in and out (based on sign)
     */
    public abstract void zoom(int delta);

    /**
     * Set the camera to observe the complete scene.
     */
    public void fitToBoundingBox(AxisAlignedBoundingBox bbox) {
        if (bbox == null) {
            return;
        }
        Vector ref = bbox.getCenter();
        final double diameter = bbox.getDiameter();
        Vector eyeRef = Camera.getInstance().getRef()
                .subtract(Camera.getInstance().getEye()).getNormalized();
        eyeRef = eyeRef.multiply(diameter * FIT_FACTOR);
        Vector eye =
                Camera.getInstance().getRef().subtract(eyeRef);
        Camera.getInstance().setup(eye, ref, Camera.getInstance().getUp());
    }
}
