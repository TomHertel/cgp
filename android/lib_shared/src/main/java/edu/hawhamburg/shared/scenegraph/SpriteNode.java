package edu.hawhamburg.shared.scenegraph;

import edu.hawhamburg.shared.datastructures.mesh.ITriangleMesh;
import edu.hawhamburg.shared.datastructures.mesh.Triangle;
import edu.hawhamburg.shared.datastructures.mesh.TriangleMesh;
import edu.hawhamburg.shared.math.AxisAlignedBoundingBox;
import edu.hawhamburg.shared.math.Matrix;
import edu.hawhamburg.shared.math.Vector;
import edu.hawhamburg.shared.rendering.OpenGL;
import edu.hawhamburg.shared.rendering.OpenGLPlatform;
import edu.hawhamburg.shared.rendering.ShaderAttributes;

/**
 * Render a 2D sprite on screen
 */
public class SpriteNode extends LeafNode {

    /**
     * The texture is mapped to a simple 2-triangle-mesh, which is represented in this node.
     */
    private TriangleMeshNode meshNode;

    private double DEPTH = 0.08;


    /**
     * @param x                   in [-1,1]
     * @param y                   in [-1,1]
     * @param width               in [-1,1]
     * @param height              in [-1,1]
     * @param spriteImageFilename 2D sprite image filename
     */
    public SpriteNode(double x, double y, double width, double height, String spriteImageFilename) {
        this(x, y, width, height, spriteImageFilename, 0.98);
    }


    /**
     * @param x                   in [-1,1]
     * @param y                   in [-1,1]
     * @param width               in [-1,1]
     * @param height              in [-1,1]
     * @param spriteImageFilename 2D sprite image filename
     */
    public SpriteNode(double x, double y, double width, double height, String spriteImageFilename, double depth) {
        this.DEPTH = depth;

        ITriangleMesh fullscreenBackgroundMesh = new TriangleMesh();
        fullscreenBackgroundMesh.addVertex(new Vector(x, y, DEPTH));
        fullscreenBackgroundMesh.addVertex(new Vector(x + width, y, DEPTH));
        fullscreenBackgroundMesh.addVertex(new Vector(x + width, y + height, DEPTH));
        fullscreenBackgroundMesh.addVertex(new Vector(x, y + height, DEPTH));
        fullscreenBackgroundMesh.addTextureCoordinate(OpenGL.instance().platform().fixTexCoord(new Vector(0, 0)));
        fullscreenBackgroundMesh.addTextureCoordinate(OpenGL.instance().platform().fixTexCoord(new Vector(1, 0)));
        fullscreenBackgroundMesh.addTextureCoordinate(OpenGL.instance().platform().fixTexCoord(new Vector(1, 1)));
        fullscreenBackgroundMesh.addTextureCoordinate(OpenGL.instance().platform().fixTexCoord(new Vector(0, 1)));
        fullscreenBackgroundMesh.addTriangle(new Triangle(0, 1, 2, 0, 1, 2));
        fullscreenBackgroundMesh.addTriangle(new Triangle(0, 2, 3, 0, 2, 3));
        fullscreenBackgroundMesh.computeTriangleNormals();
        fullscreenBackgroundMesh.setTextureName(spriteImageFilename);
        meshNode = new TriangleMeshNode(fullscreenBackgroundMesh);
        meshNode.setTextureLighting(TriangleMeshNode.TextureLighting.NO_LIGHTING);
    }

    @Override
    public void traverse(RenderMode mode, Matrix modelMatrix) {
        if (!isActive()) {
            return;
        }
        Matrix identity = Matrix.createIdentityMatrix4();
        // Model matrix
        Matrix openGLModelMatrix = modelMatrix.getTransposed();
        ShaderAttributes.getInstance().setModelMatrixParameter(openGLModelMatrix);
        // Projection matrix
        ShaderAttributes.getInstance().setProjectionMatrixParameter(
                identity);
        // View matrix
        ShaderAttributes.getInstance().setViewMatrixParameter(identity);
        drawGL(mode, identity);
    }

    @Override
    public void drawGL(RenderMode mode, Matrix modelMatrix) {
        meshNode.drawGL(RenderMode.REGULAR, null);
    }

    @Override
    public AxisAlignedBoundingBox getBoundingBox() {
        return null;
    }
}
