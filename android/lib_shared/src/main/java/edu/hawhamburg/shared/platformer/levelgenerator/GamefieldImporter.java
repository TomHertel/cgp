/**
 * Diese Datei gehört zum Android/Java Framework zur Veranstaltung "Computergrafik für
 * Augmented Reality" von Prof. Dr. Philipp Jenke an der Hochschule für Angewandte
 * Wissenschaften (HAW) Hamburg. Weder Teile der Software noch das Framework als Ganzes dürfen
 * ohne die Einwilligung von Philipp Jenke außerhalb von Forschungs- und Lehrprojekten an der HAW
 * Hamburg verwendet werden.
 * <p>
 * This file is part of the Android/Java framework for the course "Computer graphics for augmented
 * reality" by Prof. Dr. Philipp Jenke at the University of Applied (UAS) Sciences Hamburg. Neither
 * parts of the framework nor the complete framework may be used outside of research or student
 * projects at the UAS Hamburg.
 */

package edu.hawhamburg.shared.platformer.levelgenerator;

import edu.hawhamburg.shared.math.Vector;
import edu.hawhamburg.shared.platformer.*;

import static edu.hawhamburg.shared.platformer.levelgenerator.GamefieldImporter.DIR.X;
import static edu.hawhamburg.shared.platformer.levelgenerator.GamefieldImporter.DIR.Z;

/**
 * Importer from a Gamefield to a Game
 */
public class GamefieldImporter {
    enum DIR {X, Z}

    /**
     * Holds the current position in the current line.
     */
    private class Index {
        int x;
        int z;
        DIR dir = X;

        Index(int x, int z, DIR dir) {
            this.x = x;
            this.z = z;
            this.dir = dir;
        }

        Index getNext() {
            return new Index(x + ((dir == X) ? 1 : 0), z + ((dir == Z) ? 1 : 0), dir);
        }

        public Vector getDirVector() {
            if (dir == X) {
                return new Vector(1, 0, 0);
            } else {
                return new Vector(0, 0, 1);
            }
        }
    }

    /**
     * Game to be filled with the Gamefield.
     */
    private Game game;

    /**
     * This Gamefield will be imported.
     */
    private Gamefield gamefield;

    public GamefieldImporter(Game game, Gamefield gamefield) {
        this.game = game;
        this.gamefield = gamefield;
    }

    /**
     * Import gamefield to game. Generate all segments.
     */
    public void importFromGameField() {
        game.clear();
        World world = game.getWorld();
        world.setup(gamefield.getResX(), gamefield.getResZ(), 1);
        Index index = getStart();

        // Start-Brick
        Brick brick = new Brick();
        brick.setup(Brick.Type.GROUND, index.x, index.z, 0);
        world.addBrick(brick);
        game.getDynamicGameState().setStartBrick(brick);
        game.getDynamicGameState().setStartOrientation(new Vector(1, 0, 0));

        // Start generating the bricks and segments (recursively descent at switch bricks).
        generateLine(index);

        // Player
        game.getDynamicGameState().reset();
        game.getPlayer().updateOrientation(game.getDynamicGameState().getOrientation());
        game.getPlayer().setPosition(game.getDynamicGameState().getPosition());
        game.addGameObject(game.getPlayer());
    }

    /**
     * Start generating the bricks and segments (recursively descent at switch bricks).
     */
    private void generateLine(Index index) {
        // Skip first brick (already created
        index = index.getNext();

        while (istGueltig(index)) {
            // Handle current index position
            String symbol = gamefield.getSymbol(index.x, index.z);
            switch (symbol) {
                case Constants.GRAMMAR_END_BLOCK:
                case Constants.GRAMMAR_REGULAR_BLOCK:
                case Constants.GRAMMAR_Z_DIR:
                case Constants.GRAMMAR_X_DIR: {
                    // Simple bricks, no function
                    Brick brick = new Brick();
                    brick.setup(Brick.Type.GROUND, index.x, index.z, 0);
                    game.getWorld().addBrick(brick);
                    break;
                }
                case Constants.GRAMMAR_SWITCH_Z_X: {
                    SwitchBrick brick = new SwitchBrick();
                    brick.setup(Brick.Type.SWITCH, index.x, index.z, 0);
                    brick.addSwitch(Vector.VECTOR_3_Z, Vector.VECTOR_3_X);
                    brick.addSwitch(Vector.VECTOR_3_X.multiply(-1), Vector.VECTOR_3_Z);
                    game.getWorld().addBrick(brick);
                    generateLine(new Index(index.x, index.z, X));
                    break;
                }
                case Constants.GRAMMAR_SWITCH_X_Z: {
                    SwitchBrick brick = new SwitchBrick();
                    brick.setup(Brick.Type.SWITCH, index.x, index.z, 0);
                    brick.addSwitch(Vector.VECTOR_3_X, Vector.VECTOR_3_Z);
                    brick.addSwitch(Vector.VECTOR_3_Z.multiply(-1), Vector.VECTOR_3_X);
                    game.getWorld().addBrick(brick);
                    generateLine(new Index(index.x, index.z, Z));
                    break;
                }
                case "⇅": {
                    SwitchBrick brick = new SwitchBrick();
                    brick.setup(Brick.Type.SWITCH, index.x, index.z, 0);
                    brick.addSwitch(Vector.VECTOR_3_X, Vector.VECTOR_3_X.multiply(-1));
                    game.getWorld().addBrick(brick);
                    break;
                }
                case "⇄": {
                    SwitchBrick brick = new SwitchBrick();
                    brick.setup(Brick.Type.SWITCH, index.x, index.z, 0);
                    brick.addSwitch(Vector.VECTOR_3_Z, Vector.VECTOR_3_Z.multiply(-1));
                    game.getWorld().addBrick(brick);
                    break;
                }

            }
            index = index.getNext();
        }
    }

    /**
     * Returns true if the game field cell at the given index is valid
     */
    private boolean istGueltig(Index index) {
        if (index.x < 0 || index.x >= gamefield.getResX()) {
            return false;
        }
        if (index.z < 0 || index.z >= gamefield.getResZ()) {
            return false;
        }
        if (gamefield.getSymbol(index.x, index.z) == null) {
            return false;
        }
        return true;
    }

    private Index getStart() {
        for (int x = 0; x < gamefield.getResX(); x++) {
            for (int z = 0; z < gamefield.getResZ(); z++) {
                if (Constants.GRAMMAR_START_BLOCK.equals(gamefield.getSymbol(x, z))) {
                    return new Index(x, z, X);
                }
            }
        }
        return null;
    }
}
