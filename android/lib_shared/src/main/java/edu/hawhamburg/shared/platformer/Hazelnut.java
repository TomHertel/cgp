/**
 * Diese Datei gehört zum Android/Java Framework zur Veranstaltung "Computergrafik für
 * Augmented Reality" von Prof. Dr. Philipp Jenke an der Hochschule für Angewandte
 * Wissenschaften (HAW) Hamburg. Weder Teile der Software noch das Framework als Ganzes dürfen
 * ohne die Einwilligung von Philipp Jenke außerhalb von Forschungs- und Lehrprojekten an der HAW
 * Hamburg verwendet werden.
 * <p>
 * This file is part of the Android/Java framework for the course "Computer graphics for augmented
 * reality" by Prof. Dr. Philipp Jenke at the University of Applied (UAS) Sciences Hamburg. Neither
 * parts of the framework nor the complete framework may be used outside of research or student
 * projects at the UAS Hamburg.
 */

package edu.hawhamburg.shared.platformer;

import edu.hawhamburg.shared.datastructures.mesh.ITriangleMesh;
import edu.hawhamburg.shared.datastructures.mesh.ObjReader;
import edu.hawhamburg.shared.datastructures.mesh.TriangleMesh;
import edu.hawhamburg.shared.datastructures.mesh.TriangleMeshFactory;
import edu.hawhamburg.shared.math.Vector;
import edu.hawhamburg.shared.scenegraph.TriangleMeshNode;

import java.util.List;

/**
 * The player can throw one haselnut at a time
 */
public class Hazelnut extends GameObject {

    /**
     * Hazelnut movement speed.
     */
    private static final double SPEED = 0.03;

    /**
     * Throwing direction of the haselnut.
     */
    private Vector direction = new Vector(0, 0, 0);

    public Hazelnut(Vector pos, Vector direction) {
        super(Type.HASELNUT, pos);
        this.direction.copy(direction);

        ObjReader reader = new ObjReader();
        List<ITriangleMesh> meshes = reader.read(new TriangleMeshFactory(), "meshes/haselnut.obj");
        TriangleMesh baseMesh = (TriangleMesh) meshes.get(0);
        setup(meshes.get(0).getBoundingBox());
        addChild(new TriangleMeshNode(meshes.get(0)));
    }

    @Override
    public void updateGameState() {
        updatePosition(direction.multiply(SPEED));
    }

    public Vector getDirection() {
        return direction;
    }
}
