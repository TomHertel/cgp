package edu.hawhamburg.shared.datastructures.mesh;

/**
 * Create triangle mesh objects.
 */
public class TriangleMeshFactory implements ITriangleMeshFactory {
    @Override
    public ITriangleMesh createMesh() {
        return new TriangleMesh();
    }

    @Override
    public AbstractTriangle createTriangle() {
        return new Triangle();
    }

    @Override
    public AbstractTriangle createTriangle(int v0, int v1, int v2, ITriangleMesh mesh) {
        return new Triangle(v0, v1, v2);
    }
}
