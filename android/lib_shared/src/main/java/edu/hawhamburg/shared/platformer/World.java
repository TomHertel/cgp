/**
 * Diese Datei gehört zum Android/Java Framework zur Veranstaltung "Computergrafik für
 * Augmented Reality" von Prof. Dr. Philipp Jenke an der Hochschule für Angewandte
 * Wissenschaften (HAW) Hamburg. Weder Teile der Software noch das Framework als Ganzes dürfen
 * ohne die Einwilligung von Philipp Jenke außerhalb von Forschungs- und Lehrprojekten an der HAW
 * Hamburg verwendet werden.
 * <p>
 * This file is part of the Android/Java framework for the course "Computer graphics for augmented
 * reality" by Prof. Dr. Philipp Jenke at the University of Applied (UAS) Sciences Hamburg. Neither
 * parts of the framework nor the complete framework may be used outside of research or student
 * projects at the UAS Hamburg.
 */

package edu.hawhamburg.shared.platformer;

import edu.hawhamburg.shared.math.Vector;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

/**
 * This is the model class for the world. The world consists of cube-shaped blocks, called bricks.
 */
public class World implements JsonSerializable {

    private static final String JSON_NUMBER_OF_ROWS = "numRows";
    private static final String JSON_NUMBER_OF_COLS = "numCols";
    private static final String JSON_BRICKS = "bricks";
    private static final String JSON_MAX_OFFSET = "maxOffset";

    /**
     * Size of the cubes.
     */
    private double brickSize = 0.2;

    /**
     * Lower-left corner of the world;
     */
    private Vector worldBaseLowerLeft = new Vector(-1, 0, -1);

    /**
     * Looking from top onto the world, it is 2-dimensional. This 2D array contains the bricks in each field.
     */
    private Brick[][][] bricks = null;

    public World() {
    }

    /**
     * Setup the dimensions and the internal data structures.
     */
    public void setup(int numberOfRows, int numberOfCols, int maxOffset) {
        bricks = new Brick[numberOfRows][numberOfCols][maxOffset * 2 + 1];
    }

    /**
     * Number of rows in world.
     */
    public int getNumberOfRows() {
        return bricks.length;
    }

    /**
     * Number of cols in world.
     */
    public int getNumberOfCols() {
        return bricks[0].length;
    }

    /**
     * Return the maximum offset from the 0-level (lower, higher bricks)
     *
     * @return
     */
    public int getMaxHeightOffset() {
        return (bricks[0][0].length - 1) / 2;
    }

    /**
     * Return the lower left origin of the given brick.
     */
    public Vector getBrickOrigin(int row, int column, int offset) {
        Brick brick = getBrick(row, column, offset);
        return worldBaseLowerLeft.add(new Vector(row * brickSize,
                -brickSize * 0.5 + offset * brickSize,
                column * brickSize));
    }

    /**
     * Return the lower left origin of the given brick.
     */
    public Vector getBrickOrigin(Brick brick) {
        return getBrickOrigin(brick.getRow(), brick.getColumn(), brick.getOffset());
    }

    /**
     * Return the brick, return null if the cell is empty.
     */
    public Brick getBrick(int row, int column, int offset) {
        if (!validCell(row, column, offset)) {
            return null;
        }
        return bricks[row][column][offset + getMaxHeightOffset()];
    }

    /**
     * Add a brick at slot (row, column, offset)
     */
    public void addBrick(int row, int column, int offset) {
        Brick brick = new Brick();
        brick.setup(Brick.Type.GROUND, row, column, offset);
        addBrick(brick);
    }

    /**
     * Add a brick at slot. The brick must be set up already.
     */
    public void addBrick(Brick brick) {
        bricks[brick.getRow()][brick.getColumn()][brick.getOffset() + getMaxHeightOffset()] = brick;
    }

    public double getBrickSize() {
        return brickSize;
    }

    /**
     * Returns true if the cell has a neighboring cell along the given side.
     */
    public boolean hasNeighbor(int row, int column, int offset, Brick.Side side) {
        if (!validCell(row, column, offset)) {
            throw new IllegalArgumentException("Invalid cell index.");
        }
        switch (side) {
            case X_NEG:
                return getBrick(row - 1, column, offset) != null;
            case X_POS:
                return getBrick(row + 1, column, offset) != null;
            case Y_NEG:
                return getBrick(row, column, offset - 1) != null;
            case Y_POS:
                return getBrick(row, column, offset + 1) != null;
            case Z_NEG:
                return getBrick(row, column - 1, offset) != null;
            case Z_POS:
                return getBrick(row, column + 1, offset) != null;
        }
        return false;
    }

    /**
     * Returns true of the cell index is valid
     */
    private boolean validCell(int row, int column, int offset) {
        return row >= 0 && row < getNumberOfRows() && column >= 0 && column < getNumberOfCols() &&
                offset >= -getMaxHeightOffset() && offset <= getMaxHeightOffset();
    }

    /**
     * Computes the center of the brick top side.
     */
    public Vector getBrickTopCenter(Brick brick) {
        Vector topCenterPos = getBrickOrigin(brick.getRow(), brick.getColumn(), brick.getOffset());
        topCenterPos.addSelf(Vector.VECTOR_3_Y.multiply(getBrickSize()));
        topCenterPos.addSelf(Vector.VECTOR_3_X.multiply(getBrickSize() * 0.5));
        topCenterPos.addSelf(Vector.VECTOR_3_Z.multiply(getBrickSize() * 0.5));
        return topCenterPos;
    }

    /**
     * Computes and returns the center of the brick.
     */
    public Vector getBrickCenter(Brick brick) {
        Vector center = getBrickOrigin(brick.getRow(), brick.getColumn(), brick.getOffset());
        center.addSelf(Vector.VECTOR_3_Y.multiply(getBrickSize() * 0.5));
        center.addSelf(Vector.VECTOR_3_X.multiply(getBrickSize() * 0.5));
        center.addSelf(Vector.VECTOR_3_Z.multiply(getBrickSize() * 0.5));
        return center;
    }

    /**
     * Returns the brick, the Position pos collides with; returns null if no collision.
     */
    public Brick collidesWithBrick(Vector pos) {
        Vector v = pos.subtract(worldBaseLowerLeft).multiply(1.0 / brickSize);
        int row = (int) v.get(0);
        int column = (int) v.get(2);
        int offset = (int) (v.get(1) + 0.5);

        if (v.get(0) < 0 || v.get(2) < 0) {
            return null;
        }
        Brick collisionBrick = getBrick(row, column, offset);
        return collisionBrick;
    }

    /**
     * Computes the center of the scene.
     */
    public Vector getCenter() {
        return worldBaseLowerLeft.add(new Vector(getNumberOfRows() * brickSize / 2, 0, getNumberOfCols() * brickSize / 2));
    }

    /**
     * Return the diameter of the world scene;
     */
    public double getDiameter() {
        return Math.sqrt(Math.pow(getNumberOfRows() * brickSize, 2) + Math.pow(getNumberOfCols() * brickSize, 2));
    }

    public void clear() {
        bricks = null;
    }

    @Override
    public void fromJson(JSONObject worldObject) {
        // Dimensions
        int numberOfRows = ((Long) worldObject.get(JSON_NUMBER_OF_ROWS)).intValue();
        int numberOfCols = ((Long) worldObject.get(JSON_NUMBER_OF_COLS)).intValue();
        int maxOffset = ((Long) worldObject.get(JSON_MAX_OFFSET)).intValue();
        setup(numberOfRows, numberOfCols, maxOffset);

        // Brick
        JSONArray bricks = (JSONArray) worldObject.get(JSON_BRICKS);
        for (int i = 0; i < bricks.size(); i++) {
            Brick brick = new Brick();
            brick.fromJson((JSONObject) bricks.get(i));
            addBrick(brick);
        }
    }

    /**
     * Returns the brick below the player.
     */
    public Brick getBrickAt(Vector pos) {
        Vector v = pos.subtract(worldBaseLowerLeft).multiply(1.0 / brickSize);
        int row = (int) v.get(0);
        int column = (int) v.get(2);
        int offset = (int) (v.get(1) + 0.4); // virtually move pos into brick

        if (v.get(0) < 0 || v.get(2) < 0) {
            return null;
        }
        Brick collisionBrick = getBrick(row, column, offset);
        return collisionBrick;
    }

    /**
     * Returns true of the position is outside of the world
     */
    public boolean isOutside(Vector position) {
        Vector offset = new Vector(getNumberOfRows() * getBrickSize(),
                (getMaxHeightOffset() + 0.5) * getBrickSize(),
                getNumberOfCols() * getBrickSize());
        for (int i = 0; i < 3; i++) {
            if (position.get(i) < worldBaseLowerLeft.get(i) ||
                    position.get(i) > worldBaseLowerLeft.get(i) + offset.get(i)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Delete the brick from the game scene (required update of scene triangle mesh).
     */
    public void deleteBrick(Brick brick) {
        bricks[brick.getRow()][brick.getColumn()][brick.getOffset() + getMaxHeightOffset()] = null;
    }

    public boolean isReady (){
        return bricks != null;
    }
}