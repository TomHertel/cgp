/**
 * Diese Datei gehört zum Android/Java Framework zur Veranstaltung "Computergrafik für
 * Augmented Reality" von Prof. Dr. Philipp Jenke an der Hochschule für Angewandte
 * Wissenschaften (HAW) Hamburg. Weder Teile der Software noch das Framework als Ganzes dürfen
 * ohne die Einwilligung von Philipp Jenke außerhalb von Forschungs- und Lehrprojekten an der HAW
 * Hamburg verwendet werden.
 * <p>
 * This file is part of the Android/Java framework for the course "Computer graphics for augmented
 * reality" by Prof. Dr. Philipp Jenke at the University of Applied (UAS) Sciences Hamburg. Neither
 * parts of the framework nor the complete framework may be used outside of research or student
 * projects at the UAS Hamburg.
 */

package edu.hawhamburg.shared.platformer;

import exercise4.Monster;
import edu.hawhamburg.shared.math.Vector;
import edu.hawhamburg.shared.misc.AssetPath;
import edu.hawhamburg.shared.misc.Logger;
import edu.hawhamburg.shared.scenegraph.INode;
import edu.hawhamburg.shared.scenegraph.InnerNode;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.*;

/**
 * The game contains all the information about the game.
 */
public class Game implements Observer, JsonSerializable {

    private static final String JSON_WORLD = "world";
    private static final String JSON_DYNAMIC_GAME_STATE = "dynamicGameState";
    private static final String JSON_COINS = "coins";
    private static final String JSON_BRICK = "brick";
    private static final String JSON_MONSTER = "enemies";
    private static final double HASELNUT_THROW_HEIGHT = 0.05;

    /**
     * Game world = world geometry.
     */
    private World world = new World();

    /**
     * Interactively controlled player
     */
    private Player player;

    /**
     * List of plugins for the game
     */
    private List<PlatformerPlugin> plugins = new ArrayList<PlatformerPlugin>();

    /**
     * Game root node in the scene graph
     */
    private final InnerNode gameRootNode;

    /**
     * This list contains all the game objects in the game (e.g. player, enemy, ...)
     */
    private List<GameObject> gameObjects = new ArrayList<>();

    /**
     * This object represents the dynamic flow of the game which controls the camera path thru the game.
     */
    private DynamicGameState dynamicGameState = new DynamicGameState(world);

    /**
     * This is the last hazelnut thrown by the player (only one at a time).
     */
    private Hazelnut hazelnut = null;

    public Game(InnerNode gameRootNode) {
        this.gameRootNode = gameRootNode;
        GameEventQueue.getInstance().addObserver(this);

        // Player
        player = new Player(Vector.ZERO_3);
        player.updateOrientation(dynamicGameState.getOrientation());
        addGameObject(player);

        //createDummyWorld();

        new Timer().schedule(new TimerTask() {
            @Override
            public void run() {
                if (getWorld().isReady()) {
                    updateGameState();
                }

            }
        }, 50, 50);
    }

    public void addGameObject(GameObject gameObject) {
        gameRootNode.addChild(gameObject);
        gameObjects.add(gameObject);
    }

    public void removeGameObject(GameObject gameObject) {
        if (!gameRootNode.removeChild(gameObject)) {
            Logger.getInstance().log("Failed to remove game object from scene graph.");
        }
        if (!gameObjects.remove(gameObject)) {
            Logger.getInstance().log("Failed to remove game object from game objects list.");
        }
    }

    /**
     * Update the game state.
     */
    public void updateGameState() {
        if (dynamicGameState.isPaused()) {
            return;
        }

        dynamicGameState.updateGameState();
        movePlayerInteractive();
        // Check for collision
        movePlayerNonInteractive();

        for (PlatformerPlugin plugin : plugins) {
            plugin.updateGameState();
        }

        for (GameObject gameObject : gameObjects) {
            gameObject.updateGameState();
        }

        if (hazelnut != null) {
            // Manage hazelnut
            if (world.isOutside(hazelnut.getPosition())) {
                removeGameObject(hazelnut);
                hazelnut = null;
                Logger.getInstance().log("Hazelnut has left the game.");
            }
        }
    }

    private void movePlayerInteractive() {
        // Move player
        switch (player.getMoveState()) {
            case RIGHT:
                player.updatePosition(dynamicGameState.getOrientation().multiply(Constants.PLAYER_SPEED));
                player.updateOrientation(dynamicGameState.getOrientation());
                break;
            case LEFT:
                player.updatePosition(dynamicGameState.getOrientation().multiply(-Constants.PLAYER_SPEED));
                player.updateOrientation(dynamicGameState.getOrientation().multiply(-1));
                break;
        }

        // Jumps not allowed while jumping
        // Check for collision
        //if (!player.isSimulated()) {
        switch (player.getMoveState()) {
            case JUMP_LEFT:
                if (!player.isJumping()) {
                    player.jump(player.getPosition(), dynamicGameState.getOrientation().multiply(-1));
                    player.updateOrientation(dynamicGameState.getOrientation().multiply(-1));
                }
                break;
            case JUMP_RIGHT:
                if (!player.isJumping()) {
                    player.jump(player.getPosition(), dynamicGameState.getOrientation());
                    player.updateOrientation(dynamicGameState.getOrientation());
                }
                break;
            case JUMP:
                if (!player.isJumping()) {
                    player.jump(player.getPosition(), Vector.ZERO_3);
                }
                break;
            case DOWN:
                Brick brick = world.getBrickAt(player.getPosition());
                if (brick instanceof SwitchBrick) {
                    SwitchBrick switchBrick = (SwitchBrick) brick;
                    for (int i = 0; i < switchBrick.getNumberOfSwitches(); i++) {
                        SwitchBrick.Switch s = switchBrick.getSwitch(i);
                        if (dynamicGameState.getOrientation().multiply(s.from) > 0.98) {
                            dynamicGameState.setOrientation(s.to);
                            dynamicGameState.setPosition(world.getBrickTopCenter(switchBrick));
                            GameEventQueue.getInstance().emitEvent(new GameEvent(GameEvent.Type.SWITCH));
                            player.updateOrientation(dynamicGameState.getOrientation());
                            player.setPosition(dynamicGameState.getPosition());
                            player.setMoveState(Player.Move.IDLE);
                            //Logger.getInstance().log("Switch orientation from " + s.from + " to " + s.to);
                            break;
                        }
                    }
                }
                break;
        }

    }

    /**
     * Simulate the non-interactive player movement.
     */
    public void movePlayerNonInteractive() {
        player.updateVelocity(Constants.GRAVITY.multiply(Constants.INTEGRATION_STEP_SIZE));
        player.updatePosition(player.getVelocity().multiply(Constants.INTEGRATION_STEP_SIZE));
        Brick collisionBrick = world.collidesWithBrick(player.getPosition());
        if (collisionBrick != null) {
            if (collisionBrick.getType() == Brick.Type.BREAKING) {
                GameEventQueue.getInstance().emitEvent(new GameEvent(GameEvent.Type.BREAKING_BRICK, collisionBrick));
            }
            //player.setIsSimulated(false);
            player.getPosition().set(1, world.getBrickTopCenter(collisionBrick).get(1) - Constants.EPSILON);
            player.setVelocity(Vector.ZERO_3);
        }
    }

    /**
     * Add a plugin
     */
    public void addPlugin(PlatformerPlugin plugin) {
        plugins.add(plugin);
        plugin.setup(this);
        Logger.getInstance().log("Registered plugin " + plugin.getPluginName());
    }

    public void regenerate_world(WorldMeshGeneratorPlugin plugin) {
        gameRootNode.removeChild(plugin.getTriangleMeshNode());
        plugin.init();
        INode node = plugin.getSceneGraphContent();
        if (node != null) {
            gameRootNode.addChild(node);
        }
    }


    public void initPlugins() {
        for (PlatformerPlugin plugin : plugins) {
            plugin.init();
            INode node = plugin.getSceneGraphContent();
            if (node != null) {
                gameRootNode.addChild(node);
            }
        }
    }

    public void fromJson(String filename) {
        try (InputStreamReader reader = new InputStreamReader(AssetPath.getInstance().readTextFileToStream(filename))) {
            clear();
            JSONParser parser = new JSONParser();
            try {
                Object obj = parser.parse(reader);
                JSONObject levelObject = (JSONObject) obj;
                fromJson(levelObject);
                Logger.getInstance().log("Successfully read level from file.");
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (org.json.simple.parser.ParseException e) {
                e.printStackTrace();
            }
        } catch (FileNotFoundException e) {
            Logger.getInstance().log("Failed to read level from " + filename);
        } catch (IOException e) {
            Logger.getInstance().log("Failed to read level from " + filename);
        }
    }

    @Override
    public void fromJson(JSONObject levelObject) {

        Set<Map.Entry> entries = levelObject.entrySet();
        for (Map.Entry entry : entries) {
            String key = (String) entry.getKey();
            Object value = entry.getValue();
            switch (key) {
                case JSON_WORLD:
                    // Read world
                    world.fromJson((JSONObject) levelObject.get(JSON_WORLD));
                    break;
                case JSON_MONSTER:
                    // monsters
                    JSONArray monsters = (JSONArray) value;
                    for (int i = 0; i < monsters.size(); i++) {

                        Brick brick2 = new Brick();
                        brick2.fromJson((JSONObject) ((JSONObject) monsters.get(i)).get(JSON_BRICK));
                        Monster monster = new Monster(world.getBrickTopCenter(brick2));
                        addGameObject(monster);
                    }
                    break;

                case JSON_COINS:
                    // Coins
                    JSONArray coins = (JSONArray) value;
                    for (int i = 0; i < coins.size(); i++) {

                        Brick brick = new Brick();
                        brick.fromJson((JSONObject) ((JSONObject) coins.get(i)).get(JSON_BRICK));
                        Coin coin = new Coin(world.getBrickTopCenter(brick));
                        addGameObject(coin);
                    }
                default:
                    // Let plugins handle json object
                    for (PlatformerPlugin plugin : plugins) {
                        plugin.handleJson((String) key, value);
                    }
            }
        }

        // Dynamic game state
        JSONObject dynamicGameStateObject = (JSONObject) levelObject.get(JSON_DYNAMIC_GAME_STATE);
        dynamicGameState.fromJson(dynamicGameStateObject);

        dynamicGameState.reset();
        player.updateOrientation(dynamicGameState.getOrientation());
        player.setPosition(dynamicGameState.getPosition());
        addGameObject(player);
    }

    /**
     * Clear content.
     */
    public void clear() {
        world.clear();
        gameObjects.clear();
        dynamicGameState.clear();
        player.reset();
    }

    @Override
    public void update(Observable o, Object arg) {
        if (o == GameEventQueue.getInstance()) {
            GameEvent gameEvent = (GameEvent) arg;

            for (PlatformerPlugin plugin : plugins) {
                plugin.handleGameEvent(gameEvent);
            }

            switch (gameEvent.getType()) {
                case COLLISION_COIN:
                    player.coinCollected();
                    removeGameObject((GameObject) gameEvent.getPayload());
                    break;
                case COLLISION_MONSTER:
                    player.playerLoosesLife();
                    break;
                case HAZELNUT_HIT_MONSTER:
                    Logger.getInstance().log("Hazelnut hit monster");
                    removeGameObject((GameObject) gameEvent.getPayload());
                    removeGameObject(hazelnut);
                    hazelnut = null;
                    break;
            }
        }
    }

    public void throwHazelnut() {
        if (hazelnut == null) {
            Hazelnut haselnut = new Hazelnut(player.getPosition().add(Vector.VECTOR_3_Y.multiply(HASELNUT_THROW_HEIGHT)),
                    dynamicGameState.getOrientation());
            addGameObject(haselnut);
            this.hazelnut = haselnut;
        } else {
            Logger.getInstance().log("Cannot throw new haselnet, old one still underway.");
        }
    }

    public DynamicGameState getDynamicGameState() {
        return dynamicGameState;
    }

    public World getWorld() {
        return world;
    }

    public Player getPlayer() {
        return player;
    }

    public int getNumberOfGameObjects() {
        return gameObjects.size();
    }

    public GameObject getGameObject(int index) {
        return gameObjects.get(index);
    }

    public Hazelnut getHazelnut() {
        return hazelnut;
    }
}
