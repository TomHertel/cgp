/**
 * Diese Datei gehört zum Android/Java Framework zur Veranstaltung "Computergrafik für
 * Augmented Reality" von Prof. Dr. Philipp Jenke an der Hochschule für Angewandte
 * Wissenschaften (HAW) Hamburg. Weder Teile der Software noch das Framework als Ganzes dürfen
 * ohne die Einwilligung von Philipp Jenke außerhalb von Forschungs- und Lehrprojekten an der HAW
 * Hamburg verwendet werden.
 * <p>
 * This file is part of the Android/Java framework for the course "Computer graphics for augmented
 * reality" by Prof. Dr. Philipp Jenke at the University of Applied (UAS) Sciences Hamburg. Neither
 * parts of the framework nor the complete framework may be used outside of research or student
 * projects at the UAS Hamburg.
 */

package edu.hawhamburg.shared.platformer;

import edu.hawhamburg.shared.math.Vector;

/**
 * This static class contains all the 'magic numbers' of the platformer game
 */
public class Constants {
    /**
     * Integration step size
     */
    public static final double INTEGRATION_STEP_SIZE = 0.02;

    /**
     * Gravitational accelleration vector (integration step size = 0.1)
     */
    public static final Vector GRAVITY = new Vector(0, -0.981, 0);

    /**
     * Positive constant 'close to zero'
     */
    public static final double EPSILON = 1e-5;

    /**
     * Movement speed of the player
     */
    public static final double PLAYER_SPEED = 0.03;

    /**
     * Width of a player jump
     */
    public static final double PLAYER_JUMP_LENGTH = 0.2;

    /**
     * Height of a player jump
     */
    public static final double PLAYER_JUMP_HEIGHT = 0.2;

    /**
     * Initial number of lives of the player.
     */
    public static final int START_NUMBER_PLAYER_LIFES = 3;

    /**
     * Number of seconds the player is invulnerable after loosing a live.
     */
    public static final int SECONDS_INVULNERABLE = 3;
    /**
     * Grammar symbols.
     */
    public static final String GRAMMAR_START_BLOCK = "B";
    public static final String GRAMMAR_END_BLOCK = "E";
    public static final String GRAMMAR_REGULAR_BLOCK = "G";
    public static final String GRAMMAR_Z_DIR = "→";
    public static final String GRAMMAR_X_DIR = "↓";
    public static final String GRAMMAR_SWITCH_Z_X = "↲";
    public static final String GRAMMAR_SWITCH_X_Z = "↳";
}
