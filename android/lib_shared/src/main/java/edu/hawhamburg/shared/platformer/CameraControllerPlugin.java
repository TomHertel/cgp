package edu.hawhamburg.shared.platformer;

import edu.hawhamburg.shared.math.Matrix;
import edu.hawhamburg.shared.math.Vector;
import edu.hawhamburg.shared.scenegraph.Camera;
import edu.hawhamburg.shared.scenegraph.INode;

public class CameraControllerPlugin extends PlatformerPlugin{

    private int step = 1;
    private double steps = 50;
    private double winkel = 0;
    private Vector ori = new Vector(0,0,0);
    private Vector lastori = new Vector(0,0,0);
    private Vector lastref = new Vector(0,0,0);
    private Vector ref = new Vector(0,0,0);
    private  Vector up = new Vector(0,1,0);

    @Override
    public void init() {
        lastori.copy(game.getDynamicGameState().getOrientation());
        lastref.copy(game.getDynamicGameState().getPosition());

    }

    @Override
    public void updateGameState() {

        DynamicGameState dynamic = game.getDynamicGameState();
        Vector eye;

        if(dynamic.isRotating())
        {
            Matrix rotation = Matrix.createRotationMatrix3(up,winkel*step/steps);
            Vector cross = rotation.multiply(lastori.cross(up));
            //cross = ori.cross(up);

            double alpha = step/steps;
            Vector r = lastref.multiply(1-alpha).add(ref.multiply(alpha));

            eye = r.add(cross);
            if(step == steps)
            {
                step = 0;
                dynamic.setIsRotating(false);
            }

            step++;

        }
        else
        {
            ref = dynamic.getPosition();
            ori = dynamic.getOrientation();
            lastori.copy(ori);
            lastref.copy(ref);
            eye = ref.add(ori.cross(up));
        }

        eye.set(1,eye.y()+0.2d);
        Camera.getInstance().setup(eye,ref,up);

    }

    @Override
    public INode getSceneGraphContent() {
        return null;
    }

    @Override
    public void handleJson(String key, Object value) {

    }

    @Override
    public String getPluginName() {
        return null;
    }

    @Override
    public void handleGameEvent(GameEvent gameEvent) {

        if(gameEvent.getType() == GameEvent.Type.SWITCH){
            game.getDynamicGameState().setIsRotating(true);
            winkel = Math.acos((lastori.multiply(ori))/(lastori.getNorm()*ori.getNorm()));
        }

    }

}
