package edu.hawhamburg.shared.scenegraph;

import edu.hawhamburg.shared.math.Matrix;

public class AnimationNode extends InnerNode {


    private int pos = 0;

    @Override
    public synchronized void traverse(RenderMode mode, Matrix modelMatrix) {
        if (!isActive()) {
            return;
        }
        if (pos == getNumberOfChildren()) {
            pos = 0;
        }
        getChild(pos).traverse(mode, modelMatrix);
        pos++;


    }

}


