/**
 * Diese Datei gehört zum Android/Java Framework zur Veranstaltung "Computergrafik für
 * Augmented Reality" von Prof. Dr. Philipp Jenke an der Hochschule für Angewandte
 * Wissenschaften (HAW) Hamburg. Weder Teile der Software noch das Framework als Ganzes dürfen
 * ohne die Einwilligung von Philipp Jenke außerhalb von Forschungs- und Lehrprojekten an der HAW
 * Hamburg verwendet werden.
 * <p>
 * This file is part of the Android/Java framework for the course "Computer graphics for augmented
 * reality" by Prof. Dr. Philipp Jenke at the University of Applied (UAS) Sciences Hamburg. Neither
 * parts of the framework nor the complete framework may be used outside of research or student
 * projects at the UAS Hamburg.
 */

package edu.hawhamburg.shared.platformer;

import edu.hawhamburg.shared.misc.Logger;
import edu.hawhamburg.shared.misc.Scene;
import edu.hawhamburg.shared.rendering.OpenGL;
import edu.hawhamburg.shared.scenegraph.INode;
import edu.hawhamburg.shared.scenegraph.InnerNode;
import edu.hawhamburg.shared.platformer.levelgenerator.Gamefield;
import edu.hawhamburg.shared.platformer.levelgenerator.GamefieldImporter;
import edu.hawhamburg.shared.platformer.levelgenerator.Grammar;
import exercise5.CollisionPlugin;
import exercise6.BreakingBrickPlugin;
import exercise7.Evaluator;
import exercise7.GeneratorPlugin;


/**
 * This scene is the entry point for the platformer game.
 */
public class PlatformerScene extends Scene {

    /**
     * World to be used.
     */
    private Game game;

    /**
     * Represents the state of the key (pressed key set).
     */
    private int keyState;

    private int frameCounter = 0;
    private long timeSinceLastMeasurement = 0;

    /**
     * Key constants
     */
    private static final int KEY_LEFT = 2;
    private static final int KEY_RIGHT = 4;
    private static final int KEY_JUMP = 8;
    private static final int KEY_DOWN = 16;

    public WorldMeshGeneratorPlugin wmgp;


    public PlatformerScene(String vShaderFilename, String fShaderFilename) {
        super(100, INode.RenderMode.REGULAR, vShaderFilename, fShaderFilename);
    }

    @Override
    public void onSetup(InnerNode rootNode) {

        OpenGL.instance().platform().enableBlending(true);

        game = new Game(getRoot());

        // Default
        game.addPlugin(new BackgroundPlugin());
        game.addPlugin(new DisplayStatePlugin());

        // Aufgabe 1 + 2
        wmgp = new WorldMeshGeneratorPlugin();
        game.addPlugin(new WorldMeshGeneratorPlugin());
        // Aufgabe 2
//        game.addPlugin(new PlayerAnimationPlugin());
        // Aufgabe 3
        game.addPlugin(new CameraControllerPlugin());
        // Aufgabe 4
        //game.addPlugin(new FlyingMonsterPlugin());
        // Aufgabe 5
        game.addPlugin(new CollisionPlugin());
        // Aufgabe 6
        game.addPlugin(new BreakingBrickPlugin());

        // Level laden/Aufgabe 7
//        game.fromJson("misc/platformer/level01.json");

        Grammar grammar = Grammar.fromGrammarFile("misc/platformer/grammar.grammar");
        Evaluator eval = new Evaluator(grammar);
        Gamefield gamefield = new Gamefield(10, 10);
        Logger.getInstance().log(gamefield.toString());
        gamefield = eval.derive(gamefield, 7);
        Logger.getInstance().log(gamefield.toString());
        GamefieldImporter gameFieldImporter = new GamefieldImporter(game, gamefield);
        gameFieldImporter.importFromGameField();

        game.addPlugin(new GeneratorPlugin());

        game.initPlugins();
    }

    @Override
    public void onTimerTick(int counter) {
    }

    @Override
    public void onSceneRedraw() {
        frameCounter++;
        int NUMBER_OF_FRAMES = 100;
        if (frameCounter >= NUMBER_OF_FRAMES) {
            long currentTime = System.currentTimeMillis();
            double seconds = (currentTime - timeSinceLastMeasurement) / 1000.0;
            int fps = (int) (NUMBER_OF_FRAMES / seconds);
            Logger.getInstance().log("FPS: " + fps);
            timeSinceLastMeasurement = currentTime;
            frameCounter = 0;
        }
    }

    @Override
    public void onKeyPressed(int keyCode) {
        super.onKeyPressed(keyCode);
        switch (keyCode) {
            case 32: // Space
                game.throwHazelnut();
                break;
            case 80: // 'p'
                game.getDynamicGameState().togglePause();
                break;
            case 38:
                // Move jump
                keyState = keyState | KEY_JUMP;
                break;
            case 37:
                // Move left
                keyState = keyState | KEY_LEFT;
                break;
            case 39:
                // Move right
                keyState = keyState | KEY_RIGHT;
                break;
            case 40:
                // Move down
                keyState = keyState | KEY_DOWN;
                break;
            default:
                Logger.getInstance().log("Unhandled key pressed: " + keyCode);
        }
        playerMoveStateFromKeyState();
    }

    @Override
    public void onKeyReleased(int keyCode) {
        super.onKeyReleased(keyCode);
        switch (keyCode) {
            case 38:
                // Jump
                keyState = keyState & ~KEY_JUMP;
                break;
            case 37:
                // Move left
                keyState = keyState & ~KEY_LEFT;
                break;
            case 39:
                // Move right
                keyState = keyState & ~KEY_RIGHT;
                break;
            case 40:
                // Move down
                keyState = keyState & ~KEY_DOWN;
                break;
            default:
                //Logger.getInstance().log("Unhandled key released: " + keyCode);
        }
        playerMoveStateFromKeyState();
    }

    /**
     * Determines the player move state from the key state.
     */
    private void playerMoveStateFromKeyState() {
        if (((keyState & KEY_LEFT) != 0) && ((keyState & KEY_JUMP) != 0)) {
            game.getPlayer().setMoveState(Player.Move.JUMP_LEFT);
        } else if (((keyState & KEY_RIGHT) != 0) && ((keyState & KEY_JUMP) != 0)) {
            game.getPlayer().setMoveState(Player.Move.JUMP_RIGHT);
        } else if ((keyState & KEY_LEFT) != 0) {
            game.getPlayer().setMoveState(Player.Move.LEFT);
        } else if ((keyState & KEY_RIGHT) != 0) {
            game.getPlayer().setMoveState(Player.Move.RIGHT);
        } else if ((keyState & KEY_JUMP) != 0) {
            game.getPlayer().setMoveState(Player.Move.JUMP);
        } else if ((keyState & KEY_DOWN) != 0) {
            game.getPlayer().setMoveState(Player.Move.DOWN);
        } else {
            game.getPlayer().setMoveState(Player.Move.IDLE);
        }
    }
}
