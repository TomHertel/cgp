package edu.hawhamburg.shared.math;

import java.util.stream.Stream;

public class Intervall {

    private double t0;
    private double t1;

    public Intervall(){
    }

    public Intervall(double t0, double t1){
        this.t0 = t0;
        this.t1 = t1;
    }

    public void set(double t0, double t1){
        this.t0 = t0;
        this.t1 = t1;
    }

    public double getT0(){
        return t0;
    }

    public double getT1(){
        return t1;
    }

    public static Intervall intersection(Intervall Tx,Intervall Ty,Intervall Tz){
        Intervall Tw = new Intervall(Stream.of(Tx.getT0(), Ty.getT0(), Tz.getT0()).max(Double::compareTo).orElse(0.0),
                Stream.of(Tx.getT1(), Ty.getT1(), Tz.getT1()).min(Double::compareTo).orElse(0.0));

        return Tw;
    }
}
