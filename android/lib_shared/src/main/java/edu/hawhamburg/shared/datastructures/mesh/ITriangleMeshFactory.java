package edu.hawhamburg.shared.datastructures.mesh;

/**
 * This factory allows for the creation of mesh objects.
 */
public interface ITriangleMeshFactory {
    /**
     * Create an empty mesh object.
     */
    public ITriangleMesh createMesh();

    /**
     * Create a triangle object.
     */
    public AbstractTriangle createTriangle();

    /**
     * Create a triangle object from three vertex indices.
     */
    public AbstractTriangle createTriangle(int v0, int v1, int v2, ITriangleMesh mesh);
}
