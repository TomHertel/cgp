package edu.hawhamburg.shared.platformer;

import edu.hawhamburg.shared.datastructures.mesh.TriangleMesh;
import edu.hawhamburg.shared.math.Vector;
import edu.hawhamburg.shared.scenegraph.INode;
import edu.hawhamburg.shared.scenegraph.TriangleMeshNode;

public class WorldMeshGeneratorPlugin extends PlatformerPlugin {

    private TriangleMeshNode triangleMeshNode;
    private  boolean regenerating = false;
    private  Brick breakingBrick = null;

    private Vector CORNER_POINTS[] = {
            new Vector(0, 0, 0), new Vector(1, 0, 0),
            new Vector(1, 1, 0), new Vector(0, 1, 0),
            new Vector(0, 0, 1), new Vector(1, 0, 1),
            new Vector(1, 1, 1), new Vector(0, 1, 1)
    };

    private int[][] FACET_INCIDES = {
            {4, 0, 3, 7}, {1, 5, 6, 2},
            {4, 5, 1, 0}, {3, 2, 6, 7},
            {0, 1, 2, 3}, {5, 4, 7, 6}
    };

    private Vector TEXTURE_POINTS[] = {
            new Vector(0, 0), new Vector(1, 0),
            new Vector(1, 1), new Vector(0, 1)
    };

    private Vector SIDE_SHIFT[] = {
            new Vector(2.0 / 6, 0),new Vector(0, 1.0/6),
            new Vector(0, 0), new Vector(1.0/6, 0),
            new Vector(1.0/6, 1.0/6), new Vector(2.0/6, 1.0/6)
    };

    @Override
    public void init() {
        World world = game.getWorld();

        double s = world.getBrickSize();

        TriangleMesh triangleMesh = new TriangleMesh();

        triangleMesh.setTextureName("textures/platformer_textures.png.");

        int offset  = 0;
        int offset2 = 0;

        for (int r = 0; r < world.getNumberOfRows(); r++) {

            for (int c = 0; c < world.getNumberOfCols(); c++) {

                for (int o = 0; o < world.getMaxHeightOffset(); o++) {

                    Brick brick = world.getBrick(r, c, o);

                    if (brick != null) {

                        if (brick != null) {

                        Vector ll = world.getBrickOrigin(brick);

                        for (Vector CORNER_POINT : CORNER_POINTS) {

                            Vector v = ll.add(CORNER_POINT.multiply(s));

                            triangleMesh.addVertex(v);

                        }

                        for (Brick.Side side : Brick.Side.values()) {

                            int i = side.ordinal();

                            if (!world.hasNeighbor(r, c, o, side)) {

                                for (Vector TEXTURE_POINT : TEXTURE_POINTS) {

                                    Vector ll2 = SIDE_SHIFT[i];
                                    Vector v = ll2.add(TEXTURE_POINT.multiply(1.0/6));

                                    triangleMesh.addTextureCoordinate(v);

                                }

                                triangleMesh.addTriangle(
                                        FACET_INCIDES[i][1] + offset,
                                        FACET_INCIDES[i][0] + offset,
                                        FACET_INCIDES[i][2] + offset,
                                        3 + offset2,
                                        2 + offset2,
                                        0 + offset2);

                                triangleMesh.addTriangle(
                                        FACET_INCIDES[i][2] + offset,
                                        FACET_INCIDES[i][0] + offset,
                                        FACET_INCIDES[i][3] + offset,
                                        0 + offset2,
                                        2 + offset2,
                                        1 + offset2);

                                offset2 += 4;

                            }

                        }

                        offset  += 8;

                    }

                    }
                }

            }

        }

      triangleMeshNode =  new TriangleMeshNode(triangleMesh);
    }

    @Override
    public void updateGameState() {

        if (regenerating) {

            game.regenerate_world(this);

            regenerating = false;

            breakingBrick = null;
        }

    }

    @Override
    public INode getSceneGraphContent() {

        return triangleMeshNode;

    }


    @Override
    public void handleJson(String key, Object value) {

    }

    @Override
    public String getPluginName() {
        return null;
    }

    @Override
    public void handleGameEvent(GameEvent gameEvent) {

        if (gameEvent.getType() == GameEvent.Type.REGENERATE_WORLD) {
            //

            breakingBrick = (Brick) gameEvent.getPayload();
            regenerating = true;
        }
    }

    public TriangleMeshNode getTriangleMeshNode() {
        return triangleMeshNode;
    }

}
