/**
 * Diese Datei gehört zum Android/Java Framework zur Veranstaltung "Computergrafik für
 * Augmented Reality" von Prof. Dr. Philipp Jenke an der Hochschule für Angewandte
 * Wissenschaften (HAW) Hamburg. Weder Teile der Software noch das Framework als Ganzes dürfen
 * ohne die Einwilligung von Philipp Jenke außerhalb von Forschungs- und Lehrprojekten an der HAW
 * Hamburg verwendet werden.
 * <p>
 * This file is part of the Android/Java framework for the course "Computer graphics for augmented
 * reality" by Prof. Dr. Philipp Jenke at the University of Applied (UAS) Sciences Hamburg. Neither
 * parts of the framework nor the complete framework may be used outside of research or student
 * projects at the UAS Hamburg.
 */

package edu.hawhamburg.shared.platformer;

import edu.hawhamburg.shared.math.Vector;

import org.json.simple.JSONObject;
import org.json.simple.JSONArray;

public class DynamicGameState implements JsonSerializable {

    /**
     * Movement speed of the dynamic game state
     */
    private static final double SPEED = 0.001;
    private static final String JSON_SWITCH_BRICKS = "switchBricks";
    private static final String JSON_START_BRICK = "startBrick";
    private static final String JSON_START_ORIENTATION = "startOrientation";

    /**
     * Orientation of the game - defines current game plane together with up (0,1,0)
     */
    private Vector orientation = new Vector(1, 0, 0);

    /**
     * The game starts here.
     */
    private Vector pos = new Vector(0, 0, 0);

    /**
     * Reference to the world
     */
    private final World world;

    /**
     * This flag indicates that the game is paused.
     */
    private boolean isPaused = false;

    /**
     * This flag indicates that the dynamic game state is currently controlled otherwise.
     */
    private boolean isRotating = false;

    private Brick startBrick = null;

    public DynamicGameState(World world) {
        this.world = world;
    }

    /**
     * Returns the position of the game (not the player position)
     */
    public Vector getPosition() {
        return pos;
    }

    /**
     * Returns the position of the game (not the player position)
     */
    public Vector getOrientation() {
        return orientation;
    }

    public void clear() {
    }

    /**
     * Move the game state position
     */
    public void updateGameState() {
        if (!isPaused && !isRotating) {
            pos.copy(pos.add(orientation.multiply(SPEED)));
        }
    }

    @Override
    public void fromJson(JSONObject dynamicGameStateObject) {

        // Start brick
        JSONObject jsonStartBrick = (JSONObject) dynamicGameStateObject.get(JSON_START_BRICK);
        startBrick = new Brick();
        startBrick.fromJson(jsonStartBrick);

        // Start orientation
        JSONArray jsonStartOrientation = (JSONArray) dynamicGameStateObject.get(JSON_START_ORIENTATION);
        orientation = SwitchBrick.vectorFromJson(jsonStartOrientation);

        // Read switch bricks
        JSONArray jsonSwitchBricks = (JSONArray) dynamicGameStateObject.get(JSON_SWITCH_BRICKS);
        for (int i = 0; i < jsonSwitchBricks.size(); i++) {
            JSONObject jsonSwitchBrick = (JSONObject) jsonSwitchBricks.get(i);
            SwitchBrick brick = new SwitchBrick();
            brick.fromJson(jsonSwitchBrick);
            world.addBrick(brick);
        }
    }

    public void togglePause() {
        isPaused = !isPaused;
    }

    public boolean isPaused() {
        return isPaused;
    }

    public void setIsRotating(boolean isRotating) {
        this.isRotating = isRotating;
    }

    public boolean isRotating() {
        return isRotating;
    }

    /**
     * Set new game orientation.
     */
    public void setOrientation(Vector newOrientation) {
        orientation.copy(newOrientation);
    }

    public void reset() {
        pos = world.getBrickTopCenter(startBrick);
    }

    public void setPosition(Vector newPosition) {
        pos.copy(newPosition);
    }

    public void setStartBrick(Brick brick) {
        startBrick = brick;
    }

    public void setStartOrientation(Vector vector) {
        orientation.copy(vector);
    }
}
