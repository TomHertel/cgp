/**
 * Diese Datei gehört zum Android/Java Framework zur Veranstaltung "Computergrafik für
 * Augmented Reality" von Prof. Dr. Philipp Jenke an der Hochschule für Angewandte
 * Wissenschaften (HAW) Hamburg. Weder Teile der Software noch das Framework als Ganzes dürfen
 * ohne die Einwilligung von Philipp Jenke außerhalb von Forschungs- und Lehrprojekten an der HAW
 * Hamburg verwendet werden.
 * <p>
 * This file is part of the Android/Java framework for the course "Computer graphics for augmented
 * reality" by Prof. Dr. Philipp Jenke at the University of Applied (UAS) Sciences Hamburg. Neither
 * parts of the framework nor the complete framework may be used outside of research or student
 * projects at the UAS Hamburg.
 */

package edu.hawhamburg.shared.platformer;

import edu.hawhamburg.shared.scenegraph.FullscreenTextureNode;
import edu.hawhamburg.shared.scenegraph.INode;

/**
 * Render the background image
 */
public class BackgroundPlugin extends PlatformerPlugin {
    @Override
    public void init() {
    }

    @Override
    public void updateGameState() {
    }

    @Override
    public INode getSceneGraphContent() {
        return new FullscreenTextureNode("jungle_background.jpg");
    }

    @Override
    public void handleJson(String key, Object value) {
    }

    @Override
    public String getPluginName() {
        return "BackgroundPlugin";
    }

    @Override
    public void handleGameEvent(GameEvent gameEvent) {
    }

    private void draw() {
    }
}
