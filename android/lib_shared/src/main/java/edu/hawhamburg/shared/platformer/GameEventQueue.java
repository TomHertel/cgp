/**
 * Diese Datei gehört zum Android/Java Framework zur Veranstaltung "Computergrafik für
 * Augmented Reality" von Prof. Dr. Philipp Jenke an der Hochschule für Angewandte
 * Wissenschaften (HAW) Hamburg. Weder Teile der Software noch das Framework als Ganzes dürfen
 * ohne die Einwilligung von Philipp Jenke außerhalb von Forschungs- und Lehrprojekten an der HAW
 * Hamburg verwendet werden.
 * <p>
 * This file is part of the Android/Java framework for the course "Computer graphics for augmented
 * reality" by Prof. Dr. Philipp Jenke at the University of Applied (UAS) Sciences Hamburg. Neither
 * parts of the framework nor the complete framework may be used outside of research or student
 * projects at the UAS Hamburg.
 */

package edu.hawhamburg.shared.platformer;

import java.util.Observable;

/**
 * This singleton instance handles all event and passes them to however is interested.
 */
public class GameEventQueue extends Observable {

    /**
     * Singleton instance.
     */
    private static GameEventQueue instance;

    /**
     * Private constructor
     */
    private GameEventQueue() {
    }

    /**
     * Access to the singleton instance.
     */
    public static GameEventQueue getInstance() {
        if (instance == null) {
            instance = new GameEventQueue();
        }
        return instance;
    }

    public void emitEvent(GameEvent gameEvent) {
        setChanged();
        notifyObservers(gameEvent);
    }
}
