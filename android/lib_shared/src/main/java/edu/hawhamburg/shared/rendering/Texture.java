/**
 * Diese Datei gehört zum Android/Java Framework zur Veranstaltung "Computergrafik für
 * Augmented Reality" von Prof. Dr. Philipp Jenke an der Hochschule für Angewandte
 * Wissenschaften (HAW) Hamburg. Weder Teile der Software noch das Framework als Ganzes dürfen
 * ohne die Einwilligung von Philipp Jenke außerhalb von Forschungs- und Lehrprojekten an der HAW
 * Hamburg verwendet werden.
 * <p>
 * This file is part of the Android/Java framework for the course "Computer graphics for augmented
 * reality" by Prof. Dr. Philipp Jenke at the University of Applied (UAS) Sciences Hamburg. Neither
 * parts of the framework nor the complete framework may be used outside of research or student
 * projects at the UAS Hamburg.
 */
package edu.hawhamburg.shared.rendering;

/**
 * Represents an OpenGL ES texture.
 */
public class Texture {


    private int textureId = -1;

    public Texture(int textureId) {
        this.textureId = textureId;
    }

    /**
     * Bind the texture as current texture.
     */
    public void bind() {
        if (textureId < 0) {
            throw new IllegalArgumentException("Texture not available.");
        }
        OpenGL.instance().platform().glBindTexture(textureId);
    }

    public void delete() {
        if (textureId > 0) {
            OpenGL.instance().platform().glDeleteTextures(textureId);
        }
    }
}
