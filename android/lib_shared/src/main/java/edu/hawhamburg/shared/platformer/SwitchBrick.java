/**
 * Diese Datei gehört zum Android/Java Framework zur Veranstaltung "Computergrafik für
 * Augmented Reality" von Prof. Dr. Philipp Jenke an der Hochschule für Angewandte
 * Wissenschaften (HAW) Hamburg. Weder Teile der Software noch das Framework als Ganzes dürfen
 * ohne die Einwilligung von Philipp Jenke außerhalb von Forschungs- und Lehrprojekten an der HAW
 * Hamburg verwendet werden.
 * <p>
 * This file is part of the Android/Java framework for the course "Computer graphics for augmented
 * reality" by Prof. Dr. Philipp Jenke at the University of Applied (UAS) Sciences Hamburg. Neither
 * parts of the framework nor the complete framework may be used outside of research or student
 * projects at the UAS Hamburg.
 */

package edu.hawhamburg.shared.platformer;

import edu.hawhamburg.shared.math.Vector;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

/**
 * A special brick which allows to switch between two segments.
 */
public class SwitchBrick extends Brick {
    private static final String JSON_SWITCHES = "switches";
    private static final String JSON_FROM = "from";
    private static final String JSON_TO = "to";

    /**
     * Inner class to represent a switch between two game state segments.
     */
    public class Switch {
        //public int from, to;
        public Vector from = new Vector(0, 0, 0), to = new Vector(0, 0, 0);

        public Switch(Vector from, Vector to) {
            this.from.copy(from);
            this.to.copy(to);
        }
    }

    private List<Switch> switches = new ArrayList<Switch>();

    public SwitchBrick() {
    }

    public int getNumberOfSwitches() {
        return switches.size();
    }

    public Switch getSwitch(int index) {
        return switches.get(index);
    }


    @Override
    public void fromJson(JSONObject jsonObject) {
        super.fromJson(jsonObject);
        JSONArray jsonSwitches = (JSONArray) jsonObject.get(JSON_SWITCHES);
        for (int i = 0; i < jsonSwitches.size(); i++) {
            JSONObject jsonSwitch = (JSONObject) jsonSwitches.get(i);
            JSONArray fromArray = (JSONArray) jsonSwitch.get(JSON_FROM);
            JSONArray toArray = (JSONArray) jsonSwitch.get(JSON_TO);
            Vector from = vectorFromJson(fromArray);
            Vector to = vectorFromJson(toArray);
            addSwitch(from, to);
        }
        setType(Type.SWITCH);
    }

    /**
     * Create a 3D vector from a Json array.
     */
    public static Vector vectorFromJson(JSONArray fromArray) {
        return new Vector(((Double) fromArray.get(0)), ((Double) fromArray.get(1)), ((Double) fromArray.get(2)));
    }

    public void addSwitch(Vector from, Vector to) {
        switches.add(new Switch(from, to));
    }

    /**
     * Handle all switches.
     */
    public void forEach(Consumer<Switch> switchConsumer) {
        switches.forEach(switchConsumer);
    }
}
