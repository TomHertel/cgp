/**
 * Diese Datei gehört zum Android/Java Framework zur Veranstaltung "Computergrafik für
 * Augmented Reality" von Prof. Dr. Philipp Jenke an der Hochschule für Angewandte
 * Wissenschaften (HAW) Hamburg. Weder Teile der Software noch das Framework als Ganzes dürfen
 * ohne die Einwilligung von Philipp Jenke außerhalb von Forschungs- und Lehrprojekten an der HAW
 * Hamburg verwendet werden.
 * <p>
 * This file is part of the Android/Java framework for the course "Computer graphics for augmented
 * reality" by Prof. Dr. Philipp Jenke at the University of Applied (UAS) Sciences Hamburg. Neither
 * parts of the framework nor the complete framework may be used outside of research or student
 * projects at the UAS Hamburg.
 */

package edu.hawhamburg.shared.platformer.levelgenerator;

import edu.hawhamburg.shared.misc.AssetPath;
import edu.hawhamburg.shared.misc.Logger;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import exercise7.Rule;

/**
 * The grammar contains a list of rules.
 */
public class Grammar {

    /**
     * List of rules in the grammar.
     */
    public List<IRule> rules;

    private Grammar(List<IRule> rules) {
        this.rules = rules;
    }

    /**
     * Parse grammar from text file.
     */
    public static Grammar fromGrammarFile(String ruleFile) {
        List<IRule> rules = new ArrayList<>();
        String assetPath = AssetPath.getInstance().getPathToAsset(ruleFile);
        try (FileReader reader = new FileReader(assetPath)) {
            BufferedReader br = new BufferedReader(reader);
            String line;
            while ((line = br.readLine()) != null) {
                // TODO
                rules.add(Rule.fromString(line));
            }
        } catch (FileNotFoundException e) {
            Logger.getInstance().log("Failed to parseFile rule file " + ruleFile);
        } catch (IOException e) {
            Logger.getInstance().log("Failed to parseFile rule file" + ruleFile);
        }
        Grammar grammar = new Grammar(rules);
        return grammar;
    }
}
