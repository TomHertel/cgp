/**
 * Diese Datei gehört zum Android/Java Framework zur Veranstaltung "Computergrafik für
 * Augmented Reality" von Prof. Dr. Philipp Jenke an der Hochschule für Angewandte
 * Wissenschaften (HAW) Hamburg. Weder Teile der Software noch das Framework als Ganzes dürfen
 * ohne die Einwilligung von Philipp Jenke außerhalb von Forschungs- und Lehrprojekten an der HAW
 * Hamburg verwendet werden.
 * <p>
 * This file is part of the Android/Java framework for the course "Computer graphics for augmented
 * reality" by Prof. Dr. Philipp Jenke at the University of Applied (UAS) Sciences Hamburg. Neither
 * parts of the framework nor the complete framework may be used outside of research or student
 * projects at the UAS Hamburg.
 */

package edu.hawhamburg.shared.platformer;

import edu.hawhamburg.shared.scenegraph.INode;

/**
 * Shared abstract class for all plugins for a game.
 */
public abstract class PlatformerPlugin {

    /**
     * Reference to the game the plugin is used for.
     */
    protected Game game;

    /**
     * This setup method is called automatically when the plugin is added to a game.
     */
    public void setup(Game game) {
        this.game = game;
    }

    /**
     * Perform all initialization (not in the constructor!).
     */
    public abstract void init();

    /**
     * This method is called for each game state main loop tick.
     */
    public abstract void updateGameState();

    /**
     * Return a node containing all the scene graph content for the plugin
     */
    public abstract INode getSceneGraphContent();

    /**
     * Handle a JSON key-value pair.
     */
    public abstract void handleJson(String key, Object value);

    /**
     * Return a name for the plugin (just for debugging).
     */
    public abstract String getPluginName();

    /**
     * Using this method all plugins are informed about game events.
     */
    public abstract void handleGameEvent(GameEvent gameEvent);
}
