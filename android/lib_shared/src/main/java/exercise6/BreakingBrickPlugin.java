package exercise6;

import edu.hawhamburg.shared.platformer.*;
import edu.hawhamburg.shared.scenegraph.INode;

public class BreakingBrickPlugin extends PlatformerPlugin {

    boolean removed = false;

    @Override
    public void init() {

    }

    @Override
    public void updateGameState() {

        if (removed) {
            for (int i = 0; i < game.getNumberOfGameObjects(); i++) {
                GameObject gameObject = game.getGameObject(i);
                if (gameObject.getType() == GameObject.Type.RIGID_BODY_BRICK && gameObject.getPosition().y() < -2) {
                    game.removeGameObject((gameObject));

                }

            }

        }

    }

    @Override
    public INode getSceneGraphContent() {
        return null;
    }

    @Override
    public void handleJson(String key, Object value) {

    }

    @Override
    public String getPluginName() {
        return "BreakingBrickPlugin";
    }

    @Override
    public void handleGameEvent(GameEvent gameEvent) {

        if (gameEvent.getType() == GameEvent.Type.BREAKING_BRICK) {

            ParticleBrick p = new ParticleBrick(game.getWorld().getBrickCenter((Brick) gameEvent.getPayload()), game, (Brick) gameEvent.getPayload());

            game.addGameObject(p);

            game.getWorld().deleteBrick((Brick) gameEvent.getPayload());

            GameEventQueue.getInstance().emitEvent(
                    new GameEvent(GameEvent.Type.REGENERATE_WORLD, gameEvent.getPayload()));

        }

    }

}
