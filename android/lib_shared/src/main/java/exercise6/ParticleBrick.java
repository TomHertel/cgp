package exercise6;

import edu.hawhamburg.shared.datastructures.mesh.TriangleMesh;
import edu.hawhamburg.shared.math.Vector;
import edu.hawhamburg.shared.platformer.Brick;
import edu.hawhamburg.shared.platformer.Game;
import edu.hawhamburg.shared.platformer.GameObject;
import edu.hawhamburg.shared.platformer.World;
import edu.hawhamburg.shared.scenegraph.TransformationNode;
import edu.hawhamburg.shared.scenegraph.TriangleMeshNode;

public class ParticleBrick extends GameObject {

    private Game game;

    double velocity = 0;
    double position = 0;

    private Vector CORNER_POINTS[] = {
            new Vector(0, 0, 0), new Vector(1, 0, 0),
            new Vector(1, 1, 0), new Vector(0, 1, 0),
            new Vector(0, 0, 1), new Vector(1, 0, 1),
            new Vector(1, 1, 1), new Vector(0, 1, 1)
    };

    private int[][] FACET_INCIDES = {
            {4, 0, 3, 7}, {1, 5, 6, 2},
            {4, 5, 1, 0}, {3, 2, 6, 7},
            {0, 1, 2, 3}, {5, 4, 7, 6}
    };

    private Vector TEXTURE_POINTS[] = {
            new Vector(0, 1), new Vector(1, 1),
            new Vector(1, 0), new Vector(0, 0)

    };

    private Vector SIDE_SHIFT[] = {
            new Vector(1.333, 0.833), new Vector(1, 0.666),
            new Vector(1, 0.833), new Vector(1.166, 0.833),
            new Vector(1.166, 0.666), new Vector(1.333, 0.666)
    };

    TriangleMesh triangleMesh = new TriangleMesh();

    public ParticleBrick(Vector pos, Game game,Brick brick) {

        super(Type.RIGID_BODY_BRICK, pos);
        this.setPosition(getPosition().subtract(pos));
        this.game = game;
        triangleMesh.setTextureName("textures/platformer_textures.png");
        World world = game.getWorld();

        Vector ll = world.getBrickOrigin(brick);

        double s = world.getBrickSize();

        int offset2 = 0;

        for (Vector CORNER_POINT : CORNER_POINTS) {

            Vector vector = ll.add(CORNER_POINT.multiply(s));
            triangleMesh.addVertex(vector);

        }

        for (Brick.Side side : Brick.Side.values()) {

            int ordinal = side.ordinal();

            for (Vector TEXTURE_POINT : TEXTURE_POINTS) {

                Vector ll2 = SIDE_SHIFT[ordinal];
                Vector v = ll2.add(TEXTURE_POINT.multiply((0.16667)));
                triangleMesh.addTextureCoordinate(v);

            }

            triangleMesh.addTriangle(
                    FACET_INCIDES[ordinal][1] ,
                    FACET_INCIDES[ordinal][0] ,
                    FACET_INCIDES[ordinal][2],
                    3 + offset2,
                    2 + offset2,
                    offset2);
            triangleMesh.addTriangle(
                    FACET_INCIDES[ordinal][2],
                    FACET_INCIDES[ordinal][0],
                    FACET_INCIDES[ordinal][3] ,
                    offset2,
                    2 + offset2,
                    1 + offset2);

            offset2 += 4;

        }

        triangleMesh.computeTriangleNormals();
        TriangleMeshNode  triangleMeshNode = new TriangleMeshNode(triangleMesh);
        TransformationNode orientationNode = new TransformationNode();
        orientationNode.addChild(triangleMeshNode);
        addChild(orientationNode);

    }

    @Override
    public void updateGameState() {

        //double m = 100;
        double g = 9.81;
        double d = 0.002;

        //double F = m*g;
        position += d*velocity;
        velocity += (d*g);

        // p = p + v * d
        // v = v  + a * d; a = F/m

        this.setPosition(this.getPosition().subtract(new Vector(0.0,position,0.0)));

    }

}
